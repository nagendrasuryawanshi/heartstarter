const Styles = {
    footer: {
        margin: 10,
        backgroundColor: '#fff',
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10

    },
    dot: {
        height: 65,
        width: 65,
        backgroundColor: 'rgb(249, 249, 249)',
        borderRadius: 50,
        marginTop: '-10%',
        alignSelf: 'center'
    },
    container: {
        backgroundColor: '#f9f9f9'
    },
    cardCol: {
        width: '30%',
        backgroundColor: 'rgb(215, 37, 59)',
        borderBottomStartRadius: 10,
        borderTopStartRadius: 10
    },
    cardColText: {
        color: '#fff',
        alignSelf: 'center',
        marginTop: 50,
        // fontWeight: 'bold',
        fontSize: 16
    },
    cardColFirst: {
        width: '60%'
    },
    cardView: {
        alignSelf: 'center'
    },
    image: {
        height: 120,
        width: 120
    },
    cardIcon: {
        color: '#000',
        alignSelf: 'center',
        marginTop: 50,
    },
    cardColSecond: {
        width: '10%'
    },
    footerCard: {
        backgroundColor: '#fff',
        width: 45,
        borderRadius: 50,
        height: 45,
        marginLeft: 10,
        marginTop: 10
    },
    footerIcon: {
        color: 'rgb(215, 37, 59)',
        alignItems: 'center',
        margin: 7
    },
    button: {
        alignItems: 'center'
    },
    buttonIcon: {
        color: 'rgb(93, 113, 131)'
    }

}

export default Styles;