import { Dimensions } from "react-native";
const screen = Dimensions.get("window");
const Styles = {
  container: {
    backgroundColor: "#f9f9f9",
  },
  footer: {
    margin: 10,
    backgroundColor: "#fff",
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  dot: {
    height: 65,
    width: 65,
    backgroundColor: "rgb(249, 249, 249)",
    borderRadius: 32,
    marginTop: "-15%",
    alignSelf: "center"
  },
  image: {
    width: (screen.width + screen.height) / 3,
    height: (screen.width + screen.height) / 3,
    //marginTop: 22,
  },
  imageAnimate: {
    width: (screen.width + screen.height) / 3.9,
    height: (screen.width + screen.height) / 3.8,
    marginTop: 22,
  },
  col: {
    color: "rgb(93, 113, 131)"
  },
  card: {
    backgroundColor: "#fff",
    width: 50,
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    marginLeft: "10%",
    marginRight: "10%"
  },
  view: {
    color: "rgb(215, 37, 59)",
    alignItems: "center",
    marginTop: "25%"
  }
};

export default Styles;
