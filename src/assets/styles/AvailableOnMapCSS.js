import {Dimensions } from 'react-native';
const screen=Dimensions.get('window')

const Styles = {
    container: {
        position: 'absolute',
        justifyContent: 'flex-end',
        alignItems: 'center',
        top: '10%',
        right: 0,
        left: 0,
        bottom: 0
      },
      modal: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'silver'
      },
      map: {
        position: 'absolute',
        top:'-15%',
        right: 0,
        left: 0,
        bottom: 0
      },
      footer: {
        margin: 10,
        backgroundColor: '#fff',
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    
      },
      dot: {
        height: 65,
        width: 65,
        backgroundColor: 'rgb(249, 249, 249)',
        borderRadius: 50,
        marginTop: '-10%',
        alignSelf: 'center'
      },
      mainContainer: {
        backgroundColor: '#D6D6DE'
      },
      spinnerView: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.4)',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        width: '100%',
        height: '100%'
      },
      spinnerTextColor: {
        color: '#FFF'
      },
      sppinnerColor: {
        color: 'rgb(215, 37, 59)'
      },
      calloutViewFirst: {
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        width: 300,
      },
      calloutViewSecond: {
        backgroundColor: '#E7183F',
        padding: 10
      },
      calloutViewThird: {
        marginTop: 10,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 10

      },
      viewFirstCol: {
        width: '80%'
      },
      viewFirstText: {
        color: '#fff',
        fontSize: 30
      },
      viewFirstSecondColText: {
        color: '#fff',
        fontSize: 40,
        alignSelf: 'flex-end',
        marginRight: -20
      },
      ViewSecondFirstCol: {
        width: '60%'
      },
      ViewSecondSecondCol: {
        width: '20%'
      },
      ViewSecondCardItem: {
        alignSelf: 'flex-end',
        borderWidth: 0.5,
        borderColor: 'rgb(215, 37, 59)',
        paddingBottom: 5,
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 5,
        height: 40,
        width: 40,
        borderRadius: 20
      },
      ViewSecondIcon: {
        color: '#000'
      },
      ViewSecondCardItemSecond: {
        alignSelf: 'flex-end',
        borderWidth: 0.5,
        borderColor: 'rgb(215, 37, 59)',
        paddingBottom: 5,
        paddingLeft: 11,
        paddingRight: 8,
        paddingTop: 5,
        height: 40,
        width: 40,
        borderRadius: 20
      },
      mapViewMarker: {
        backgroundColor: '#E7183F'
      },
      footerFirstIcon: {
        color: "rgb(93, 113, 131)"
      },
      footerSecondIcon: {
        color: "rgb(215, 37, 59)",
        alignItems: "center",
        margin: 7
      },
      footerCard: {
        backgroundColor: "#fff",
        width: 45,
        borderRadius: 50,
        height: 45,
        marginLeft: 10,
        marginTop: 10
      },
      view: {
        height: 120,
        marginRight: 20,
        marginLeft: 20,
        backgroundColor: '#fff',
        marginTop: 10,
        borderRadius: 10
      },
      viewRow: {
        backgroundColor: '#E7183F',
        height: 50,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
      },
      viewRowText: {
        color: '#fff',
        fontSize: 30
      },
      viewRowIcon: {
        color: '#fff',
        fontSize: 40,
        alignSelf: 'flex-end',
        marginRight: -20
      },
      viewCardItemColFirst: {
        color: '#000',
        fontSize: 30
      },
      colWidth: {
        width: '5%'
      },
      searchInputStyle:{
        description: {
          fontWeight: 'bold',
        },
        predefinedPlacesDescription: {
          color: '#1faadb',
    
        },
        textInputContainer: {
          backgroundColor: '#fff',
          borderTopColor: 'white',
          borderBottomColor: 'white',
          borderRadius:10
        }
      },
      item:{
        backgroundColor: '#fff',
        marginTop:screen.height/11*2,
        borderRadius:10,
        marginLeft:'5%',
        marginRight:'5%',
        alignItems:'center'
      },
      button:{
        backgroundColor:'rgb(215, 37, 59)',
        borderTopRightRadius:10,
        borderBottomEndRadius:10
      },
      buttonText:{
        color:'#fff',
        paddingLeft:10,
        paddingRight:10
      }
}

export default Styles;