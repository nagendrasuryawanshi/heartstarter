const Styles = {
    container: {
        position: "absolute",
        justifyContent: "flex-end",
        alignItems: "center",
        top: "10%",
        right: 0,
        left: 0,
        bottom: 0
    },
    map: {
        position: "absolute",
        top: 0,
        right: 0,
        left: 0,
        bottom: 0
    },
    item: {
        backgroundColor: '#fff',
        marginTop: 120,
        borderRadius: 10,
        marginLeft: '5%',
        marginRight: '5%',
        alignItems: 'center'
    },
    button: {
        backgroundColor: 'rgb(215, 37, 59)',
        borderTopRightRadius: 10,
        borderBottomEndRadius: 10
    },
    buttonText: {
        color: '#fff',
        paddingLeft: 10,
        paddingRight: 10
    },
    mainContainer: {
        backgroundColor: "#D6D6DE"
    },
    spinnerView: {
        flex: 1,
        backgroundColor: "rgba(0,0,0,0.4)",
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        width: "100%",
        height: "100%"
    },
    spinner: {
        color: "#FFF"
    },
    spinnerColor: {
        color: "rgb(215, 37, 59)"
    },
    calloutView: {
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        width: 300,

    },
    calloutViewFirst: {
        backgroundColor: "#E7183F",
        padding: 10
    },
    calloutRow: {
        width: "80%"
    },
    calloutRowText: {
        color: "#fff",
        fontSize: 30
    },
    calloutRowIcon: {
        color: "#fff",
        fontSize: 40,
        alignSelf: "flex-end",
        marginRight: -20
    },
    calloutViewSecond: {
        marginTop: 10,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 10
    },
    calloutViewSecondRow: {
        width: "60%"
    },
    calloutViewSecondCol: {
        width: "20%"
    },
    calloutViewSecondCardItem: {
        alignSelf: "flex-end",
        borderWidth: 0.5,
        borderColor: "rgb(215, 37, 59)",
        paddingBottom: 5,
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 5,
        height: 40,
        width: 40,
        borderRadius: 20
    },
    calloutViewSecondCardItemIcon: {
        color: "#000"
    },
    calloutViewSecondCardItemSecond: {
        alignSelf: "flex-end",
        borderWidth: 0.5,
        borderColor: "rgb(215, 37, 59)",
        paddingBottom: 5,
        paddingLeft: 11,
        paddingRight: 8,
        paddingTop: 5,
        height: 40,
        width: 40,
        borderRadius: 20
    },
    buttonSecond: {
        backgroundColor: "#E7183F",
        margin: 15,
        borderRadius: 10
    },
    secondButtonText: {
        color: "#fff",
        fontSize: 20,
        padding: 10
    },
    renderLeftButton: {
        alignSelf: 'center',
        paddingHorizontal: 5
    },
    view: {
        height: 120,
        marginRight: 20,
        marginLeft: 20,
        backgroundColor: "#fff",
        marginTop: 10,
        borderRadius: 10
    },
    viewRow: {
        backgroundColor: "#E7183F",
        height: 50,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },
    viewText: {
        color: "#fff",
        fontSize: 30
    },
    viewIcon: {
        color: "#fff",
        fontSize: 40,
        alignSelf: "flex-end",
        marginRight: -20
    },
    cardItemText: {
        color: "#000",
        fontSize: 30
    },
    cardItemCol: {
        width: "5%"
    },
    mapMarkerColor: {
        backgroundColor: "#E7183F"
    }
}

export default Styles;