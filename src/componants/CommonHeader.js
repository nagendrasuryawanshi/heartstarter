import React, { Component } from "react";
import { StyleSheet, Text, ImageBackground, View, TouchableOpacity, StatusBar,Dimensions } from "react-native";
import { Header, Left, Body, Right, Button, Icon, Title ,Row,Col} from "native-base";
import API from "../api/Apis";

const Apis = new API();
const window = Dimensions.get('screen')
export class FirstHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      OrientationStatus : '',
 
        Height_Layout : '',
 
        Width_Layout : '',
    };
  }

  componentDidMount(){
 
    this.DetectOrientation();
 
  }
 
  DetectOrientation(event){
    console.log("event",event)
    console.log('this.state.Width_Layout',window.width)
    console.log('this.state.Height_Layout',window.height)
    if(this.state.Width_Layout > this.state.Height_Layout)
    {
 
      // Write Your own code here, which you want to execute on Landscape Mode.
      console.log('Landscape Mode')
        this.setState({
        OrientationStatus : 'Landscape Mode'
        });
    }
    else{
 
      // Write Your own code here, which you want to execute on Portrait Mode.
      console.log('Portrait Mod')
        this.setState({
        OrientationStatus : 'Portrait Mode'
        });
    }
 
  }
  render() {
    return (
      <ImageBackground 
      //   onLayout={(event) => this.setState({
      //   Width_Layout : event.nativeEvent.layout.width,
      //   Height_Layout : event.nativeEvent.layout.height
      //  }, ()=> this.DetectOrientation(event))}
        source={require("../assets/Images/HEADER04.png")}
        style={{
          height: window.height/4.5, 
          position: 'absolute' , 
          top: -35, 
          left: 0,  
          right: 0, 
          zIndex: 9999, 
          //bottom:5
        //   elevation: 3,
        //   shadowColor: "#000",
        //   shadowOffset: {
        //   width: 5,
        //   height: 5,
        // },
        // shadowOpacity: 0.36,
        // shadowRadius: 6.68,
         }}
      >
          <Header
        transparent
          style={styles.header}
          statusBarProps={{ barStyle: "light-content" }}
          barStyle="light-content"
        >
          <Left style={{width: '20%',marginTop:40}}>
             <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
              <Icon name="minus" type='Feather' style={{ color: "#000", alignSelf: 'flex-start', fontSize: 30 }} />
              <Icon name="minus" type='Feather' style={{ color: "red", alignSelf: 'flex-start', fontSize: 18, marginTop: -19, marginLeft: 5, }} />
              <Icon name="minus" type='Feather' style={{ color: "#000", alignSelf: 'flex-start', fontSize: 30, marginTop: -20 }} />
            </TouchableOpacity>
          </Left>
            <View style={{width: '70%'}}>
              <Title style={{ color: "#000" , marginTop: 47 , paddingLeft: 10}}>
                Heart<Text style={{ color: "red" }}>S</Text>tarter
              </Title>
            </View>
            <Right style={{width: '10%',marginTop:40}}>
            <Button transparent onPress={() => Apis.calling()}>
              <Icon
                name="md-call"
                style={{fontSize:25,fontWeight: 'bold', color: "#000", transform: [{ rotate: "270deg" }] }}
              />
            </Button>
          </Right>
        </Header>
      </ImageBackground>
    );
  }
}

// export class FirstHeader extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       OrientationStatus : '',
 
//         Height_Layout : '',
 
//         Width_Layout : '',
//     };
//   }

//   componentDidMount(){
 
//     this.DetectOrientation();
 
//   }
 
//   DetectOrientation(event){
//     console.log("event",event)
//     console.log('this.state.Width_Layout',window.width)
//     console.log('this.state.Height_Layout',window.height)
//     if(this.state.Width_Layout > this.state.Height_Layout)
//     {
 
//       // Write Your own code here, which you want to execute on Landscape Mode.
//       console.log('Landscape Mode')
//         this.setState({
//         OrientationStatus : 'Landscape Mode'
//         });
//     }
//     else{
 
//       // Write Your own code here, which you want to execute on Portrait Mode.
//       console.log('Portrait Mod')
//         this.setState({
//         OrientationStatus : 'Portrait Mode'
//         });
//     }
 
//   }
//   render() {
//     return (
//       <ImageBackground  source={require("../assets/Images/HEADER04.png")} 
//       style={{
// width:null,
// height:window.height/4.5,
// marginTop:-40
//        }}>
//           <Header
//           transparent
//           style={{
//            // position:'relative',
//             marginTop:35,
//            }}
//              >
//           <Left style={{width: '20%'}}>
//              <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
//               <Icon name="minus" type='Feather' style={{ color: "#000", alignSelf: 'flex-start', fontSize: 30 }} />
//               <Icon name="minus" type='Feather' style={{ color: "red", alignSelf: 'flex-start', fontSize: 18, marginTop: -19, marginLeft: 5, }} />
//               <Icon name="minus" type='Feather' style={{ color: "#000", alignSelf: 'flex-start', fontSize: 30, marginTop: -20 }} />
//             </TouchableOpacity>
//           </Left>
//             <View style={{width: '70%',marginTop:10}}>
//               <Title style={{ color: "#000" }}>
//                 Heart<Text style={{ color: "red" }}>S</Text>tarter
//               </Title>
//             </View>
//             <Right style={{width: '10%'}}>
//             <Button transparent onPress={() => Apis.calling()}>
//               <Icon
//                 name="md-call"
//                 style={{fontSize:25,fontWeight: 'bold', color: "#000", transform: [{ rotate: "270deg" }] }}
//               />
//             </Button>
//           </Right>
//         </Header>
//         </ImageBackground>
//     );
//   }
// }

export class SecondHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <ImageBackground 
      //   onLayout={(event) => this.setState({
      //   Width_Layout : event.nativeEvent.layout.width,
      //   Height_Layout : event.nativeEvent.layout.height
      //  }, ()=> this.DetectOrientation(event))}
        source={require("../assets/Images/HEADER04.png")}
        style={{height: window.height/4.5, 
          position: 'absolute' , 
          top: -35, 
          left: 0,  
          right: 0, 
          zIndex: 9999, 
        //   elevation: 3,
        //   shadowColor: "#000",
        //   shadowOffset: {
        //   width: 5,
        //   height: 5,
        // },
        // shadowOpacity: 0.36,
        // shadowRadius: 6.68,
         }}
      >
          <Header
        transparent
          style={styles.header}
          statusBarProps={{ barStyle: "light-content" }}
          barStyle="light-content"
        >
          <Left style={{width: '25%',left:-20 ,marginTop:40}}>
            {/* <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Icon name="menu" style={{ color: "#000" }} />
            </Button> */}
            <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
              <Icon name="minus" type='Feather' style={{ color: "#000", alignSelf: 'flex-start', fontSize: 30 }} />
              <Icon name="minus" type='Feather' style={{ color: "red", alignSelf: 'flex-start', fontSize: 18, marginTop: -19, marginLeft: 5, }} />
              <Icon name="minus" type='Feather' style={{ color: "#000", alignSelf: 'flex-start', fontSize: 30, marginTop: -20 }} />
            </TouchableOpacity>
          </Left>
            <View style={{width: '75%'}}>
              <Title style={{ color: "#000" , marginTop: 45 , marginLeft: '-15%'}}>
                Heart<Text style={{ color: "red" }}>S</Text>tarter
              </Title>
            </View>
        </Header>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({

  header: {
    //backgroundColor: '#fff',
     height: null,
     position: 'relative',
     top: -15,
  },
  backendImage:{ 
    height: 100, 
    width: null, 
    elevation: 11,
    shadowColor: "#000",
    shadowOffset: {
    width: 0,
    height: 5,
  },
  shadowOpacity: 0.36,
  shadowRadius: 6.68,
  }
});
