import React, { Component } from "react";
import { StyleSheet, Text, Picker, View, ToastAndroid, TouchableOpacity, AsyncStorage, Alert, Linking, ScrollView, Dimensions } from "react-native";
import { Container, Button, Icon, Thumbnail, Content, Form, Item, Input, Label, Row, Spinner, Col, Right, Left } from "native-base";
import ImagePicker from "react-native-image-picker";
import DeviceInfo from "react-native-device-info";
import UserAvatar from "react-native-user-avatar";
import MultiSelect from "react-native-multiple-select";
let tracker = new GoogleAnalyticsTracker("UA-128498782-1");
import API from "../api/Apis";
import translations from "../utils/language.utils";
import NotifService from "../NotifService";
import { GoogleAnalyticsTracker, GoogleAnalyticsSettings, GoogleTagManager } from "react-native-google-analytics-bridge";
import { Storage, Fcm } from "../notifications";
import Config from "../config";
import { SecondHeader } from "./CommonHeader";



Storage.init();
//Fcm.saveTokenDevice();
const URL = Config.key.HS_API_URL;
const VERSION = Config.key.HS_API_VERSION;
const screen = Dimensions.get('window');
const Apis = new API();
const Gender = [{ id: 1, name: `${translations("Male")}` }, { id: 2, name: `${translations("Female")}` }];
const minOffset = 0;
const maxOffset = 80;
const thisYear = 2018;
var all = new Array();


export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      loading: "",
      phoneNumber: "",
      avatarSource: "",
      profilePhoto: "",
      longitude: "",
      latitude: "",
      deviceName: "",
      deviceId: "",
      userId: "",
      path: "",
      fileName: "",
      role: [],
      gender: [],
      getUniqueID: null,
      VERSION: null,
      imageName: "",
      notificationPerDay: [],
      checkBoxChecked: [],
      selectedItems: [],
      birthYear: "",
      devicePhoneNumber:''
    };


  }
  ////////checkbox func//////////////
  onSelectedItemsChange = selectedItems => {
    console.log("selectedItems", selectedItems);
    console.log("this.state.selectedItems", this.state.selectedItems);

    this.setState({ selectedItems });
  };


  setSpinner(st) {
    this.setState({ loading: st });
  }

  componentWillMount() {
      Apis.phoneNumberPemision();
    // this.reload();
    //////notification value dropdown////////

    var a = 0;
    for (i = 0; i < 10; i++) {
      all[i] = a + 10;
      a = a + 10;
    }
    //////end notification value dropdown////////

    const deviceId = DeviceInfo.getDeviceId();
    const systemName = DeviceInfo.getSystemName();
    const getUniqueID = DeviceInfo.getUniqueID();
    const getPhoneNumber =  DeviceInfo.getPhoneNumber();
    console.log("getPhoneNumber",getPhoneNumber)
    this.setState({ 
      getUniqueID: getUniqueID,
      devicePhoneNumber:getPhoneNumber
     });
    this.setState({
      deviceId: deviceId,
      deviceName: systemName
    });
    //  console.log("getUniqueID", this.state.getUniqueID);
    navigator.geolocation.getCurrentPosition(position => {
      console.log(
        `Got location: ${position.coords.latitude}, ${
        position.coords.longitude
        }`
      );

      this.setState({
        latitude: `${position.coords.latitude}`,
        longitude: `${position.coords.longitude}`
      });
    });

    AsyncStorage.getItem("USER", (err, res) => {
      if (res != null && res != "" && res != 0) {
        let result = JSON.parse(res).User;
        this.setState({
          userId: result._id,
          firstName: result.firstName,
          lastName: result.lastName,
          profilePhoto: result.profilePhoto,
          phoneNumber: result.phoneNumber,
          getUniqueID: result.deviceId,
          deviceName: result.deviceName,
          latitude: result.latitude,
          longitude: result.longitude,
          userTypeId: result.userTypeId,
          gender: result.gender,
          birthYear: result.birthYear,
          notificationPerDay: result.notificationPerDay,
          selectedItems: result.roles ? result.roles : []
          // isverified
        });
      }
    });

    Apis.getUserType()
      .then(response => {
        console.log("userType Response", response);

        this.setState({ role: response.Roles });
      })
      .catch(err => {
        //  console.log("UserType Err", err);
      });

    AsyncStorage.getItem("VERSION", (err, result) => {
      if (result === "versionMisMatch") {
        this.setState({ VERSION: result });
      } else {
        this.setState({ VERSION: null });
      }
    });
  }

  showToast(message) {
    ToastAndroid.showWithGravityAndOffset(
      message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50
    );
  }

  selectPhotoTapped() {
    const options = {
      title: "Select Avatar",
      customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        // console.log('User cancelled image picker');
      } else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        //console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response };
        this.setState({
          profilePhoto: response.fileName,
          imageName: response.uri,
          path: "data:image/jpeg;base64," + response.data,
          fileName: response.fileName
        });
      }
    });
  }

  checkApiVersion() {
    Apis.getApplicationVersion()
      .then(response => {
        console.log("Api Version", response);
        if (response.VersionStatus == Config.key.HS_VERSION_STATUS_CODE.GONE) {
          console.log("401");
          Alert.alert(
            "",
            `${translations("CHECKAPIVERSION_Text")}`[
            ({
              text: "Get New Version",
              onPress: () =>
                Linking.openURL("https://play.google.com/store?hl=en"),
              style: "cancel"
            },
              { text: "Ask me later", onPress: () => this.versionMismatch() })
            ],
            { cancelable: false }
          );
        } else if (
          response.VersionStatus ==
          Config.key.HS_VERSION_STATUS_CODE.MOVED_PERMANENTLY
        ) {
          // console.log("301");
          Alert.alert(
            "",
            `${translations("CHECKAPIVERSION_Text")}`[
            {
              text: "Update",
              onPress: () =>
                Linking.openURL("https://play.google.com/store?hl=en"),
              style: "cancel"
            }
            ],
            { cancelable: this.props.navigation.navigate("Home") }
          );
        } else if (
          response.VersionStatus ==
          Config.key.HS_VERSION_STATUS_CODE.LATEST_AVAILABLE
        ) {
          // console.log("0");
          Alert.alert(
            "",
            `${translations("CHECKAPIVERSION_Text")}`[
            ({
              text: "Get New Version",
              onPress: () =>
                Linking.openURL("https://play.google.com/store?hl=en"),
              style: "cancel"
            },
              {
                text: "Ask me later",
                onPress: () => console.log("Ask me later")
              })
            ],
            { cancelable: this.props.navigation.navigate("Profile") }
          );
        } else {
          AsyncStorage.setItem("VERSION", "", () => {
            AsyncStorage.getItem("VERSION", (err, result) => {
              //console.log(result);
            });
          });
          console.log("VERSION MATCH", response.apiVersion);
        }
      })
      .catch(err => {
        console.log("err", err);
      });
  }

  versionMismatch() {
    AsyncStorage.setItem("VERSION", "versionMisMatch", () => {
      AsyncStorage.getItem("VERSION", (err, result) => {
        // console.log("VERSION", result);
        this.setState({ VERSION: result });
      });
    });
  }

  Submit() {
    this.setSpinner(true);
    switch (true) {
      case !this.state.firstName &&
        !this.state.lastName &&
        !this.state.phoneNumber &&
        !this.state.notificationPerDay &&
        !this.state.gender &&
        !this.state.birthYear &&
        !this.state.getUniqueID &&
        !this.state.deviceName &&
        !this.state.latitude &&
        !this.state.longitude:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_ProfileFillAllDetail")}`);
        break;
      case !this.state.firstName:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_ProfileFirstName")}`);
        break;
      case !this.state.lastName:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_ProfileLastName")}`);
        break;
      case !this.state.phoneNumber:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_ProfileMobile")}`);
        break;
      case this.state.phoneNumber.length <= 9:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_ProfileMobile")}`);
        break;
      case !this.state.notificationPerDay:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_ProfileNotification")}`);
        break;
      case !this.state.gender:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_ProfileGender")}`);
        break;
      case !this.state.birthYear:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_ProfileYearofbirth")}`);
        break;
      default:
        data = {
          firstName: this.state.firstName,
          lastName: this.state.lastName,
          profilePhoto: this.state.profilePhoto,
          phoneNumber: this.state.phoneNumber,
          deviceId: this.state.getUniqueID,
          deviceName: this.state.deviceName,
          latitude: this.state.latitude,
          longitude: this.state.longitude,
          gender: this.state.gender,
          roles: this.state.selectedItems ? this.state.selectedItems : [],
          birthYear: this.state.birthYear,
          notificationPerDay: this.state.notificationPerDay
        };
        const photo = {
          uri: this.state.imageName,
          type: "image/jpeg",
          name: this.state.fileName
        };
        const getUniqueID = DeviceInfo.getUniqueID();
        const form = new FormData();
        if (photo.uri != null && photo.uri != "") {
          form.append("profilePhoto", photo);
        } else {
          form.append("profilePhoto", data.profilePhoto);
        }

        form.append("firstName", data.firstName);
        form.append("lastName", data.lastName);
        form.append("phoneNumber", data.phoneNumber);
        form.append("deviceId", getUniqueID);
        form.append("deviceName", data.deviceName);
        form.append("longitude", data.longitude);
        form.append("latitude", data.latitude);
        form.append("gender", data.gender);
        if (data.roles != "" && data.roles != undefined) {
          data.roles.map(role => {
            form.append("roles", role);
          });
        }
        form.append("birthYear", data.birthYear);
        form.append("notificationPerDay", data.notificationPerDay);

        //console.log("form data", form);
        if (
          this.state.userId != null &&
          this.state.userId != "" &&
          this.state.userId != 0
        ) {
          form.append("Id", this.state.userId);
        }

        if (
          this.state.userId != null &&
          this.state.userId != "" &&
          this.state.userId != 0
        ) {
          //console.log("response of craete User", form);
          fetch(URL + VERSION + "/users/save", {
            body: form,
            method: "POST",
            headers: {
              "Content-Type": "multipart/form-data"
            }
          })
            .then(response => response.json())
            .then(responseData => {
              console.log("response", responseData);
              GoogleAnalyticsSettings.setDispatchInterval(2);
              tracker.trackEvent("ProfileButton", "onPress");
              if (responseData != null) {
                this.reload();
                this.setSpinner(false);
                this.showToast(
                  `${translations("TOAST_ProfileUpdateSuccessfully")}`
                );
                this.props.navigation.navigate("Profile");
              }
            })
            .catch(error => {
              this.setSpinner(false);
              // tracker.trackException(error, false);
              console.log("response of  not craete User", error);
              this.showToast(`${translations("TOAST_NetworkErr")}`);
            });
        } else {
          fetch(URL + VERSION + "/users/save", {
            body: form,
            method: "POST",
            headers: {
              "Content-Type": "multipart/form-data"
            }
          })
            .then(response => response.json())
            .then(responseData => {
              if (
                responseData != "" &&
                responseData != 0 &&
                responseData != null
              ) {
                //tracker.trackEvent("Button", "onPress");
                AsyncStorage.setItem(
                  "USER",
                  JSON.stringify(responseData),
                  () => {
                    AsyncStorage.mergeItem(
                      "USER",
                      JSON.stringify(responseData),
                      () => {
                        AsyncStorage.getItem("USER", (err, result) => {
                          console.log(result);
                          this.props.navigation.navigate("Home");
                        });
                      }
                    );
                  }
                );
                this.setSpinner(false);
                this.showToast(`${translations("TOAST_Create")}`);
                this.props.navigation.navigate("Home");
              } else {
                this.setSpinner(false);
                this.props.navigation.navigate("Profile");
              }
            })
            .catch(error => {
              this.setSpinner(false);
              this.props.navigation.navigate("Profile");
              console.log("response of  not craete User", error);
              this.showToast(`${translations("TOAST_NetworkErr")}`);
              //tracker.trackException(error, false);
            });
        }
    }
  }

  reload() {
    this.setSpinner(true);
    const getUniqueID = DeviceInfo.getUniqueID();
    Apis.getUserDeviceById(getUniqueID)
      .then(responseData => {
        console.log("responseData", responseData);
        AsyncStorage.setItem(
          "USER",
          JSON.stringify({ User: responseData }),
          () => {
            AsyncStorage.mergeItem("USER", JSON.stringify(responseData), () => {
              AsyncStorage.getItem("USER", (err, result) => {
                console.log("result.firstName", JSON.parse(result).firstName);
                this.setState({
                  userId: JSON.parse(result)._id,
                  firstName: JSON.parse(result).firstName,
                  lastName: JSON.parse(result).lastName,
                  profilePhoto: JSON.parse(result).profilePhoto,
                  phoneNumber: JSON.parse(result).phoneNumber,
                  getUniqueID: JSON.parse(result).deviceId,
                  deviceName: JSON.parse(result).deviceName,
                  latitude: JSON.parse(result).latitude,
                  longitude: JSON.parse(result).longitude,
                  userTypeId: JSON.parse(result).userTypeId,
                  gender: JSON.parse(result).gender,
                  birthYear: JSON.parse(result).birthYear,
                  notificationPerDay: JSON.parse(result).notificationPerDay,
                  selectedItems: JSON.parse(result).roles
                    ? JSON.parse(result).roles
                    : []
                });
                this.setSpinner(false);
                this.props.navigation.navigate("Home");
              });
            });
          }
        );
      })
      .catch(err => {
        this.setSpinner(false);
        console.log(err);
      });
  }
  onHandleChange = evt => {
    this.setState({
      birthYear: evt
    });
  };

  render() {
    tracker.trackScreenView("Profile");
    const options = [];
    // console.log("tracker", tracker)
    const { selectedItems, VERSION } = this.state;
    for (let i = minOffset; i <= maxOffset; i++) {
      const year = thisYear - i;
      options.push(
        <Picker.Item key={i} value={year.toString()} label={year.toString()} />
      );
    }
    const imageUrl = URL + this.state.profilePhoto;
    return (
      <Container style={{ backgroundColor: "#fff" }}>
        <SecondHeader {...this.props} />
        <ScrollView>
          <Content style={{ paddingTop: screen.height / 6, backgroundColor: "rgb(253, 244, 245)" }}>

            <Form>
              <View style={{ backgroundColor: "rgb(253, 244, 245)" }}>
                <Row style={{ margin: 20, alignSelf: "center" }}>
                  <Col style={{ marginTop: 20 }}>
                    <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
                      <View style={styles.dotCenter}>
                        <View style={{ alignSelf: "center", marginTop: "27%" }}>
                          <Icon
                            type="MaterialIcons"
                            name="add-a-photo"
                            style={{ color: "rgb(215, 37, 59)" }}
                          />
                        </View>
                      </View>
                    </TouchableOpacity>
                  </Col>
                  <Col style={{ marginTop: 20 }}>
                    <View style={{ alignSelf: "center" }}>
                      {this.state.profilePhoto != "" &&
                        this.state.profilePhoto != null &&
                        this.state.profilePhoto != "undefined" &&
                        this.state.profilePhoto != undefined ? (
                          <Thumbnail large source={{ uri: imageUrl }} />
                        ) : (
                          <UserAvatar
                            size="80"
                            name={this.state.firstName + " " + this.state.lastName}
                          />
                        )}
                      {this.state.path && this.state.path != null ? (
                        <Thumbnail
                          style={{ marginTop: -75 }}
                          large
                          source={{ uri: this.state.path }}
                        />
                      ) : null}
                    </View>
                  </Col>
                  <Col style={{ marginTop: 20 }}>
                    <View style={styles.dotCenter}>
                      <TouchableOpacity
                        onPress={() => this.props.navigation.navigate("Home")}
                      >
                        <View style={{ alignSelf: "center", marginTop: "27%" }}>
                          <Icon
                            type="Feather"
                            name="log-out"
                            style={{ color: "rgb(215, 37, 59)" }}
                          />
                        </View>
                      </TouchableOpacity>
                    </View>
                  </Col>
                </Row>
              </View>
              <Item last />
              <Row style={{ backgroundColor: '#fff' }}>
                <Col>
                  <Item inlineLabel last>
                    <Label style={{ width: '50%' }}>
                      <Text style={{ color: "rgb(215, 37, 59)" }}>
                        {translations("First")}{" "}
                      </Text>{" "}
                      {translations("Name")}
                    </Label>
                    <Input
                      keyboardType="default"
                      placeholder={translations("PlacehlderFirstname")}
                      onChangeText={text => this.setState({ firstName: text })}
                      value={this.state.firstName}
                    />
                  </Item>
                  <Item inlineLabel last>
                    <Label style={{ width: '50%' }}>
                      <Text style={{ color: "rgb(215, 37, 59)" }}>
                        {translations("Last")}{" "}
                      </Text>
                      {translations("Name")}
                    </Label>
                    <Input
                      keyboardType="default"
                      placeholder={translations("PlacehlderLastname")}
                      onChangeText={text => this.setState({ lastName: text })}
                      value={this.state.lastName}
                    />
                  </Item>
                </Col>
              </Row>
              <Row style={{ backgroundColor: '#fff' }}>
                <Col>
                  <Item inlineLabel last>
                    <Label style={{ width: '50%' }}>
                      <Text style={{ color: "rgb(215, 37, 59)" }}>
                        {translations("Phone")}{" "}
                      </Text>{" "}
                      {translations("Number")}
                    </Label>
                    <Input
                      keyboardType="numeric"
                      placeholder={translations("Placehlderphonenumber")}
                      maxLength={10}
                      onChangeText={text => this.setState({ phoneNumber: text })}
                      value={this.state.phoneNumber?this.state.phoneNumber:this.state.devicePhoneNumber}
                    />
                  </Item>

                  {/* <Item inlineLabel last>
                  <Label>
                    <Text style={{ color: "rgb(215, 37, 59)" }}>
                      Notification
                    </Text>{" "}
                    Per day
                  </Label>
                  <Input
                    keyboardType="numeric"
                    placeholder="notification per day "
                    maxLength={2}
                    onChangeText={text =>
                      this.setState({ notificationPerDay: text })
                    }
                    value={
                      this.state.notificationPerDay
                        ? this.state.notificationPerDay.toString()
                        : null
                    }
                  />

                </Item> */}
                </Col>
              </Row>
              <Row style={{ backgroundColor: '#fff' }}>
                <Item style={{ borderBottomWidth: 0 }}>
                  <Label style={{ width: '65%' }}>
                    <Text style={{ color: "rgb(215, 37, 59)" }}>
                      {translations("Notification")}
                    </Text>{" "}
                    {translations("Per")} {translations("day")}
                  </Label>
                </Item>
                <Picker
                  selectedValue={
                    this.state.notificationPerDay
                      ? this.state.notificationPerDay.toString()
                      : null
                  }
                  style={{
                    height: 50,
                    width: 150,
                    marginLeft: -50,
                    marginRight: "0%"
                  }}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({ notificationPerDay: itemValue })
                  }
                >
                  <Picker.Item label={translations("NotificationLabel")} />
                  {all.map((item, index) => {
                    return (
                      <Picker.Item
                        key={index}
                        label={item.toString()}
                        value={item.toString()}
                      />
                    );
                  })}
                </Picker>
              </Row>
              <Item last />
              <Row style={{ backgroundColor: '#fff' }}>
                <Item style={{ borderBottomWidth: 0 }}>
                  <Label style={{ width: '65%' }}>
                    <Text style={{ color: "rgb(215, 37, 59)" }}>
                      {translations("Your")}{" "}
                    </Text>
                    {translations("Gender")}
                  </Label>
                </Item>
                <Picker
                  selectedValue={this.state.gender}
                  style={{
                    height: 50,
                    width: 150,
                     marginLeft: -50,
                    // marginRight: "4%"
                  }}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({ gender: itemValue })
                  }
                >
                  <Picker.Item label={translations("GenderLabel")} />
                  {Gender.map((item, index) => {
                    return (
                      <Picker.Item
                        key={index}
                        label={item.name}
                        value={item.name}
                      />
                    );
                  })}
                </Picker>
              </Row>
              <Item last />

              <Row style={{ backgroundColor: '#fff' }}>
                <Item style={{ borderBottomWidth: 0 }}>
                  <Label style={{ width: '65%' }}>
                    <Text style={{ color: "rgb(215, 37, 59)" }}>
                      {translations("Year")}{" "}
                    </Text>
                    {translations("OfBirth")}
                  </Label>
                </Item>
                {/* <DatePicker
                // defaultDate={new Date()}
                // minimumDate={new Date(199, 1, 1)}
                maximumDate={new Date()}
                locale={"en"}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                placeHolderText={
                  this.state.birthYear
                    ? this.state.birthYear.split("T")[0]
                    : "select"
                }
                textStyle={{ color: "black" }}
                placeHolderTextStyle={{ marginLeft: "25%" }}
                onDateChange={this.setDate}
              /> */}
                <Picker
                  selectedValue={
                    this.state.birthYear ? this.state.birthYear.toString() : null
                  }
                  style={{
                    height: 50,
                    width: 150,
                    marginLeft: -50,
                    // marginLeft: "10%",
                    // marginRight: "4%"
                  }}
                  onValueChange={this.onHandleChange}
                >
                  <Picker.Item value="" label={translations("YearLabel")} />
                  {options}
                </Picker>
                {/* <Text>{this.state.birthYear}</Text> */}
                {/* <select value={this.selectedYear} onChange={this.onHandleChange}>  */}
              </Row>
              <Item last />
              <Row style={{ backgroundColor: '#fff' }}>
                <Col>
                  <Item style={{ borderBottomWidth: 0, backgroundColor: '#fff' }}>
                    <Label style={{ marginTop: "2%", width: '65%' }}>
                      <Text style={{ color: "rgb(215, 37, 59)" }}>
                        {translations("Your")}{" "}
                      </Text>{" "}
                      {translations("Role")}
                    </Label>
                  </Item>
                  {/* <View
              style={{
                width: "100%",
                marginLeft: "52%",
                marginTop: -42,
                height: '10%',
                backgroundColor:'#fff',
                position: 'relative',
              }}
            > */}
                </Col>
                <Col>
                  <View style={{ flex: 1, marginTop: 10, marginLeft: 0 }}>
                    <MultiSelect
                      hideTags
                      items={this.state.role}
                      uniqueKey="_id"
                      ref={component => {
                        this.multiSelect = component;
                      }}
                      onSelectedItemsChange={this.onSelectedItemsChange}
                      selectedItems={selectedItems}
                      selectText={translations("RoleLabel")}
                      searchInputPlaceholderText="Search Items..."
                      onChangeInput={text => console.log(text)}
                      altFontFamily="ProximaNova-Light"
                      tagRemoveIconColor="#CCC"
                      tagBorderColor="#CCC"
                      tagTextColor="#CCC"
                      selectedItemTextColor="#000"
                      selectedItemIconColor="#CCC"
                      itemTextColor="#000"
                      displayKey="role"
                      searchInputStyle={{ color: "#CCC" }}
                      submitButtonColor={false.toString()}
                      submitButtonText={false}
                    />
                    <View
                      style={styles.selectedItem}
                    >
                      {this.multiSelect
                        ? this.multiSelect.getSelectedItemsExt()
                        : null}
                    </View>
                  </View>

                </Col>
              </Row>
              {/* <Item/> */}
            </Form>

          </Content>
        </ScrollView>
        {VERSION === "versionMisMatch" ? (
          <Button disabled full style={{ margin: 10, borderRadius: 10 }}>
            <Text style={{ color: "#fff" }}>
              {" "}
              {translations("Save")} & {translations("Update")}{" "}
            </Text>
          </Button>
        ) : (
            <Button
              full
              onPress={() => this.Submit()}
              style={{
                backgroundColor: "rgb(215, 37, 59)",
                margin: 10,
                borderRadius: 10
              }}
            >
              <Text style={{ color: "#fff" }}>
                {" "}
                {translations("Save")} & {translations("Update")}{" "}
              </Text>
            </Button>
          )}
        {this.state.loading ? (
          <View
            style={{
              flex: 1,
              backgroundColor: "rgba(0,0,0,0.4)",
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              width: "100%",
              height: "100%"
            }}
          >
            <Spinner color="rgb(215, 37, 59)" />
          </View>
        ) : null}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  footer: {
    margin: 10,
    backgroundColor: "#fff",
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  dotCenter: {
    backgroundColor: "#f5e1e3",
    borderRadius: 40,
    height: 80,
    width: 80,
    marginRight: "5%",
    marginLeft: "5%",
    alignSelf: "center"
  },
  selectedItem: {
    height: 47,
    marginTop: -12
  },
});
