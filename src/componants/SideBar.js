import React, { Component } from "react";
import { View, AsyncStorage, Linking, Alert } from "react-native";
import {
  Container,
  Content,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Footer,
  Title
} from "native-base";
import { NavigationActions } from "react-navigation";
import styles from "../assets/styles/SiderBarCSS";
import Config from "../config";
import { trackScreenView } from "../analytics.utils";
import DeviceInfo from "react-native-device-info";
import API from "../api/Apis";
import UserAvatar from "react-native-user-avatar";
import translations from "../utils/language.utils";
const URL = Config.key.HS_API_URL;
const Apis = new API();

export default class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: [],
      firstName: "",
      lastName: "",
      profilePhoto: "",
      screenName: "Home"
    };
  }

  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
    trackScreenView(navigateAction);
  };

  componentWillMount() {
    this.reload();
    Apis.phoneNumberPemision();
  }

  Active(screenName) {
    this.setState({ screenName: screenName });
  }

  componentWillReceiveProps() {
    this.reload();
  }

  checkProfileStatus() {
    const getUniqueID = DeviceInfo.getUniqueID();
    Apis.getUserDeviceById(getUniqueID)
      .then(responseData => {
        console.log("responseData", responseData);
        if (responseData != null && responseData != "") {
          if (responseData.message) {
            this.setState({ profileStatus: true });
            Alert.alert(
              "",
              "Please fill your profile details",
              [
                {
                  text: "OK",
                  onPress: () => this.props.navigation.navigate("Profile")
                }
              ],
              { cancelable: false }
            );
          } else {
            this.props.navigation.navigate("ActiveOnMap");
          }
        }
      })
      .catch(err => {
        console.log("err", err);
      });
  }

  reload() {
    const getUniqueID = DeviceInfo.getUniqueID();
    Apis.getUserDeviceById(getUniqueID)
      .then(responseData => {
        AsyncStorage.setItem(
          "USER",
          JSON.stringify({ User: responseData }),
          () => {
            AsyncStorage.mergeItem("USER", JSON.stringify(responseData), () => {
              AsyncStorage.getItem("USER", (err, result) => {
                //console.log(result);
                if (result != null) {
                  this.setState({
                    userData: JSON.parse(result).User,
                    firstName: JSON.parse(result).User.firstName,
                    lastName: JSON.parse(result).User.lastName,
                    profilePhoto: JSON.parse(result).User.profilePhoto
                  });
                }
              });
            });
          }
        );
      })
      .catch(err => {
        console.log(err);
      });
  }

  checkApiVersion() {
    Apis.getApplicationVersion()
      .then(response => {
        if (response.VersionStatus == Config.key.HS_VERSION_STATUS_CODE.GONE) {
          //console.log("401");
          Alert.alert(
            "",
            "NEW VERSION AVAILABLE",
            [
              {
                text: "Get New Version",
                onPress: () =>
                  Linking.openURL("https://play.google.com/store?hl=en")
              },
              {
                text: "Ask me later",
                onPress: () => this.versionMismatch(),
                style: "cancel"
              }
            ],
            { cancelable: this.props.navigation.navigate("Profile") }
          );
        } else if (
          response.VersionStatus ==
          Config.key.HS_VERSION_STATUS_CODE.MOVED_PERMANENTLY
        ) {
          // console.log("301");
          Alert.alert(
            "",
            "NEW VERSION AVAILABLE",
            [
              {
                text: "Update",
                onPress: () =>
                  Linking.openURL("https://play.google.com/store?hl=en"),
                style: "cancel"
              }
            ],
            { cancelable: this.props.navigation.navigate("Home") }
          );
        } else if (
          response.VersionStatus ==
          Config.key.HS_VERSION_STATUS_CODE.LATEST_AVAILABLE
        ) {
          // console.log("0");
          Alert.alert(
            "",
            "NEW VERSION AVAILABLE",
            [
              {
                text: "Get New Version",
                onPress: () =>
                  Linking.openURL("https://play.google.com/store?hl=en"),
                style: "cancel"
              },
              {
                text: "Ask me later",
                onPress: () => console.log("Ask me later")
              }
            ],
            { cancelable: this.props.navigation.navigate("Profile") }
          );
        } else {
          AsyncStorage.setItem("VERSION", "", () => {
            AsyncStorage.getItem("VERSION", (err, result) => {
              //console.log(result);
              this.props.navigation.navigate("Profile");
            });
          });
          console.log("VERSION MATCH", response.apiVersion);
        }
      })
      .catch(err => {
        console.log("err", err);
      });
  }

  versionMismatch() {
    AsyncStorage.setItem("VERSION", "versionMisMatch", () => {
      AsyncStorage.getItem("VERSION", (err, result) => {
        // console.log("VERSION", result);
      });
    });
  }

  profile() {
    this.checkApiVersion();
  }

  checkProfileStatus2() {
    const getUniqueID = DeviceInfo.getUniqueID();
    Apis.getUserDeviceById(getUniqueID)
      .then(responseData => {
        if (responseData != null && responseData != "") {
          if (responseData.message) {
            this.setState({ profileStatus: true });
            Alert.alert(
              "",
              `${translations("TOAST_SidebarFillAllDetail")}`,

              [
                {
                  text: "OK",
                  onPress: () => this.props.navigation.navigate("Profile")
                }
              ],
              { cancelable: false }
            );
          } else {
            if (responseData.verified == false) {
              Alert.alert(
                "",
                `${translations("TOAST_SidebarNotVerified")}`,

                [
                  {
                    text: "OK"
                    // onPress: () => this.props.navigation.navigate("Profile")
                  }
                ],
                { cancelable: false }
              );
            } else {
              this.checkApiVersion2();
            }
          }
        }
      })
      .catch(err => {
        console.log("err", err);
      });
  }

  checkApiVersion2() {
    Apis.getApplicationVersion()
      .then(response => {
        if (response.VersionStatus == Config.key.HS_VERSION_STATUS_CODE.GONE) {
          //  console.log("401");
          Alert.alert(
            "",

            `${translations("TOAST_SidebarNewVersion")}`,
            [
              {
                text: "Get New Version",
                onPress: () =>
                  Linking.openURL("https://play.google.com/store?hl=en"),
                style: "cancel"
              },
              { text: "Ask me later", onPress: () => this.versionMismatch() }
            ],
            { cancelable: this.props.navigation.navigate("AddDevice") }
          );
        } else if (
          response.VersionStatus ==
          Config.key.HS_VERSION_STATUS_CODE.MOVED_PERMANENTLY
        ) {
          // console.log("301");
          Alert.alert(
            "",
            `${translations("TOAST_SidebarNewVersion")}`,
            [
              {
                text: "Update",
                onPress: () =>
                  Linking.openURL("https://play.google.com/store?hl=en"),
                style: "cancel"
              }
            ],
            { cancelable: this.props.navigation.navigate("Home") }
          );
        } else if (
          response.VersionStatus ==
          Config.key.HS_VERSION_STATUS_CODE.LATEST_AVAILABLE
        ) {
          // console.log("0");
          Alert.alert(
            "",
            `${translations("TOAST_SidebarNewVersion")}`,
            [
              {
                text: "Get New Version",
                onPress: () =>
                  Linking.openURL("https://play.google.com/store?hl=en"),
                style: "cancel"
              },
              {
                text: "Ask me later",
                onPress: () => console.log("Ask me later")
              }
            ],
            { cancelable: this.props.navigation.navigate("AddDevice") }
          );
        } else {
          AsyncStorage.setItem("VERSION", "", () => {
            AsyncStorage.getItem("VERSION", (err, result) => {
              //console.log(result);
              this.props.navigation.navigate("AddDevice");
            });
          });
          console.log("VERSION MATCH", response.apiVersion);
        }
      })
      .catch(err => {
        console.log("err", err);
      });
  }

  AddDevice() {
    this.checkProfileStatus2();
  }

  activeMap() {
    this.checkProfileStatus();
  }

  render() {
    return (
      <Container>
        <Content style={styles.content}>
          <View>
            <CardItem>
              <Left>
                {this.state.profilePhoto != null &&
                this.state.profilePhoto != "undefined" &&
                this.state.userData.profilePhoto != "" &&
                this.state.profilePhoto != undefined ? (
                  <Thumbnail
                    large
                    source={{ uri: URL + "/" + this.state.profilePhoto }}
                    style={styles.thumbnail}
                    onPress={() => this.checkApiVersion()}
                  />
                ) : (
                  <UserAvatar
                    size="80"
                    name={this.state.firstName + " " + this.state.lastName}
                  />
                )}

                <Body>
                  {this.state.userData != null && this.state.userData != "" ? (
                    <Text
                      style={styles.bodyText}
                      onPress={() => this.profile()}
                    >
                      {this.state.firstName
                        ? this.state.firstName
                        : translations("SOS")}{" "}
                      {this.state.lastName ? this.state.lastName : null}
                    </Text>
                  ) : null}

                  <Text note>CPR-Runner</Text>
                </Body>
              </Left>
            </CardItem>
          </View>
          <CardItem
            button
            onPress={() => {
              this.props.navigation.navigate("Home"), this.Active("Home");
            }}
          >
            <Icon
              type="FontAwesome"
              name="heartbeat"
              style={
                this.state.screenName === "Home"
                  ? styles.redColor
                  : styles.greyColor
              }
            />
            <Text
              style={
                this.state.screenName === "Home"
                  ? styles.redColor
                  : styles.greyColor
              }
            >
              {translations("SOS")}
            </Text>
          </CardItem>
          <CardItem
            button
            onPress={() => {
              this.activeMap(), this.Active("AddDeviceMap");
            }}
          >
            <Icon
              name="medkit"
              type="FontAwesome"
              style={
                this.state.screenName === "AddDeviceMap"
                  ? styles.redColor
                  : styles.greyColor
              }
            />
            <Text
              style={
                this.state.screenName === "AddDeviceMap"
                  ? styles.redColor
                  : styles.greyColor
              }
            >
              {translations("EmergencyResponse")}
            </Text>
            <Right>
              <Button style={styles.button}>
                <Text>2</Text>
              </Button>
            </Right>
          </CardItem>
          <CardItem
            button
            onPress={() => {
              this.AddDevice(), this.Active("AddDevice");
            }}
          >
            <Icon
              style={
                this.state.screenName === "AddDevice"
                  ? styles.redColor
                  : styles.greyColor
              }
              type="FontAwesome"
              name="eye"
            />
            <Text
              style={
                this.state.screenName === "AddDevice"
                  ? styles.redColor
                  : styles.greyColor
              }
            >
              {translations("AddDevice")}
            </Text>
          </CardItem>
          <CardItem
            button
            onPress={() => {
              this.checkApiVersion(), this.Active("Profile");
            }}
          >
            <Icon
              style={
                this.state.screenName === "Profile"
                  ? styles.redColor
                  : styles.greyColor
              }
              type="FontAwesome"
              name="user-circle"
            />
            <Text
              style={
                this.state.screenName === "Profile"
                  ? styles.redColor
                  : styles.greyColor
              }
            >
              {translations("Profile")}
            </Text>
          </CardItem>
          {/* <CardItem
              button
               onPress={() => {
               this.Active("SignUp");
                 this.props.navigation.navigate("SignUp");
               }}
            >
                <Icon
                  style={
                    this.state.screenName === "SignUp"
                      ? styles.redColor
                      : styles.greyColor
                  }
                  type="FontAwesome"
                  name="user-circle"
                />
              <Text
                style={
                  this.state.screenName === "SignUp"
                    ? styles.redColor
                    : styles.greyColor
                }
              >
               Admin Login
            </Text>
          </CardItem> */}
        </Content>
        <Footer style={styles.content}>
          <View>
            <Body>
              <Title style={styles.title}>
                Heart<Text style={styles.text}>S</Text>tarter
              </Title>
            </Body>
          </View>
        </Footer>
      </Container>
    );
  }
}
