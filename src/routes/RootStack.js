import { createStackNavigator } from "react-navigation";
import HomeScreen from "../componants/HomeScreen";
import SiderBar from "../componants/SideBar";
import CPR from "../pages/CPR_Guide";
import CPR_Detail from "../pages/CPR_Detail";
import AddDevice from "../pages/Add_Device";
import AddMoreDevice from "../pages/Add_More_Device";
import Profile from "../componants/Profile";
import MapFinder from "../pages/MapFinder";
import ActiveOnMap from "../pages/AvailableOnMap";
import SignUp from "../pages/Signup";
import AddDeviceMap from "../pages/Add_DeviceMap";
import { FirstHeader , SecondHeader } from '../componants/CommonHeader';
import React ,{ Component } from 'react';
import CheckMap from '../pages/CheckMap';
import Admin from '../pages/Adminviewpage';
import FooterPage from '../componants/FooterPage';
const RootStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: ({ navigation }) => ({
        //header: <Header/>
        // header: props => <FirstHeader {...props} />
        header:null
      })
      
    },
    SiderBar: {
      screen: SiderBar,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    CPR: {
      screen: CPR,
      navigationOptions: ({ navigation }) => ({
        // header: props => <SecondHeader {...props} />
        header: null
      })
    },
    CPR_Detail: {
      screen: CPR_Detail,
      navigationOptions: ({ navigation }) => ({
        // header: props => <SecondHeader {...props} />
        header: null
      })
    },
    AddDevice: {
      screen: AddDevice,
      navigationOptions: ({ navigation }) => ({
        // header: props => <SecondHeader {...props} />
        header: null
      })
    },
    AddMoreDevice: {
      screen: AddMoreDevice,
      navigationOptions: ({ navigation }) => ({
        // header: props => <SecondHeader {...props} />,
        header: null
      })
    },
    Profile: {
      screen: Profile,
      navigationOptions: ({ navigation }) => ({
        // header: props => <SecondHeader {...props} />
        header: null
      })
    },
    MapFinder: {
      screen: MapFinder,
      navigationOptions: ({ navigation }) => ({
        // header: props => <FirstHeader {...props} />
        header:null
      })
    },
    ActiveOnMap: {
      screen: ActiveOnMap,
      navigationOptions: ({ navigation }) => ({
        // header: props => <FirstHeader {...props} />
        header:null
      })
    },
    AddDeviceMap: {
      screen: AddDeviceMap,
      navigationOptions: ({ navigation }) => ({
        // header: props => <SecondHeader {...props} />
        header:null
      })
    },
    SignUp: {
      screen: SignUp,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },

    FooterPage: {
      screen: FooterPage,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    CheckMap: {
      screen: CheckMap,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    Admin: {
      screen: Admin,
      navigationOptions: ({ navigation }) => ({
        // header: props => <SecondHeader {...props} />
          header: null
      })
    },
  },
  
  {
    initialRouteName: 'Home'
  }
);


export default RootStack;
