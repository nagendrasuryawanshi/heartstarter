import Config from "../config";
const URL = Config.key.HS_API_URL + Config.key.HS_API_VERSION;
//const URL = Config.key.HS_API_URL;
import RestClient from "react-native-rest-client";
import { PermissionsAndroid } from "react-native";
import RNImmediatePhoneCall from "react-native-immediate-phone-call";
import VersionCheck from "react-native-version-check";

const getCurrentVersion = VersionCheck.getCurrentVersion();

export default class heartStarterAPI extends RestClient {
  constructor() {
    super(URL);
  }

  getReceivedDevices() {
    return this.GET("/device/getAll");
  }

  getApiVersion() {
    // console.log(
    //   "url",
    //   URL + "/apiversion/getApiVersion?apiVersion=" + Config.key.HS_API_VERSION
    // );
    return this.GET(
      "/apiversion/getApiVersion?apiVersion=" + Config.key.HS_API_VERSION
    );
  }
  getUserType() {
    return this.GET("/roles/getAll");
  }

  insertUser(data) {
    return this.POST("/users/save", data);
  }
  AddDevice(data) {
    return this.POST("/device/save", data);
  }

  getAllUsers() {
    return this.GET("/users/getAll");
  }

  addDeviceType() {
    return this.GET("/deviceType/getAll");
  }

  getDeviceById(uid) {
    //console.log("getDeviceById", uid);

    return this.GET("/device/getUserDevice?uid=" + uid);
  }

  getUserDeviceById(deviceId) {
    // console.log("url",URL+ "/v1/users/get?deviceId=" + deviceId)
    return this.GET("/users/get?deviceId=" + deviceId);
  }

  getApplicationVersion() {
    return this.GET(
      `/appversion/checkAppVersion?appVersion=${getCurrentVersion}&apiVersion=${
        Config.key.HS_API_VERSION
      }`
    );
  }
  adminLogin(data) {
    return this.POST("/admin/login", data);
  }
  userStatus(id, data) {
    console.log(data);
    return this.POST("/users/update/" + id, data);
  }
  getAllSteps(deviceId) {
    return this.GET("/cprsteps/getall");
  }

  sosRequestSent(data) {
    return this.POST("/sosrequest/save", data);
  }

  // getUserDeviceInfo(){
  //   return this.GET("/v1/appdevice/getall")
  //    }

  getUserDeviceInfo(deviceId) {
    return this.GET("/platform/getbydeviceId?deviceId=" + deviceId);
  }

  appInfoSave(data) {
    return this.POST("/platform/save", data);
  }
  sosStatusUpdate(id, data) {
    console.log(data);
    return this.POST("/sosrequest/update/" + id, data);
  }
  calling() {
    //alert("call")
    try {
      const granted = PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CALL_PHONE
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        RNImmediatePhoneCall.immediatePhoneCall(Config.key.HS_PHONE_NUMBER);

        console.log("You can use the call");
      } else {
        RNImmediatePhoneCall.immediatePhoneCall(Config.key.HS_PHONE_NUMBER);
        console.log("Call permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }
  locationPemision() {
    try {
      const granted = PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the location");
      } else {
        console.log("location permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  phoneNumberPemision() {
    try {
      const granted = PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the phone number");
      } else {
        console.log("location permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }
}
