import React, { Component } from "react";
import { StyleSheet, Text, TouchableHighlight, View, ToastAndroid } from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Content,
  Form,
  Item,
  Input,
  Label,
  Title,
  Subtitle, Spinner, Card, CardItem
} from "native-base";
import translations from "../utils/language.utils";

import ImagePicker from 'react-native-image-picker';
import API from "../api/Apis";
const Apis = new API();


export default class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: null,
      loading: false
    };

  }

  setSpinner(st) {
    this.setState({ loading: st })
  }

  showToast(message) {
    ToastAndroid.showWithGravityAndOffset(
      message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50
    );
  }



componentWillMount(){
 
  this.setState({username:null,password:null})
   
}

  submit() {

    this.setSpinner(true)
    switch (true) {
      case !this.state.username &&
        !this.state.password:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_ProfileFillAllDetail")}`);
        break;
      case !this.state.username:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_Username")}`);
        break;
      case !this.state.password:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_Password")}`);
        break;
      default:

        data = {
          "username": this.state.username,
          "password": this.state.password
        }
        console.log("data", data);

        Apis.adminLogin(data).then((res) => {

          console.log("loginresponse", res);
          if(res.status==400){
            this.setSpinner(false);
            this.showToast(res.error);


          }else{

            this.setSpinner(false)
            this.props.navigation.navigate("Admin");
            this.showToast(`${translations("TOAST_SuccesssLogin")}`);
  
          }

       
        }, (error) => {
          alert('No internet connection!'); console.log(error)
        });


    }


  }
  render() {
    return (
      <Container style={{ backgroundColor: '#f6e2e4' }}>
        {/* <Header
          style={styles.header}
          statusBarProps={{ barStyle: "light-content" }}
          barStyle="light-content"
        >
          <Body>
            <Title style={{ color: "#000" }}>Login</Title>
            <Subtitle style={{ color: "#000", marginRight: "5%" }}>
              Heart
              <Text style={{ color: "rgb(215, 37, 59)" }}>S</Text>
              tarter
            </Subtitle>
            <Body>
            </Body>
          </Body>
          <Right>
            <Button transparent iconRight light>

              <Icon
                type='FontAwesome'
                name="user"
                style={{ color: "#000" }}
              />
              <Text>Sing Up</Text>
            </Button>
          </Right>
        </Header> */}
        <Content>
          <Body>
            <Title style={{ color: "#000", marginTop: 70, alignItems: 'center' }}>Login</Title>
            <Subtitle style={{ color: "#000", alignItems: 'center' }}>
              Heart
              <Text style={{ color: "rgb(215, 37, 59)" }}>S</Text>
              tarter
            </Subtitle>
          </Body>
          <Card style={{ marginTop: 30, marginLeft: 20, marginRight: 20 }}>

            {/* <Form> */}
            {/* <Item inlineLabel last>
              <Label style={{ color: 'rgb(215, 37, 59)' }}>First Name</Label>
              <Input keyboardType='default' placeholder='enter first name' onChangeText={(text) => this.setState({ firstName: text })} value={this.state.firstName} />
            </Item>
            <Item inlineLabel last>
              <Label style={{ color: 'rgb(215, 37, 59)' }}>Last Name</Label>
              <Input keyboardType='default' placeholder='enter last name' onChangeText={(text) => this.setState({ lastName: text })} value={this.state.lastName} />
            </Item>
            <Item inlineLabel last>
              <Label style={{ color: 'rgb(215, 37, 59)' }}>Email</Label>
              <Input keyboardType='default' placeholder='enter email' onChangeText={(text) => this.validate(text)} value={this.state.email} />
            </Item> */}
            <Item inlineLabel last>
              <CardItem>
                <Label style={{ color: 'rgb(215, 37, 59)' }}>{translations("Username")}</Label>
                <Input keyboardType='default' placeholder={translations("TOAST_Username")} onChangeText={(text) => this.setState({ username: text })} value={this.state.username} />

              </CardItem>
            </Item>
            <Item inlineLabel last>
              <CardItem>
                <Label style={{ color: 'rgb(215, 37, 59)' }}>{translations("Password")}</Label>
                <Input keyboardType='default' placeholder={translations("TOAST_Password")} secureTextEntry={true} onChangeText={(text) => this.setState({ password: text })} value={this.state.password} />
              </CardItem>
            </Item>

            {/* <Item inlineLabel last>
              <Label style={{ color: 'rgb(215, 37, 59)' }}>Add Picture</Label>
              <Input disabled />
              <Icon onPress={this.selectPhotoTapped.bind(this)} name='camera' type='FontAwesome' />
            </Item> */}
            {/* {this.state.imageName && this.state.imageName != null ? (
              <Card style={{ padding: 10, alignItems:'center' }}>
                <Image source={{ uri: this.state.imageName }} style={{ height: 60, width: 60, flex: 1 }} />
              </Card>
            ) : null} */}
            {/* </Form> */}
            <CardItem >
              <Button onPress={() => this.submit()}
                style={{ marginTop: 20, marginBottom: 20, alignSelf: 'center', marginLeft: '35%', backgroundColor: 'rgb(215, 37, 59)', borderRadius: 10, paddingBottom: 20, paddingTop: 20, paddingRight: 15, paddingLeft: 15 }} small success>
                <Text style={{ color: '#fff' }}>{translations("Login_Button")}</Text>
              </Button>
            </CardItem>
          </Card>
        </Content>


        {this.state.loading ?
          <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)', alignItems: 'center', justifyContent: 'center', position: 'absolute', width: '100%', height: '100%' }}>
            <Spinner color="rgb(215, 37, 59)" />
          </View> : null}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgb(249, 249, 249)"
  },
  button: {
    backgroundColor: "rgb(215, 37, 59)",
    margin: 10,
    borderRadius: 10,
    alignItems: "center"
  },
  buttonText: {
    color: "#fff",
    margin: 10
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  header: {
    backgroundColor: "#fff",
    elevation: 1
  },
  testInput: {
    color: "rgb(215, 37, 59)",
    textAlign: "center"
  },
  footer: {
    margin: 10,
    backgroundColor: "#fff",
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  dot: {
    height: 65,
    width: 65,
    backgroundColor: "rgb(249, 249, 249)",
    borderRadius: 50,
    marginTop: "-30%",
    marginLeft: 16
    //display:'flex',
    // marginBottom:'15%'
    // border-radius: 50%;
    // display: inline-block;
  }
});
