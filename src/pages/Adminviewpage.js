import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Dimensions,ScrollView } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Footer, Content, List, ListItem, Spinner, Card, CardItem, Row, Col } from 'native-base';
import Config from '../config'
import API from "../api/Apis";
import { SecondHeader } from "../componants/CommonHeader";
const Apis = new API();
const screen = Dimensions.get('window');
export default class Adminviewpage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      loading: false

    }
  }
  setSpinner(st) {
    this.setState({ loading: st });
  }

  showToast(message) {
    ToastAndroid.showWithGravityAndOffset(
      message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50
    );
  }
  userData() {

    Apis.getAllUsers().then(response => {
      // console.log("user Response", response);
      this.setState({ users: response.result })
    })
      .catch(err => {
        console.log("user Err", err);
      });
  }
  componentWillMount() {
    this.userData()

  }

  statusUpdate(status, id) {
    this.setSpinner(true);
    // console.log("status",status,id);
    data = {
      verified: status,
    }
    Apis.userStatus(id, data).then(response => {
      // console.log("statust Response", response);
      if (response.message == 'User Updated') {
        this.setSpinner(false);

        this.userData()
      }

    })
      .catch(err => {
        console.log("user Err", err);
      });

  }


  render() {

    return (
      <Container style={{backgroundColor:'#fff'}}>
        <SecondHeader {...this.props} />
        <ScrollView>
        <Content style={{paddingTop: screen.height / 6,backgroundColor:'#fff'}}>
          <List>
            {this.state.users.map((item, index) => {
              return (
                <ListItem key={index}>
                  <Left>
                    <Text>{item.firstName} {item.lastName}</Text>
                  </Left>
                  <Right>

                    <View style={[item.verified == true ? styles.playColor : styles.stopColor]} ><Text style={{ color: '#fff', padding: 0 }} onPress={() => this.statusUpdate(!item.verified, item._id)}>{item.verified == true ? 'verified' : 'unverified'}</Text></View>
                  </Right>
                </ListItem>
              )
            })}
          </List>
        </Content>
        </ScrollView>
        {this.state.loading ? (
          <View
            style={{
              flex: 1,
              backgroundColor: "rgba(0,0,0,0.4)",
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              width: "100%",
              height: "100%"
            }}
          >
            <Spinner color="rgb(215, 37, 59)" />
          </View>
        ) : null}
      </Container>
    );
  }
}



const styles = StyleSheet.create({

  playColor: {
    backgroundColor: 'green',
    padding: 6,
    borderRadius: 15

  },
  stopColor: {
    backgroundColor: 'red',
    padding: 4,
    borderRadius: 15

  }

})