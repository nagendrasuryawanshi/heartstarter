/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ListView,
  Image,
  ToastAndroid
} from "react-native";
import {
  Container,
  Button,
  Icon,
  Content,
  CardItem,
  Col,
  Row,
  List,
  Card,
  Body
} from "native-base";
import Config from "../config";
import FooterPage from "../componants/FooterPage";
import * as Animatable from "react-native-animatable";
import { SecondHeader } from "../componants/CommonHeader";
import API from "../api/Apis";

const Apis = new API();
const screen = Dimensions.get("window");
const datas = ["Slide to Cancel SOS Signal"];

class BlinkingClass extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showText: true
    };

    // Change the state every second or the time given by User.
    setInterval(
      () => {
        this.setState(previousState => {
          return { showText: !previousState.showText };
        });
      },
      // Define any blinking time.
      1000
    );
  }

  render() {
    let display = this.state.showText ? this.props.text : " ";
    return (
      <Text style={{ textAlign: "center", marginTop: 10 }}>{display}</Text>
    );
  }
}

export default class MapFinder extends Component {
  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    this.state = {
      open: false,
      basic: true,
      listViewData: datas,
      time: "",
      swipeLeft: false,
      statuschange: null
    };
  }

  showToast(message) {
    ToastAndroid.showWithGravityAndOffset(
      message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50
    );
  }
  setOpen(st) {
    this.setState({ open: st });
  }
  deleteRow(secId, rowId, rowMap) {
    rowMap[`${secId}${rowId}`].props.closeRow();
    const newData = [...this.state.listViewData];
    newData.splice(rowId, 1);
    this.setState({ listViewData: newData });
  }
  componentWillMount() {
    // Creating variables to hold time.

    const Data = this.props.navigation.getParam("Data", Data);
    console.log("data", Data);

    Apis.sosRequestSent(Data).then(
      res => {
        const result = res.SOSRequest.requestTime;
        console.log("result1", res);
        var data = {
          id: res.SOSRequest._id,
          status: "Cancel"
        };
        this.setState({ statuschange: data });
        //console.log("result",  Date(result));
      },
      error => {
        console.log("error", error);
      }
    );
  }
  swipeLeft() {
    this.setState({ swipeLeft: true });
  }
  ////////sorequeststatusupdate/////////

  status(status) {
    this.showToast("Request canceled successfylly.");
    console.log("status", status);
    var id = status.id,
      data = {
        status: status.status
      };
    if (status) {
      Apis.sosStatusUpdate(id, data).then(
        result => {
          console.log("cancel status", result);
          //console.log("result",  Date(result));
        },
        error => {
          console.log("error", error);
        }
      );
    }
  }
  render() {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    return (
      <Container style={{ backgroundColor: "#f9f9f9" }}>
        <SecondHeader {...this.props} />
        <Content style={{ backgroundColor: "#f9f9f9", paddingTop: screen.height / 5, top: -1 }}>
          <List
            style={{
              backgroundColor: "#fff",
              width: screen.width - 30,
              borderRadius: 10,
              elevation: 3,
              alignSelf: "center",
              // marginTop: screen.height / 8 - 70
            }}
            leftOpenValue={75}
            rightOpenValue={-75}
            dataSource={this.ds.cloneWithRows(this.state.listViewData)}
            renderRow={data => (
              <Row style={{ borderRadius: 10 }}>
                <Col
                  style={{
                    paddingBottom: 12,
                    paddingTop: 18,
                    borderRadius: 10,
                    marginLeft: 15
                  }}
                >
                  <Text style={{ alignSelf: "center" }}> {data} </Text>
                </Col>
                <Col
                  style={{
                    paddingBottom: 12,
                    paddingTop: 12,
                    backgroundColor: "rgb(215, 37, 59)",
                    width: 70,
                    borderRadius: 10,
                    marginRight: 1,
                    marginTop: 1,
                    marginBottom: 1
                  }}
                >
                  <Icon
                    type="Entypo"
                    name="heart"
                    style={{ color: "#fff", alignSelf: "center", fontSize: 35 }}
                  />
                </Col>
              </Row>
            )}
            renderLeftHiddenRow={data => (
              <Button
                style={{
                  borderRadius: 10,
                  backgroundColor: "rgb(215, 37, 59)",
                  marginLeft: 1,
                  marginTop: 1,
                  marginBottom: 1
                }}
                full
                onPress={() => {
                  this.props.navigation.navigate("Home"),
                    this.status(this.state.statuschange);
                }}
              >
                <Icon
                  style={{ color: "#fff", fontWeight: "bold" }}
                  name="trash"
                />
              </Button>
            )}
            //  renderRightHiddenRow={(data, secId, rowId, rowMap) =>
            //     <Button style={{borderRadius:10 ,backgroundColor:'rgb(215, 37, 59)',marginRight:1,marginTop:1,marginBottom:1}}  full onPress={() =>  this.props.navigation.navigate('Home')}>
            //        <Icon style={{color:'#fff',fontWeight:'bold'}} name="trash" />
            //       </Button>}
            disableLeftSwipe={true}
          />

          <View
            style={{ marginTop: screen.height / 8 - 50, alignSelf: "center" }}
          />
          <View
            style={{
              alignItems: "center",
              alignSelf: "center",
              // backgroundColor: "rgb(215, 37, 59)",
              // height: 40,
              // width: 40,
              // borderRadius: 20
            }}
          >
            {/* <Animatable.Text
              style={{ alignItems: "stretch", marginTop: 5 }}
              animation="zoomIn"
              iterationCount="infinite"
            >
              <Icon
                name="podcast"
                type="FontAwesome"
                style={{ color: "#fff" }}
              />
            </Animatable.Text> */}
            <Image source={require('../assets/Images/HEART1/user-gif.gif')} style={{height:40,width:40}} />
          </View>

          <View style={{ alignSelf: "center" }}>
            <Text style={{ color: "#000" }}>Signal Sent</Text>
          </View>
          <View style={{ alignSelf: "center" }}>
            <Text
              style={{
                width: 0.5,
                alignSelf: "center",
                borderColor: "rgb(215, 37, 59)",
                borderWidth: 0.5,
                height: 30
              }}
            />
          </View>
          <Row style={{ alignSelf: "center" }}>
            <Text
              style={{
                alignSelf: "center",
                borderColor: "rgb(215, 37, 59)",
                borderWidth: 0.5,
                width: "67%",
                height: 0.2
              }}
            />
          </Row>
          <Row>
            <Col>
              <Text
                style={{
                  width: 0.5,
                  alignSelf: "center",
                  borderColor: "rgb(215, 37, 59)",
                  borderWidth: 0.5,
                  height: 30
                }}
              />
              <CardItem
                style={{
                  alignSelf: "center",
                  borderWidth: 0.5,
                  borderColor: "rgb(215, 37, 59)",
                  paddingBottom: 5,
                  paddingLeft: 8,
                  paddingRight: 8,
                  paddingTop: 5,
                  height: 40,
                  width: 40,
                  borderRadius: 20
                }}
              >
                <Icon
                  name="send-o"
                  type="FontAwesome"
                  style={{ color: "rgb(215, 37, 59)" }}
                />
              </CardItem>
              <Text style={{ alignSelf: "center" }}>Dron En Route</Text>
              <Text
                style={{
                  width: 0.5,
                  alignSelf: "center",
                  borderColor: "rgb(215, 37, 59)",
                  borderWidth: 0.5,
                  height: 30
                }}
              />
              <CardItem
                style={{
                  marginTop: 5,
                  alignSelf: "center",
                  borderWidth: 0.5,
                  borderColor: "rgb(215, 37, 59)",
                  paddingBottom: 5,
                  paddingLeft: 8,
                  paddingRight: 8,
                  paddingTop: 5,
                  height: 40,
                  width: 40,
                  borderRadius: 20
                }}
              >
                <Icon
                  name="human-handsup"
                  type="MaterialCommunityIcons"
                  style={{ color: "rgb(215, 37, 59)" }}
                />
              </CardItem>
              <Text style={{ alignSelf: "center" }}>Go Retrive</Text>
            </Col>
            <Col>
              <Text
                style={{
                  width: 0.5,
                  alignSelf: "center",
                  borderColor: "rgb(215, 37, 59)",
                  borderWidth: 0.5,
                  height: 30
                }}
              />
              <CardItem
                style={{
                  backgroundColor: "rgb(215, 37, 59)",
                  alignSelf: "center",
                  borderWidth: 0.5,
                  borderColor: "rgb(215, 37, 59)",
                  paddingBottom: 5,
                  paddingLeft: 8,
                  paddingRight: 8,
                  paddingTop: 5,
                  height: 40,
                  width: 40,
                  borderRadius: 20
                }}
              >
                <Icon
                  name="ambulance"
                  type="MaterialCommunityIcons"
                  style={{ color: "#fff" }}
                />
              </CardItem>
              <Text style={{ alignSelf: "center" }}>911 Contacted</Text>
            </Col>
            <Col>
              <Text
                style={{
                  width: 0.5,
                  alignSelf: "center",
                  borderColor: "rgb(215, 37, 59)",
                  borderWidth: 0.5,
                  height: 30
                }}
              />
              <CardItem
                style={{
                  backgroundColor: "rgb(215, 37, 59)",
                  alignSelf: "center",
                  borderWidth: 0.5,
                  borderColor: "rgb(215, 37, 59)",
                  paddingBottom: 5,
                  paddingLeft: 8,
                  paddingRight: 8,
                  paddingTop: 5,
                  height: 40,
                  width: 40,
                  borderRadius: 20
                }}
              >
                <Icon
                  name="user-md"
                  type="FontAwesome"
                  style={{ color: "#fff" }}
                />
              </CardItem>
              <Text style={{ alignSelf: "center" }}>Helpers En Route</Text>
              <Text
                style={{
                  width: 0.5,
                  alignSelf: "center",
                  borderColor: "rgb(215, 37, 59)",
                  borderWidth: 0.5,
                  height: 30
                }}
              />
              <CardItem
                style={{
                  marginTop: 5,
                  alignSelf: "center",
                  borderWidth: 0.5,
                  borderColor: "rgb(215, 37, 59)",
                  paddingBottom: 5,
                  paddingLeft: 8,
                  paddingRight: 8,
                  paddingTop: 5,
                  height: 40,
                  width: 40,
                  borderRadius: 20
                }}
              >
                <Icon
                  name="street-view"
                  type="FontAwesome"
                  style={{ color: "rgb(215, 37, 59)" }}
                />
              </CardItem>
              <Text style={{ alignSelf: "center" }}>Arrived</Text>
            </Col>
          </Row>
        </Content>
        <FooterPage {...this.props} />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  rowChange: {
    backgroundColor: "transparent"
  },

  footer: {
    margin: 10,
    backgroundColor: "#fff",
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  dot: {
    height: 65,
    width: 65,
    backgroundColor: "rgb(249, 249, 249)",
    borderRadius: 50,
    marginTop: "-10%",
    alignSelf: "center"
  }
});
