import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  Picker,
  View,
  ToastAndroid,
  TouchableOpacity,
  Image,
  ImageBackground,
  AsyncStorage,
  Alert,
  Linking,
  Dimensions,
  ScrollView
} from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Content,
  Form,
  Item,
  Input,
  Label,
  Title,
  Card,
  CardItem,
  Row,
  Spinner,
  Col
} from "native-base";
import ImagePicker from "react-native-image-picker";
import API from "../api/Apis";
import { PermissionsAndroid } from "react-native";
import Config from "../config";
import {
  GoogleAnalyticsSettings,
  GoogleAnalyticsTracker
} from "react-native-google-analytics-bridge";
import DeviceInfo from "react-native-device-info";
import translations from "../utils/language.utils";
import { SecondHeader } from "../componants/CommonHeader";

let tracker = new GoogleAnalyticsTracker("UA-128498782-1");
const screen = Dimensions.get('window');
const URL = Config.key.HS_API_URL + Config.key.HS_API_VERSION;
const Apis = new API();
const myApiKey = Config.key.MAP_API_KEY;

export default class AddDevice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      myLat: null,
      myLon: null,
      address: "",
      mobileNumber: "",
      latitude: "",
      longitude: "",
      note: "",
      loading: false,
      avatarSource1: "",
      avatarSource2: "",
      avatarSource3: "",
      device: "",
      Add: [],
      userData: [],
      Device: [],
      VERSION: null,
      moreImage1: [],
      moreImage2: [],
      moreImage3: [],
      loading: false
    };
  }

  setSpinner(st) {
    this.setState({ loading: st });
  }

  componentWillMount() {
    this.setState({ moreImage1: "" });
    AsyncStorage.getItem("USER", (err, result) => {
      // console.log("USER", JSON.parse(result));
      if (result != null) {
        this.setState({ userData: JSON.parse(result).User });
        // console.log("USER", JSON.parse(result));
      }
    });

    Apis.addDeviceType()
      .then(response => {
        // console.log("Device Response", response);

        this.setState({ Device: response.DeviceType });
      })
      .catch(err => {
        console.log("Device Err", err);
      });

    AsyncStorage.getItem("VERSION", (err, result) => {
      // console.log("VERSION", result);
      if (result === "versionMisMatch") {
        this.setState({ VERSION: result });
      } else {
        this.setState({ VERSION: null });
      }
    });
    this.setState({
      address: "",
      mobileNumber: "",
      device: "",
      note: "",
      moreImage1: "",
      moreImage2: "",
      moreImage3: "",
      longitude: "",
      latitude: "",
      avatarSource1: "",
      avatarSource2: "",
      avatarSource3: ""
    });
  }

  showToast(message) {
    ToastAndroid.showWithGravityAndOffset(
      message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50
    );
  }

  Submit() {
    const { navigation } = this.props;
    const ADD = navigation.getParam("ADD", ADD);
    // console.log("this.state.longitude", this.state.longitude);
    this.setSpinner(true);
    switch (true) {
      case !this.state.device &&
        !this.state.note &&
        !this.state.mobileNumber &&
        !this.state.moreImage2 &&
        !this.state.moreImage3 &&
        !this.state.longitude &&
        !this.state.moreImage1:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_FillAllDetail")}`);
        break;
      case !this.state.device:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_SelectDevice")}`);
        break;
      case !ADD:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_SelectLatitude")}`);
        break;
      case !this.state.note:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_EnterNotes")}`);
        break;
      case !this.state.mobileNumber:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_EnterNumber")}`);
        break;
      case this.state.mobileNumber.length <= 9:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_InvalidNumber")}`);
        break;
      case !this.state.moreImage1 &&
        !this.state.moreImage2 &&
        !this.state.moreImage3:
        this.setSpinner(false);
        this.showToast(`${translations("TOAST_UploadImage")}`);
        break;
      default:
        console.log("this.state.device", this.state.device);
        // console.log("ADD", ADD.longitude);

        data = {
          address: this.state.address,
          phoneNumber: this.state.mobileNumber,
          device: this.state.device,
          locationNote: this.state.note,
          image1: this.state.moreImage1,
          image2: this.state.moreImage2,
          image3: this.state.moreImage3,
          longitude: ADD.longitude,
          latitude: ADD.latitude,
          // longitude:77.43579838424921,
          // latitude: 23.22804518761864,
          uid: this.state.userData._id,
          deviceTypeId: this.state.device
        };
        console.log("data", data);
        // const photo = {
        //   uri: this.state.imageName,
        //   type: "image/jpeg",
        //   name: this.state.fileName
        // };
        const form = new FormData();
        form.append("address", data.address);
        form.append("device", data.device);
        form.append("phoneNumber", data.phoneNumber);
        form.append("locationNote", data.locationNote);
        if (data.image1) {
          form.append("image1", data.image1);
        }
        if (data.image2) {
          form.append("image2", data.image2);
        }
        if (data.image3) {
          form.append("image3", data.image3);
        }
        form.append("longitude", data.longitude);
        form.append("latitude", data.latitude);
        form.append("uid", data.uid);
        form.append("deviceTypeId", data.deviceTypeId);
        // console.log("formData", form);

        fetch(URL + "/device/save", {
          body: form,
          method: "POST",
          headers: {
            "Content-Type": "multipart/form-data"
          }
        })
          // .then(response => response.json())
          // .catch(error => {
          //   this.setSpinner(false);
          //   // this.props.navigation.navigate("AddDevice");
          //   console.log("response of  not craete User", error);
          //   this.showToast(`${translations("TOAST_NetworkErr")}`);
          // })
          .then(responseData => {
            // console.log("response", responseData);
            this.setSpinner(false);
            // if (responseData && responseData.message) {
            // tracker.trackException(responseData.message.message, false);

            // console.log("adddevicerror");

            // this.showToast("priyanka:"+responseData.message.message);

            // } else {
            // GoogleAnalyticsSettings.setDispatchInterval(2);
            tracker.trackEvent("Button", "onPress");
            // console.log("success");
            this.setState({
              address: "",
              mobileNumber: "",
              device: "",
              note: "",
              moreImage1: "",
              moreImage2: "",
              moreImage3: "",
              longitude: "",
              latitude: "",
              avatarSource1: "",
              avatarSource2: "",
              avatarSource3: ""
            });

            this.setSpinner(false);
            //this.showToast(`${translations("TOAST_Create")}`);
            this.props.navigation.navigate("AddMoreDevice");
            // }
          })
          .catch(error => {
            this.setState({
              address: "",
              mobileNumber: "",
              device: "",
              note: "",
              moreImage1: "",
              moreImage2: "",
              moreImage3: "",
              longitude: "",
              latitude: "",
              avatarSource1: "",
              avatarSource2: "",
              avatarSource3: ""
            });

            // longitude:77.43579838424921,
            // latitude: 23.22804518761864,

            this.setSpinner(false);
            // this.props.navigation.navigate("AddDevice");
            tracker.trackException("error", false);
            // console.log("response of  not craete User", error);
            //this.showToast("Ooops somthing network error!");
            this.props.navigation.navigate("AddMoreDevice");
          });
    }
  }

  selectPhotoTapped1() {
    const options = {
      title: "Select Avatar",
      customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      // console.log("Response = ", response);

      if (response.didCancel) {
        // console.log("User cancelled image picker");
      } else if (response.error) {
        // console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        // console.log("User tapped custom button: ", response.customButton);
      } else {
        const source = { uri: response };
        const avatar = "data:image/jpeg;base64," + response.data;
        // console.log("source1", 'data:image/jpeg;base64,' + response.data)
        this.setState({ avatarSource1: avatar });
        const photo = {
          uri: response.uri,
          type: "image/jpeg",
          name: response.fileName
        };
        this.setState({ moreImage1: photo });
      }
    });
  }

  selectPhotoTapped2() {
    const options = {
      title: "Select Avatar",
      customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      // console.log("Response = ", response);

      if (response.didCancel) {
        // console.log("User cancelled image picker");
      } else if (response.error) {
        // console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        // console.log("User tapped custom button: ", response.customButton);
      } else {
        const source = { uri: response };
        const avatar = "data:image/jpeg;base64," + response.data;
        // console.log("source1", 'data:image/jpeg;base64,' + response.data)
        this.setState({ avatarSource2: avatar });
        const photo = {
          uri: response.uri,
          type: "image/jpeg",
          name: response.fileName
        };
        this.setState({ moreImage2: photo });
      }
    });
  }

  selectPhotoTapped3() {
    const options = {
      title: "Select Avatar",
      customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      // console.log("Response = ", response);

      if (response.didCancel) {
        // console.log("User cancelled image picker");
      } else if (response.error) {
        // console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        //console.log("User tapped custom button: ", response.customButton);
      } else {
        const source = { uri: response };
        const avatar = "data:image/jpeg;base64," + response.data;
        // console.log("source1", 'data:image/jpeg;base64,' + response.data)
        this.setState({ avatarSource3: avatar });
        const photo = {
          uri: response.uri,
          type: "image/jpeg",
          name: response.fileName
        };
        this.setState({ moreImage3: photo });
      }
    });
  }



  componentWillUpdate() {
    AsyncStorage.getItem("VERSION", (err, result) => {
      if (result === "versionMisMatch") {
        this.setState({ VERSION: result });
      } else {
        this.setState({ VERSION: null });
      }
    });
  }



  render() {
    tracker.trackScreenView("AddDevice");
    const ADD = this.props.navigation.getParam("ADD", ADD);

    return (
      <Container style={{ backgroundColor: "#fff" }}>
        <SecondHeader {...this.props} />
        <ScrollView>
          <Content style={{ paddingTop: screen.height / 6, backgroundColor: "rgb(253, 244, 245)" }}>
            <Form>
              <View style={{ backgroundColor: "rgb(253, 244, 245)" }}>
                <Picker
                  selectedValue={this.state.device}
                  style={{
                    alignItems: 'center',
                    height: 100,
                    width: '50%',
                    marginLeft: "25%",
                    //marginRight: "4%",
                    //marginBottom:-150,
                    marginTop: 15,
                    transform: [
                      { scaleX: 1.8 },
                      { scaleY: 1.8 },
                    ]
                  }}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({ device: itemValue })
                  }
                >
                  <Picker.Item style={{ fontSize: 40 }} label={translations("ChooseDevice")} />
                  {this.state.Device.map((item, index) => {
                    return (
                      <Picker.Item
                        key={index}
                        label={item.deviceType}
                        value={item._id}
                      />
                    );
                  })}
                </Picker>
              </View>
              <Item last />
              <Item inlineLabel last style={{ borderBottomWidth: 0, backgroundColor: '#fff' }}>
                <Label>
                  <Text style={{ color: "rgb(215, 37, 59)" }}>
                    {translations("Item")}
                  </Text>{" "}
                  {translations("Location")}
                </Label>
                <Input
                  keyboardType="default"
                  multiline={true}
                  onChangeText={text => this.setState({ longitude: text })}
                  placeholder={translations("ItemLocation")}
                  // value={ADD?ADD.latitude+' '+ADD.longitude:null}
                  value={ADD ? ADD.address : null}
                />

                <Icon
                  active
                  type="Entypo"
                  name="location-pin"
                  //  onPress={() => this.getCurrentLocation(ADD)}
                  onPress={() => this.props.navigation.navigate("AddDeviceMap")}
                />
              </Item>
              <Item inlineLabel last style={{ backgroundColor: '#fff' }}>
                <Label>
                  <Text style={{ color: "rgb(215, 37, 59)" }}>
                    {translations("Location")}
                  </Text>{" "}
                  {translations("Notes")}
                </Label>
                <Input
                  keyboardType="default"
                  placeholder={translations("LocationNotes")}
                  multiline={true}
                  onChangeText={text => this.setState({ note: text })}
                  value={this.state.note}
                />
              </Item>
              <Item inlineLabel last style={{ backgroundColor: '#fff' }}>
                <Label>
                  <Text style={{ color: "rgb(215, 37, 59)" }}>
                    {translations("Phone")}
                  </Text>{" "}
                  {translations("Number")}
                </Label>
                <Input
                  keyboardType="numeric"
                  placeholder={translations("PhoneNumber")}
                  maxLength={10}
                  onChangeText={text => this.setState({ mobileNumber: text })}
                  value={this.state.mobileNumber}
                />
              </Item>
              <Item inlineLabel last style={{ borderBottomWidth: 0, backgroundColor: '#fff' }}>
                <Label>
                  <Text style={{ color: "rgb(215, 37, 59)" }}>
                    {translations("Add")}
                  </Text>{" "}
                  {translations("Picture")}
                </Label>
                <Input disabled />
              </Item>
              <Row
                style={{
                  //marginLeft: "3%",
                  // marginRight: "3%",
                  alignSelf: "center",
                  backgroundColor: '#fff'
                }}
              >
                <Col>
                  {this.state.avatarSource1 && this.state.avatarSource1 != null ? (
                    <Card style={{ padding: 10, marginLeft: 3, marginRight: 5, alignSelf: 'center' }}>
                      <Image
                        source={{ uri: this.state.avatarSource1 }}
                        style={{ height: 33, width: 60, flex: 1 }}
                      />
                    </Card>
                  ) : (
                      <TouchableOpacity onPress={this.selectPhotoTapped1.bind(this)}>
                        <Card style={{ padding: 20, marginLeft: 3, marginRight: 5, alignSelf: 'center' }}>
                          {/* <CardItem style={{alignSelf:'center'}}> */}
                          <Icon style={{ fontSize: 40 }} type="Feather" name="plus" />
                          {/* </CardItem> */}
                        </Card>
                      </TouchableOpacity>
                    )}
                </Col>
                <Col>
                  {this.state.avatarSource2 && this.state.avatarSource2 != null ? (
                    <Card style={{ padding: 10, marginLeft: 5, marginRight: 5, alignSelf: 'center' }}>
                      <Image
                        source={{ uri: this.state.avatarSource2 }}
                        style={{ height: 33, width: 60, flex: 1 }}
                      />
                    </Card>
                  ) : (
                      <TouchableOpacity onPress={this.selectPhotoTapped2.bind(this)}>
                        <Card style={{ padding: 20, marginLeft: 5, marginRight: 5, alignSelf: 'center' }}>
                          {/* <CardItem style={{}}> */}
                          <Icon style={{ fontSize: 40 }} type="Feather" name="plus" />
                          {/* </CardItem> */}
                        </Card>
                      </TouchableOpacity>
                    )}
                </Col>
                <Col>
                  {this.state.avatarSource3 && this.state.avatarSource3 != null ? (
                    <Card style={{ padding: 10, marginLeft: 5, marginRight: 3, alignSelf: 'center' }}>
                      <Image
                        source={{ uri: this.state.avatarSource3 }}
                        style={{ height: 33, width: 60, flex: 1 }}
                      />
                    </Card>
                  ) : (
                      <TouchableOpacity onPress={this.selectPhotoTapped3.bind(this)}>
                        <Card style={{ padding: 20, marginLeft: 5, marginRight: 3, alignSelf: 'center' }}>
                          {/* <CardItem style={{}}> */}
                          <Icon style={{ fontSize: 40 }} type="Feather" name="plus" />
                          {/* </CardItem> */}
                        </Card>
                      </TouchableOpacity>
                    )}
                </Col>
              </Row>
            </Form>
          </Content>
        </ScrollView>
        {this.state.VERSION ? (
          <Button
            disabled
            full
            onPress={() => this.Submit()}
            style={{
              margin: 10,
              borderRadius: 10
            }}
          >
            <Text style={{ color: "#fff" }}>
              {" "}
              {translations("Save")} & {translations("Update")}{" "}
            </Text>
          </Button>
        ) : this.state.userData.verified == false ? (
          <Button
            disabled
            full
            onPress={() => this.Submit()}
            style={{
              margin: 10,
              borderRadius: 10
            }}
          >
            <Text style={{ color: "#fff" }}>
              {" "}
              {translations("Save")} & {translations("Update")}{" "}
            </Text>
          </Button>
        ) : (
              <Button
                full
                onPress={() => this.Submit()}
                style={{
                  backgroundColor: "rgb(215, 37, 59)",
                  margin: 10,
                  borderRadius: 10
                }}
              >
                <Text style={{ color: "#fff" }}>
                  {" "}
                  {translations("Save")} & {translations("Update")}{" "}
                </Text>
              </Button>
            )}

        {this.state.loading ? (
          <View
            style={{
              flex: 1,
              backgroundColor: "rgba(0,0,0,0.4)",
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              width: "100%",
              height: "100%"
            }}
          >
            <Spinner color="rgb(215, 37, 59)" />
          </View>
        ) : null}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  footer: {
    margin: 10,
    backgroundColor: "#fff",
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  dot: {
    height: 65,
    width: 65,
    backgroundColor: "rgb(249, 249, 249)",
    borderRadius: 50,
    marginTop: "-30%",
    marginLeft: 16
    //display:'flex',
    // marginBottom:'15%'
    // border-radius: 50%;
    // display: inline-block;
  }
});
