import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity,Dimensions } from 'react-native';
import { Container, Icon, Content, Card, Row, Col } from 'native-base';
import styles from '../assets/styles/CPRGuideCSS';
import Config from '../config'
import API from "../api/Apis";
import FooterPage from '../componants/FooterPage';
import { SecondHeader } from "../componants/CommonHeader";
const path4 = require('../assets/Images/noImage.png')
const Apis = new API();
const screen = Dimensions.get('window');

export default class CPR_Guide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            steps: []
        }
    }

    componentWillMount() {
        Apis.getAllSteps().then((response) => {
            console.log('steps', response)
            this.setState({ steps: response.CPRSteps })

        }).catch((err) => {
            console.log(err)
        })
    }


    render() {
        return (
            <Container style={styles.container}>
                <SecondHeader {...this.props} />
                <Content style={styles.container}>
                    <Text style={{ color: '#000', alignSelf: 'center', fontSize: 18, marginBottom: 10, paddingTop:screen.height / 5.5 }}>CPR Guide</Text>
                    {this.state.steps.map((item, index) => {
                        return (
                            <TouchableOpacity style={{ borderRadius: 10 }} key={index} onPress={() => this.props.navigation.navigate('CPR_Detail', { CPR: index + 1 })}>

                                <Card style={{
                                    borderRadius: 10,
                                    marginLeft: 18,
                                    marginRight: 18,
                                    elevation: 2,
                                    shadowColor: 'rgba(0,0,0,0.6)',
                                    shadowOffset: { height: 5, width: 0 },
                                    shadowRadius: 10,
                                    //borderColor:'#F4F4F4'
                                }}
                                >
                                    <Row style={{ borderBottomStartRadius: 10, borderTopStartRadius: 10 }}>
                                        <Col style={styles.cardCol}>
                                            <Text style={styles.cardColText}>Step {index + 1}</Text>
                                        </Col>
                                        <Col style={styles.cardColFirst}>
                                            <View style={styles.cardView}>
                                                {item.stepImage ? (
                                                    <Image source={{ uri: Config.key.HS_API_URL + item.stepImage }} style={styles.image} />
                                                ) :
                                                    <Image source={path4} style={styles.image} />
                                                }
                                            </View>
                                        </Col>
                                        <Col style={styles.cardColSecond}>
                                            <Icon style={styles.cardIcon} name="chevron-right" type='FontAwesome' />
                                        </Col>
                                    </Row>
                                </Card>
                            </TouchableOpacity>
                        )
                    })}
                </Content>
                <FooterPage {...this.props} />
            </Container>
        );
    }
}


