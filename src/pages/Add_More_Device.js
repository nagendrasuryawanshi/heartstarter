import React, { Component } from 'react';
import { StyleSheet, Text, View, ImageBackground } from 'react-native';
import { Container, Header, Left, Body, Button, Icon, Title, Content } from 'native-base';
import Config from '../config'
import { SecondHeader } from "../componants/CommonHeader";
// import AddDevice from './Add_Device';


export default class AddMoreDevice extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    reload() {
        this.props.navigation.navigate('AddDevice', { ADD: '' })
    }

    render() {
        return (
            <Container style={{ backgroundColor: 'rgb(215, 37, 59)' }}>
                <SecondHeader {...this.props} />
                <Content style={{ backgroundColor: 'rgb(215, 37, 59)' }}>
                    <View style={{ alignSelf: 'center', marginTop: 10 }}>
                        <Text style={{ color: '#fff', alignSelf: 'center' }}>Thank you for making the</Text>
                        <Text style={{ color: '#fff', alignSelf: 'center' }}>World a better place!</Text>
                        <Icon type="FontAwesome" name="check" style={{ color: '#fff', marginTop: '40%', alignSelf: 'center', fontSize: 50 }} />
                        <Text style={{ fontSize: 20, color: '#fff', marginTop: '5%', alignSelf: 'center' }}> DEVICE ADDED! </Text>
                    </View>
                </Content>
                <Button full onPress={() => this.reload()} style={{ margin: 10, borderColor: '#fff', borderWidth: 1, backgroundColor: 'rgb(215, 37, 59)' }}>
                    <Text style={{ alignSelf: 'center', color: '#fff' }}>Add More</Text>
                </Button>


            </Container>
        );
    }
}


