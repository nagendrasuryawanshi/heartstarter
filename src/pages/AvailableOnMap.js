import React, { Component } from "react";
import {
  Text,
  View,
  TouchableHighlight,
  AsyncStorage,
  Alert
} from "react-native";
import {
  Container,
  Body,
  Button,
  Icon,
  CardItem,
  Col,
  Row,
  Item,
  Spinner
} from "native-base";
import MapView from "react-native-maps";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import API from "../api/Apis";
import Polyline from "@mapbox/polyline";
import distance from "react-native-google-matrix";
import RNImmediatePhoneCall from "react-native-immediate-phone-call";
import Config from "../config";
import DeviceInfo from "react-native-device-info";
import styles from "../assets/styles/AvailableOnMapCSS";
import FooterPage from "../componants/FooterPage";
import {FirstHeader} from "../componants/CommonHeader";

const myApiKey = Config.key.MAP_API_KEY;
distance.apiKey = myApiKey;

const Apis = new API();
export default class ActiveOnMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      geoLoc: [],
      isLoading: false,
      srviceKit: [],
      lastPosition: "unknown",
      display: [],
      isLoading: false,
      is_searching: false,
      error: null,
      coords: [],
      concat: null,
      latitude: null,
      longitude: null,
      x: "false",
      userData: [],
      cordLatitude: null,
      cordLongitude: null,
      showPlacesList: false,
      startMarker: ""
    };
  }

  checkProfileStatus() {
    const getUniqueID = DeviceInfo.getUniqueID();
    Apis.getUserDeviceById(getUniqueID)
      .then(responseData => {
        if (responseData != null && responseData != "") {
          if (responseData.message) {
            console.log("part1 ", responseData);
            this.setState({ profileStatus: true });
            Alert.alert(
              "",
              "Please Fill your profile details",
              [
                {
                  text: "OK",
                  onPress: () => this.props.navigation.navigate("Profile")
                }
              ],
              { cancelable: false }
            );
          } else {
            console.log("device available");
          }
        }
      })
      .catch(err => {
        console.log("err", err);
      });
  }

  setSpinner = st => {
    this.setState({ isLoading: st });
  };
  calling() {
    try {
      const granted = PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CALL_PHONE
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        RNImmediatePhoneCall.immediatePhoneCall(Config.key.HS_PHONE_NUMBER);
      } else {
        RNImmediatePhoneCall.immediatePhoneCall(Config.key.HS_PHONE_NUMBER);
      }
    } catch (err) {
      console.warn(err);
    }
  }

  ////////////get current location lat long and devices////////////////
  componentWillMount() {
    this.checkProfileStatus();
    AsyncStorage.setItem("UID123", "", () => {
      this.setState({ display: "" });
    });
  }

  componentDidMount() {
    this.setSpinner(true);
    navigator.geolocation.getCurrentPosition(position => {
      console.log(
        `Got location: ${position.coords.latitude}, ${
          position.coords.longitude
        }`
      );
      let region = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        latitudeDelta: 0.00922 * 1.5,
        longitudeDelta: 0.00421 * 1.5
      };
      this.setState({ geoLoc: region });
    });

    AsyncStorage.getItem("USER", (err, result) => {
      console.log("USER", JSON.parse(result));
      this.setState({ userData: JSON.parse(result) });
    });
    Apis.getReceivedDevices().then(
      res => {
        console.log("deviceData: res[0]", res);
        this.setState({ srviceKit: res });
      },
      error => {
        alert("No internet connection!");
      }
    );
    AsyncStorage.getItem("UID123", (err, result) => {
      this.setState({ display: JSON.parse(result) });
    });
  }
  ////////////get current location lat long//////////////////

  mergeLot(data) {
    let destinationlat = data.geometry.location.lat;
    let destinationlng = data.geometry.location.lng;
    data = {
      lat: destinationlat,
      long: destinationlng
    };
    this.setState({
      cordLatitude: data.lat,
      cordLongitude: data.long
    });
    console.log(
      "destinationlat",
      destinationlat,
      "destinationlng",
      destinationlng
    );
    this.getDirections(data);
    this.duration(data);
  }

  async getDirections(concatLot) {
    this.setSpinner(true);
    let destinationLoc = concatLot.lat + "," + concatLot.long;
    let startLoc =
      this.state.geoLoc.latitude + "," + this.state.geoLoc.longitude;
    try {
      // String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters + "&key=" + MY_API_KEY
      let resp = await fetch(
        `https://maps.googleapis.com/maps/api/directions/json?origin=${startLoc}&destination=${destinationLoc} ` +
          "&key=" +
          myApiKey
      );
      let respJson = await resp.json();
      let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
      let coords = points.map((point, index) => {
        return {
          latitude: point[0],
          longitude: point[1]
        };
      });
      this.setState({ coords: coords });
      AsyncStorage.getItem("UID123", (err, result) => {
        this.setState({ display: JSON.parse(result) });
        if (this.state.display != null) {
          this.setSpinner(false);
        }
      });
      this.setState({ x: "true" });
      return coords;
    } catch (error) {
      this.setSpinner(false);
      this.setState({ x: "error" });
      return error;
    }
  }

  markView(marker) {
    console.log("marker", marker);

    let region = {
      latitude: marker.latitude,
      longitude: marker.longitude,
      latitudeDelta: 0.00922 * 1.5,
      longitudeDelta: 0.00421 * 1.5
    };
    this.setState({ geoLoc: region });
  }

  duration(concatLot) {
    this.setSpinner(true);
    let destinationLoc = concatLot.lat + "," + concatLot.long;
    let startLoc =
      this.state.geoLoc.latitude + "," + this.state.geoLoc.longitude;
    // console.log("startLocdurat:", startLoc, "destinationLocdurat:", destinationLoc);
    distance.get(
      {
        index: 1,
        origin: startLoc,
        destination: destinationLoc
      },
      function(err, data) {
        AsyncStorage.setItem("UID123", JSON.stringify(data), () => {
          AsyncStorage.mergeItem("UID123", JSON.stringify(data), () => {
            AsyncStorage.getItem("UID123", (err, result) => {});
          });
        });
      }
    );
  }

  render() {
    return (
      <Container style={styles.mainContainer}>
      <FirstHeader {...this.props}/>
        {this.state.isLoading ? (
          <View style={styles.spinnerView}>
            <Spinner
              visible={this.state.isLoading}
              textContent={"Looking for services..."}
              textStyle={styles.spinnerTextColor}
              color={styles.sppinnerColor.toString()}
            />
          </View>
        ) : null}
        <View style={styles.container}>
          {this.state.geoLoc != null &&
            this.state.geoLoc.latitude != null &&
            this.state.geoLoc.longitude != null &&
            this.state.geoLoc.latitudeDelta != null &&
            this.state.geoLoc.longitudeDelta != null && (
              <MapView
                ref={MapView => (this.MapView = MapView)}
                style={styles.map}
                region={this.state.geoLoc}
                // initialRegion={{
                //   latitude: this.state.geoLoc.latitude,
                //   longitude: this.state.geoLoc.longitude,
                //   latitudeDelta: this.state.geoLoc.latitudeDelta,
                //   longitudeDelta: this.state.geoLoc.longitudeDelta,
                // }}
                loadingEnabled={true}
                loadingIndicatorColor="#666666"
                loadingBackgroundColor="#eeeeee"
                moveOnMarkerPress={false}
                showsUserLocation={true}
                showsCompass={true}
                showsPointsOfInterest={false}
                provider="google"
              >
                {this.state.srviceKit != null
                  ? this.state.srviceKit.map((marker, index) => {
                      console.log("marker", marker);
                      return (
                        <MapView.Marker
                          key={index}
                          style={styles.mapViewMarker}
                          coordinate={{
                            latitude: marker.latitude,
                            longitude: marker.longitude
                          }}
                          onPress={() => this.markView(marker)}
                          title={marker.locationNote}
                          description={marker.phoneNumber}
                          image={require("../assets/Images/map_location_icon/heart-locator.png")}
                        >
                          <MapView.Callout>
                            <TouchableHighlight
                              onPress={() => this.markerClick()}
                              underlayColor="#dddddd"
                            >
                              <View style={styles.calloutViewFirst}>
                                <View style={styles.calloutViewSecond}>
                                  <Row>
                                    <Col style={styles.viewFirstCol}>
                                      <Text style={styles.viewFirstText}>
                                        {marker.locationNote}
                                      </Text>
                                    </Col>
                                    <Col>
                                      <Text>
                                        <Icon
                                          type="FontAwesome"
                                          name="heartbeat"
                                          style={styles.viewFirstSecondColText}
                                        />
                                      </Text>
                                    </Col>
                                  </Row>
                                </View>
                                <View style={styles.calloutViewThird}>
                                  <Row>
                                    <Col style={styles.ViewSecondFirstCol}>
                                      <View>
                                        <Text>Creative Maple</Text>
                                        <Text>170 Water St.</Text>
                                        <Text>+ {marker.phoneNumber}</Text>
                                      </View>
                                    </Col>
                                    <Col style={styles.ViewSecondSecondCol}>
                                      <CardItem
                                        style={styles.ViewSecondCardItem}
                                      >
                                        <Icon
                                          name="md-call"
                                          style={styles.ViewSecondIcon}
                                        />
                                      </CardItem>
                                    </Col>
                                    <Col style={styles.ViewSecondSecondCol}>
                                      <CardItem
                                        style={styles.ViewSecondCardItemSecond}
                                      >
                                        <Icon
                                          name="map-marker"
                                          type="FontAwesome"
                                          style={styles.ViewSecondIcon}
                                        />
                                      </CardItem>
                                    </Col>
                                  </Row>
                                </View>
                              </View>
                            </TouchableHighlight>
                          </MapView.Callout>
                        </MapView.Marker>
                      );
                    })
                  : null}
                {!!this.state.geoLoc.latitude &&
                  !!this.state.geoLoc.longitude && (
                    <MapView.Marker
                      coordinate={{
                        latitude: this.state.geoLoc.latitude,
                        longitude: this.state.geoLoc.longitude
                      }}
                      title={"Your Location"}
                      image={require("../assets/Images/map_location_icon/user_map-locator1.png")}
                      draggable
                    />
                  )}
                {!!this.state.cordLatitude &&
                  !!this.state.cordLongitude &&
                  this.state.display && (
                    <MapView.Marker
                      coordinate={{
                        latitude: this.state.cordLatitude,
                        longitude: this.state.cordLongitude
                      }}
                      title={"Your Destination"}
                    />
                  )}
                {!!this.state.geoLoc.latitude &&
                  !!this.state.geoLoc.longitude &&
                  this.state.x == "true" && (
                    <MapView.Polyline
                      coordinates={this.state.coords}
                      strokeWidth={2}
                      strokeColor="red"
                    />
                  )}
                {!!this.state.latitude &&
                  !!this.state.longitude &&
                  this.state.x == "error" && (
                    <MapView.Polyline
                      coordinates={[
                        {
                          latitude: this.state.geoLoc.latitude,
                          longitude: this.state.geoLoc.longitude
                        },
                        {
                          latitude: this.state.cordLatitude,
                          longitude: this.state.cordLongitude
                        }
                      ]}
                      strokeWidth={2}
                      strokeColor="red"
                    />
                  )}
              </MapView>
            )}
          <FooterPage {...this.props} />
        </View>
        {this.state.geoLoc != null &&
          this.state.geoLoc.latitude != null &&
          this.state.geoLoc.longitude != null &&
          this.state.geoLoc.latitudeDelta != null &&
          this.state.geoLoc.longitudeDelta != null && (
            <Item style={styles.item}>
              <GooglePlacesAutocomplete
                placeholder="Search address"
                minLength={2} // minimum length of text to search
                autoFocus={false}
                fetchDetails={true}
                returnKeyType={"default"}
                onPress={(data, details = null) => {
                  // 'details' is provided when fetchDetails = true
                  console.log(data);
                  console.log(details);
                  var data = details;
                  this.mergeLot(data);
                }}
                query={{
                  // available options: https://developers.google.com/places/web-service/autocomplete
                  key: Config.key.MAP_API_KEY,
                  language: "en",
                  types: "geocode" // default: 'geocode'
                }}
                listViewDisplayed={this.state.showPlacesList}
                textInputProps={{
                  onFocus: () => this.setState({ showPlacesList: true }),
                  onBlur: () => this.setState({ showPlacesList: false })
                }}
                styles={{
                  description: {
                    fontWeight: "bold"
                  },
                  predefinedPlacesDescription: {
                    color: "#1faadb"
                  },
                  textInputContainer: {
                    backgroundColor: "#fff",
                    borderTopColor: "white",
                    borderBottomColor: "white",
                    borderRadius: 10
                  }
                }}
                currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
                currentLocationLabel="Current location"
                nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                filterReverseGeocodingByTypes={[
                  "locality",
                  "administrative_area_level_3"
                ]} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
                // predefinedPlaces={[]}
                predefinedPlacesAlwaysVisible={true}
              />
              <Button style={styles.button}>
                <Text style={styles.buttonText}>Search</Text>
              </Button>
            </Item>
          )}
        {this.state.display != null ? (
          <View style={styles.view}>
            <Row style={styles.viewRow}>
              <Col style={styles.viewFirstCol}>
                <Text style={styles.viewRowText}>Cardiac Arrest</Text>
              </Col>
              <Col>
                <Text>
                  <Icon
                    type="FontAwesome"
                    name="heartbeat"
                    style={styles.viewRowIcon}
                  />
                </Text>
              </Col>
            </Row>
            <CardItem>
              <Body>
                <Row>
                  <Col>
                    <Text style={styles.viewCardItemColFirst}>
                      {this.state.display.duration}
                    </Text>
                  </Col>
                  <Col style={styles.colWidth}>
                    <Text style={styles.viewCardItemColFirst}>|</Text>
                  </Col>
                  <Col>
                    <Text style={styles.viewCardItemColFirst}>
                      {this.state.display.distance}
                    </Text>
                  </Col>
                </Row>
              </Body>
            </CardItem>
          </View>
        ) : null}
      </Container>
    );
  }
}
