import React, { Component } from "react";
import {
  Modal,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  Dimensions
} from "react-native";
import {
  Container,
  Body,
  Button,
  Icon,
  Content,
  Card,
  CardItem,
  Row,
  Col,
  Badge,
  Left,
  Right,
  Title,
  Subtitle
} from "native-base";
import Config from "../config";
import API from "../api/Apis";
import styles from "../assets/styles/CPRDetailCSS";
import FooterPage from "../componants/FooterPage";
import { SecondHeader } from "../componants/CommonHeader";

var SoundPlayer = require("react-native-sound");
var song = null;
const screen = Dimensions.get('window')
const path4 = require("../assets/Images/noImage.png");
const Apis = new API();

var { height, width } = Dimensions.get('window');
export default class CPR_Guide extends Component {
  constructor(props) {
    super(props);
    this.state = {
      CPR: "",
      pause: false,
      modalVisible: false,
      play: false,
      stop: true,
      steps: [],
      playPause: false,
      scrollY: 0
    };
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  PlayColor(st) {
    this.setState({ play: st });
    this.setState({ stop: !st });
  }

  onPressButtonPlay() {
    song = new SoundPlayer(
      "heart_attack_primary_treatmentwmv.mp3",
      SoundPlayer.MAIN_BUNDLE,
      error => {
        if (error)
          ToastAndroid.show(
            "Error when init SoundPlayer :(((",
            ToastAndroid.SHORT
          );
        else {
          song.play(success => {
            if (!success)
              ToastAndroid.show(
                "Error when play SoundPlayer :(((",
                ToastAndroid.SHORT
              );
          });
        }
      }
    );

    // if(song != null) {
    //   song.play((success) => {
    //     if(!success)
    //       ToastAndroid.show('Error when play SoundPlayer :(((', ToastAndroid.SHORT);
    //   });
    // }
  }

  onPressButton(status) {
    console.log("song Status", status);
    this.setState({ playPause: status });
    if (status == true) {
      this.onPressButtonPlay();
    } else {
      this.PlayColor(false);
      this.onPressButtonPause();
    }
  }

  onPressButtonPause() {
    if (song != null) {
      //   if (this.state.pause)
      //     // play resume
      //     song.play(success => {
      //       if (!success)
      //         ToastAndroid.show(
      //           "Error when play SoundPlayer :(((",
      //           ToastAndroid.SHORT
      //         );
      //     });
      //else
      song.pause();
    }
  }

  componentWillMount() {
    const { navigation } = this.props;
    const CPR = navigation.getParam("CPR", CPR);
    this.setState({ CPR: CPR });
    Apis.getAllSteps()
      .then(response => {
        this.setState({ steps: response.CPRSteps });
      })
      .catch(err => {
        console.log(err);
      });
  }

  switch(id) {
    if (id == 1) {
      this.props.navigation.goBack();
    }
    this.setState({ CPR: id - 1 });
  }

  switchTwo(id) {
    var lastIndex = this.state.steps.length;
    if (lastIndex == id) {
      this.props.navigation.goBack();
    }
    this.setState({ CPR: id + 1 });
  }

  handleScroll(event) {
    if (0 < event.nativeEvent.contentOffset.y) {
      this.setState({ scrollY: event.nativeEvent.contentOffset.y });
    } else if (0 == event.nativeEvent.contentOffset.y) {
      this.setState({ scrollY: event.nativeEvent.contentOffset.y });
    }
  }

  render() {
    return (
      <Container style={styles.container}>
        <SecondHeader {...this.props} />
        <Content style={styles.container}>
          {this.state.steps.map((item, index) => {
            return index + 1 == this.state.CPR ? (
              <View key={index} style={styles.view}>
                <Text style={styles.viewText}>Step {index + 1}</Text>
                <Card style={{ borderRadius: 8, overflow: 'hidden' }}>
                  <CardItem header style={{ backgroundColor: '#D7253B', paddingTop: 5 }}>
                    <Body style={{ alignSelf: 'center', alignItems: 'center', position: 'relative' }}>
                      <TouchableOpacity onPress={() => this.switch(index + 1)}>
                        <Text style={{ alignSelf: 'center', alignItems: 'center' }}><Icon style={{ color: 'white' }} type='FontAwesome' name="chevron-up" /></Text>
                        <Text style={{ color: 'white', fontWeight: 'bold' }}> GO Back</Text>
                      </TouchableOpacity>
                      <Right style={{ width: 30, alignSelf: 'flex-end', position: 'absolute', top: '30%' }}>
                        <Icon
                          onPress={() => {
                            this.setModalVisible(true);
                          }}
                          style={styles.ViewIcon}
                          type="MaterialCommunityIcons"
                          name="music-box"
                        />
                      </Right>
                    </Body>
                  </CardItem>
                  <CardItem style={{ height: height - 350, maxHeight: height - 350, minHeight: height - 350, width: width - 50, overflow: 'scroll', alignSelf: 'center' }}>
                    <ScrollView onScroll={event => this.handleScroll(event)} >
                      <Body>
                        <Text style={{ alignSelf: 'center' }}>{item.title}</Text>
                        {item.stepImage ? (
                          <Image
                            source={{
                              uri: Config.key.HS_API_URL + "/" + item.stepImage
                            }}
                            style={styles.image}
                          />
                        ) : (
                            <Image source={path4} style={styles.image} />
                          )}

                        {item.step.split(".").map((items, index) => {
                          return items ? (
                            <Row key={index} style={{ marginBottom: 2 }} >
                              <Col style={styles.mapCol}>
                                <Button
                                  style={{
                                    backgroundColor: "#D7253B",
                                    borderRadius: 15,
                                    height: 30,
                                    width: 30,
                                  }}
                                >
                                  <Text style={{ color: '#fff', padding: 10, fontWeight: 'bold' }}>{index + 1}</Text>
                                </Button>
                              </Col>
                              <Col style={styles.mapText}>
                                <Text> {items}</Text>
                              </Col>
                            </Row>
                          ) : null;
                        })}
                      </Body>
                    </ScrollView>
                  </CardItem>
                  {this.state.scrollY != 0 ? (
                    <CardItem button onPress={() => this.switchTwo(index + 1)} footer style={{ alignItems: 'center', alignSelf: 'center', paddingTop: 0, paddingBottom: 0 }}>
                      <Text>
                        <Icon
                          style={styles.icon}
                          type="Entypo"
                          name="chevron-down"
                        />
                      </Text>
                    </CardItem>
                  ) : null}
                </Card>

                {/* <Card style={styles.viewCard}>
                  <Row>
                  <View style={{flex: 1, flexDirection: 'row'}}>
                    <View style={{width: "90%", height: 65, backgroundColor: '#D7253B',alignContent:'center',alignItems:'center', paddingLeft: 15}} >
                    <TouchableOpacity onPress={() => this.switch(index + 1)}>
                        <Icon
                          style={styles.viewBody}
                          type="Entypo"
                          name="chevron-up"
                        />
                      </TouchableOpacity>
                      <Text style={styles.viewBodyText}> Go Back </Text>
                    </View>
                    <View style={{width: '10%', height: 65, backgroundColor: '#D7253B'}}>
                    <Right>
                    <Icon
                      onPress={() => {
                        this.setModalVisible(true);
                      }}
                      style={styles.ViewIcon}
                      type="MaterialCommunityIcons"
                      name="music-box"
                    />
                    </Right>
                    </View>
                  </View>
                    
                  </Row>
                  <ScrollView  ref="Content"  onScroll={event => this.handleScroll(event)} style={styles.scrollView} >
                    <CardItem style={styles.scrollViewCardItem}>
                      <Body style={styles.scrollViewCardItem}>
                        <Text style={styles.scrollViewCardItem}>
                          {item.title}
                        </Text>
                        {item.stepImage ? (
                          <Image
                            source={{
                              uri: Config.key.HS_API_URL + "/" + item.stepImage
                            }}
                            style={styles.image}
                          />
                        ) : (
                          <Image source={path4} style={styles.image} />
                        )}
                        {item.step.split(".").map((items, index) => {
                          return items ? (
                            <Row key={index} style={styles.mapRow}>
                              <Col style={styles.mapCol}>
                                  <Button
                                  style={{
                                    backgroundColor: "red",
                                    borderRadius: 15,
                                    height: 30,
                                    width: 30
                                  }}
                                >
                                  <Text style={styles.mapText}>
                                    {" "}
                                    {index + 1}{" "}
                                  </Text>
                                </Button>
                              </Col>
                              <Col>
                                <Text> {items}</Text>
                              </Col>
                            </Row>
                          ) : null;
                        })}
                      </Body>
                    </CardItem>
                  </ScrollView>
                  {this.state.scrollY != 0 ? (
                    <TouchableOpacity onPress={() => this.switchTwo(index + 1)}>
                      <Icon
                        style={styles.icon}
                        type="Entypo"
                        name="chevron-down"
                      />
                    </TouchableOpacity>
                  ) : (
                    <Icon />
                  )}
                </Card> */}
              </View>
            ) : null;
          })}
        </Content>
        <FooterPage {...this.props} />
        <Modal
          animationType="fade"
          transparent={true}
          style={styles.modalColor}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible(!this.state.modalVisible);
          }}
        >
          <Card style={styles.modalCard}>
            <CardItem>
              <Left>
                <Text>Play CPR Guide</Text>
              </Left>
              <Right>
                <Button
                  transparent
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                    this.onPressButtonPause();
                  }}
                >
                  <Icon
                    type="FontAwesome"
                    style={styles.modalIcon}
                    name="close"
                  />
                </Button>
              </Right>
            </CardItem>
            <CardItem cardBody>
              <Body>
                <Icon
                  name="music-circle"
                  type="MaterialCommunityIcons"
                  style={styles.modalCardItem}
                />
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Button
                  transparent
                  onPress={() => {
                    this.onPressButtonPause();
                    this.PlayColor(false);
                  }}
                >
                  <Icon
                    type="FontAwesome"
                    style={
                      this.state.play ? styles.stopColor : styles.playColor
                    }
                    name="stop-circle-o"
                  />
                </Button>
              </Left>
              <Right>
                <Button
                  transparent
                  onPress={() => {
                    this.onPressButton(!this.state.playPause);
                    this.PlayColor(true);
                  }}
                >
                  <Icon
                    type="FontAwesome"
                    style={
                      this.state.stop ? styles.stopColor : styles.playColor
                    }
                    name="play-circle-o"
                  />
                </Button>
              </Right>
            </CardItem>
          </Card>
        </Modal>
      </Container>
    );
  }
}
