/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Text, View, TouchableHighlight, AsyncStorage, Alert } from "react-native";
import { Container, Body, Button, Icon, CardItem, Col, Row, Item, Spinner } from "native-base";
import MapView from "react-native-maps";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import API from "../api/Apis";
import Polyline from "@mapbox/polyline";
import distance from "react-native-google-matrix";
import Config from "../config";
import styles from '../assets/styles/AddDeviceMapCSS';
import { SecondHeader } from "../componants/CommonHeader";

const myApiKey = Config.key.MAP_API_KEY;
distance.apiKey = myApiKey;

const Apis = new API();
export default class AddDeviceMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      geoLoc: [],
      isLoading: false,
      srviceKit: [],
      lastPosition: "unknown",
      display: [],
      isLoading: false,
      is_searching: false,
      error: null,
      coords: [],
      concat: null,
      latitude: null,
      longitude: null,
      x: "false",
      userData: [],
      cordLatitude: null,
      cordLongitude: null,
      showPlacesList: false,
      latLong: [],
      search: false,
      curr: false,

    };
  }

  setSpinner = st => {
    this.setState({ isLoading: st });
  };

  ////////////get current location lat long and devices////////////////
  // componentWillMount() {
  //   AsyncStorage.setItem("UID123", "", () => {
  //     this.setState({ display: "" });
  //    });
  // }

  componentDidMount() {
    this.setSpinner(true);

    navigator.geolocation.getCurrentPosition(position => {
      console.log("this.state.geoLoc", position);

      let region = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        latitudeDelta: 0.00922 * 1.5,
        longitudeDelta: 0.00421 * 1.5
      };
      this.setState({
        geoLoc: region,
        curr: true,
      });
    });

    AsyncStorage.getItem("USER", (err, result) => {
      this.setState({ userData: JSON.parse(result) });
      var uid = JSON.parse(result).User._id;
      Apis.getDeviceById(uid).then(
        res => {
          this.setState({ srviceKit: res });

        },
        error => {
          alert("No internet connection!");
        }
      );
    });
    //  AsyncStorage.getItem("UID123", (err, result) => {
    //   this.setState({ display: JSON.parse(result) });
    //  });
  }
  ////////////get current location lat long//////////////////

  mergeLot(data) {
    // console.log("dat", data);

    let destinationlat = data.geometry.location.lat;
    let destinationlng = data.geometry.location.lng;
    let region = {
      latitude: data.geometry.location.lat,
      longitude: data.geometry.location.lng,
      latitudeDelta: 0.00922 * 1.5,
      longitudeDelta: 0.00421 * 1.5
    };


    this.setState({
      geoLoc: region,
      search: true,
      curr: false
    });


    data = {
      lat: destinationlat,
      long: destinationlng
    };
    this.setState({
      cordLatitude: data.lat,
      cordLongitude: data.long
    });
    fetch(
      "https://maps.googleapis.com/maps/api/geocode/json?address=" +
      data.lat +
      "," +
      data.long +
      "&key=" +
      myApiKey
    )
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson != null) {
          if (responseJson.results[0] != null) {
            const item2 = {
              latitude: data.lat,
              longitude: data.long,
              address: responseJson.results[0].formatted_address
            };
            //  console.log("latLong item2",item2);
            this.setState({ latLong: item2 });
            console.log("this.state.latLong", this.state.latLong.address);
            // console.log("latLong",this.state.latLong);
          }
        }
      });
    this.getDirections(data);
    // this.duration(data);
  }

  async getDirections(concatLot) {
    this.setSpinner(true);
    let destinationLoc = concatLot.lat + "," + concatLot.long;
    let startLoc =
      this.state.geoLoc.latitude + "," + this.state.geoLoc.longitude;
    try {
      // String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters + "&key=" + MY_API_KEY

      let resp = await fetch(
        `https://maps.googleapis.com/maps/api/directions/json?origin=${startLoc}&destination=${destinationLoc} ` +
        "&key=" +
        myApiKey
      );
      let respJson = await resp.json();
      // console.log("respJson", respJson);
      let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
      // console.log("points", points);
      let coords = points.map((point, index) => {
        return {
          latitude: point[0],
          longitude: point[1]
        };
      });
      this.setState({ coords: coords });
      // AsyncStorage.getItem("UID123", (err, result) => {
      //  this.setState({ display: JSON.parse(result) });
      //   if (this.state.display != null) {
      //     this.setSpinner(false);
      //   }
      // });
      this.setState({ x: "true" });
      return coords;
    } catch (error) {
      this.setSpinner(false);
      this.setState({ x: "error" });
      return error;
    }
  }

  // duration(concatLot) {
  //   this.setSpinner(true);
  //   let destinationLoc = concatLot.lat + "," + concatLot.long;
  //   let startLoc =
  //     this.state.geoLoc.latitude + "," + this.state.geoLoc.longitude;

  //   distance.get(
  //     {
  //       index: 1,
  //       origin: startLoc,
  //       destination: destinationLoc
  //     },
  //     function(err, data) {
  //       AsyncStorage.setItem("UID123", JSON.stringify(data), () => {
  //         AsyncStorage.mergeItem("UID123", JSON.stringify(data), () => {
  //           AsyncStorage.getItem("UID123", (err, result) => {
  //           });
  //         });
  //       });
  //     }
  //   );
  // }

  onDragend(e) {
    let region = {
      latitude: e.nativeEvent.coordinate.latitude,
      longitude: e.nativeEvent.coordinate.longitude
    };

    var item = region;
    fetch(
      "https://maps.googleapis.com/maps/api/geocode/json?address=" +
      item.latitude +
      "," +
      item.longitude +
      "&key=" +
      myApiKey
    )
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson != null) {
          if (responseJson.results[0] != null) {
            const item2 = {
              latitude: item.latitude,
              longitude: item.longitude,
              address: responseJson.results[0].formatted_address
            };
            this.setState({ latLong: item2 });
          }
        }
      });
  }
  markView(marker) {
    console.log("marker", marker);

    let region = {
      latitude: marker.latitude,
      longitude: marker.longitude,
      latitudeDelta: 0.00922 * 1.5,
      longitudeDelta: 0.00421 * 1.5
    };
    this.setState({ geoLoc: region });

  }
  Submit() {
    if (
      (this.state.latLong != null && this.state.latLong != "",
        this.state.latLong != 0)
    ) {
      this.props.navigation.navigate("AddDevice", { ADD: this.state.latLong });
    } else {
      Alert.alert(
        "",
        "Please Select Location !!",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    }
  }
  render() {
    return (
      <Container style={styles.mainContainer}>
        <SecondHeader {...this.props} />
        {this.state.isLoading ? (
          <View
            style={styles.spinnerView}
          >
            <Spinner
              visible={this.state.isLoading}
              textContent={"Looking for services..."}
              textStyle={styles.spinner}
              color={styles.spinnerColor}
            />
          </View>
        ) : null}

        <View style={styles.container}>
          {this.state.geoLoc != null &&
            this.state.geoLoc.latitude != null &&
            this.state.geoLoc.longitude != null &&
            this.state.geoLoc.latitudeDelta != null &&
            this.state.geoLoc.longitudeDelta != null && (
              <MapView
                ref={MapView => (this.MapView = MapView)}
                style={styles.map}
                region={this.state.geoLoc}

                // initialRegion={{
                //   latitude: this.state.geoLoc.latitude,
                //   longitude: this.state.geoLoc.longitude,
                //   latitudeDelta: this.state.geoLoc.latitudeDelta,
                //   longitudeDelta: this.state.geoLoc.longitudeDelta
                // }}
                loadingEnabled={true}
                loadingIndicatorColor="#666666"
                loadingBackgroundColor="#eeeeee"
                moveOnMarkerPress={false}
                showsUserLocation={true}
                showsCompass={true}
                // onPress={this.onMapPress.bind(this)}
                showsPointsOfInterest={false}
                provider="google"
              >
                {this.state.srviceKit != null
                  ? this.state.srviceKit.map((marker, index) => (
                    <MapView.Marker
                      key={index}
                      style={styles.mapMarkerColor}
                      coordinate={{
                        latitude: marker.latitude,
                        longitude: marker.longitude
                      }}
                      title={marker.locationNote}
                      description={marker.phoneNumber}
                      onPress={() => this.markView(marker)}
                      image={require("../assets/Images/map_location_icon/heart-locator.png")}
                    >
                      <MapView.Callout
                      >
                        <TouchableHighlight
                          underlayColor="#dddddd"
                        >
                          <View
                            style={styles.calloutView}
                          >
                            <View
                              style={styles.calloutViewFirst}
                            >
                              <Row>
                                <Col style={styles.calloutRow}>
                                  <Text
                                    style={styles.calloutRowText}
                                  >
                                    {marker.locationNote}
                                  </Text>
                                </Col>
                                <Col>
                                  <Text>
                                    <Icon
                                      type="FontAwesome"
                                      name="heartbeat"
                                      style={styles.calloutRowIcon}
                                    />
                                  </Text>
                                </Col>
                              </Row>
                            </View>

                            <View
                              style={styles.calloutViewSecond}
                            >
                              <Row>
                                <Col style={styles.calloutViewSecondRow}>
                                  <View>
                                    <Text>Creative Maple</Text>
                                    <Text>170 Water St.</Text>
                                    <Text>+ {marker.phoneNumber}</Text>
                                  </View>
                                </Col>
                                <Col style={styles.calloutViewSecondCol}>
                                  <CardItem
                                    style={styles.calloutViewSecondCardItem}
                                  >
                                    <Icon
                                      name="md-call"
                                      style={styles.calloutViewSecondCardItemIcon}
                                    />
                                  </CardItem>
                                </Col>
                                <Col style={styles.calloutViewSecondCol}>
                                  <CardItem
                                    style={styles.calloutViewSecondCardItemSecond}
                                  >
                                    <Icon
                                      name="map-marker"
                                      type="FontAwesome"
                                      style={styles.calloutViewSecondCardItemIcon}
                                    />
                                  </CardItem>
                                </Col>
                              </Row>
                            </View>
                          </View>
                        </TouchableHighlight>
                      </MapView.Callout>
                    </MapView.Marker>
                  ))
                  : null}
                {!!this.state.geoLoc.latitude && !!this.state.geoLoc.longitude && this.state.curr == true && (
                  <MapView.Marker
                    coordinate={{
                      latitude: this.state.geoLoc.latitude,
                      longitude: this.state.geoLoc.longitude
                    }}
                    title={"Your Location"}
                    image={require("../assets/Images/map_location_icon/user_map-locator1.png")}
                    onDragEnd={this.onDragend.bind(this)}
                    draggable
                  />
                )}

                {!!this.state.geoLoc.latitude && !!this.state.geoLoc.longitude && this.state.search == true && (
                  <MapView.Marker
                    coordinate={{
                      latitude: this.state.geoLoc.latitude,
                      longitude: this.state.geoLoc.longitude
                    }}
                    title={this.state.latLong.address}
                  // onDragEnd={this.onDragend.bind(this)}
                  // draggable
                  />
                )}
                {/* {!!this.state.cordLatitude &&
                  !!this.state.cordLongitude &&
                   (
                    <MapView.Marker
                      coordinate={{
                        latitude: this.state.cordLatitude,
                        longitude: this.state.cordLongitude
                      }}
                      title={"Your Destination"}
                    />
                  )} */}

                {/* {!!this.state.geoLoc.latitude &&
                  !!this.state.geoLoc.longitude &&
                  this.state.x == "true" && (
                    <MapView.Polyline
                      coordinates={this.state.coords}
                      strokeWidth={2}
                      strokeColor="red"
                    />
                  )} */}

                {/* {!!this.state.latitude &&
                  !!this.state.longitude &&
                  this.state.x == "error" && (
                    <MapView.Polyline
                      coordinates={[
                        {
                          latitude: this.state.geoLoc.latitude,
                          longitude: this.state.geoLoc.longitude
                        },
                        {
                          latitude: this.state.cordLatitude,
                          longitude: this.state.cordLongitude
                        }
                      ]}
                      strokeWidth={2}
                      strokeColor="red"
                    />
                  )} */}
              </MapView>
            )}

          <Button
            full
            onPress={() => this.Submit()}
            style={styles.buttonSecond}
          >
            <Text style={styles.secondButtonText}>
              {" "}
              ADD LOCATION{" "}
            </Text>
          </Button>
        </View>
        {this.state.geoLoc != null && this.state.geoLoc.latitude != null && this.state.geoLoc.longitude != null && this.state.geoLoc.latitudeDelta != null && this.state.geoLoc.longitudeDelta != null &&
          <Item style={styles.item}>
            <GooglePlacesAutocomplete
              placeholder='Search address'
              minLength={2} // minimum length of text to search
              autoFocus={false}
              fetchDetails={true}
              returnKeyType={'default'}
              onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                console.log(data);
                console.log(details);
                var data = details;
                this.mergeLot(data);
              }}
              query={{
                // available options: https://developers.google.com/places/web-service/autocomplete
                key: Config.key.MAP_API_KEY,
                language: 'en',
                types: 'geocode', // default: 'geocode'
              }}

              listViewDisplayed={this.state.showPlacesList}
              textInputProps={{

                onFocus: () => this.setState({ showPlacesList: true }),
                onBlur: () => this.setState({ showPlacesList: false }),
              }}
              styles={{
                description: {
                  fontWeight: 'bold',
                },
                predefinedPlacesDescription: {
                  color: '#1faadb',

                },
                textInputContainer: {
                  backgroundColor: '#fff',
                  borderTopColor: 'white',
                  borderBottomColor: 'white',
                  borderRadius: 10
                }
              }}

              currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
              currentLocationLabel="Current location"
              nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch

              filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
              // predefinedPlaces={[]}

              predefinedPlacesAlwaysVisible={true}
              renderLeftButton={() => <Icon name="search" size={20} color="#1F4497" style={styles.renderLeftButton} />}

            />
            <Button style={styles.button}><Text style={styles.buttonText}> Search </Text></Button>
          </Item>
        }
        {/* {this.state.display != null ? (
          <View style={styles.view} >
            <Row
              style={styles.viewRow}
            >
              <Col style={styles.calloutRow}>
                <Text style={styles.viewText}>
                  Cardiac Arrest
                </Text>
              </Col>
              <Col>
                <Text>
                  <Icon
                    type="FontAwesome"
                    name="heartbeat"
                    style={styles.viewIcon}
                  />
                </Text>
              </Col>
            </Row>

            <CardItem>
              <Body>
                <Row>
                  <Col>
                    <Text style={styles.cardItemText}>
                      {this.state.display.duration}
                    </Text>
                  </Col>
                  <Col style={styles.cardItemCol}>
                    <Text style={styles.cardItemText}>|</Text>
                  </Col>
                  <Col>
                    <Text style={styles.cardItemText}>
                      {this.state.display.distance}
                    </Text>
                  </Col>
                </Row>
              </Body>
            </CardItem>
          </View>
        ) : null}  */}
      </Container>
    );
  }
}

