import {GoogleAnalyticsSettings, GoogleAnalyticsTracker} from 'react-native-google-analytics-bridge';


GoogleAnalyticsSettings.setDryRun(false);
let tracker = new GoogleAnalyticsTracker("UA-128498782-1");

export const setAppName = (appName) => {

    tracker.setAppName(appName);
  
  };

  

   export const trackScreenView = (screenName) => {
    console.log("screenName", screenName);
    tracker.trackScreenView(screenName);
  
  };

  export const setAppVersion = (appVersion) => {

    tracker.setAppVersion(appVersion);
  
  };

  // Enable tracking of uncaught exceptions

export const setTrackUncaughtExceptions = (enabled = true) => {

    tracker.setTrackUncaughtExceptions(enabled);
  
  };
  
  
  
  /**
  
  error: String, a description of the exception (up to 100 characters), accepts nil
  
  fatal (required): Boolean, indicates whether the exception was fatal, defaults to false
  
  */
  
  export const trackException = (error, fatal = false) => {
  
    tracker.trackException(error, fatal);
  
  };