const mongoose = require("mongoose");
var Schema = mongoose.Schema;

var appVersionSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  appVersion: { type: Number, required: true },
  apiVersion: { type: String, required: true },
  createdAt: { type: Number, default: Date.now() },
  updatedAt: { type: Date },
  isDelete: { type: Boolean, default: false },
  deletedAt: { type: Number }
});

var AppVersion = mongoose.model("AppVersion", appVersionSchema);

module.exports = { AppVersion };
