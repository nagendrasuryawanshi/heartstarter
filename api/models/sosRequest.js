const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const sosRequestSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  phoneNumber: { type: String },
  uid: { type: Schema.Types.ObjectId, ref: "User" },
  status: { type: String, default: "Active" },
  deviceId: { type: String, required: true },
  requestTime: { type: Date },
  location: {
    type: {
      type: String, // Don't do `{ location: { type: String } }`
      enum: ["Point"], // 'location.type' must be 'Point'
      required: true
    },
    coordinates: {
      type: [Number],
      required: true
    }
  },
  createdAt: { type: Number, default: Date.now() },
  updatedAt: { type: Date },
  isDelete: { type: Boolean, default: false },
  deletedAt: { type: Date }
});

const SOSRequest = mongoose.model("SOSRequest", sosRequestSchema);

module.exports = { SOSRequest };
