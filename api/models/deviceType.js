const mongoose = require("mongoose");
var Schema = mongoose.Schema;

var deviceTypeSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  deviceType: { type: String, required: true },
  
  createdAt: { type: Number, default: Date.now() },
  updatedAt: { type: Number },
  isDelete: { type: Boolean, default: false },
  deletedAt: { type: Number }
});

var DeviceType = mongoose.model("DeviceType", deviceTypeSchema);

module.exports = { DeviceType };
