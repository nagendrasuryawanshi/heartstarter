const mongoose = require("mongoose");
var Schema = mongoose.Schema;

var deviceSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  user: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
  deviceType: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "DeviceType",
    required: true
  },
  latitude: { type: Number, required: true },
  longitude: { type: Number, required: true },
  locationNote: { type: String, required: true },
  phoneNumber: { type: String, required: true },
  image1: { type: String },
  image2: { type: String },
  image3: { type: String },

  createdAt: { type: Number, default: Date.now() },
  updatedAt: { type: Number },
  isDelete: { type: Boolean, default: false },
  deletedAt: { type: Number }
});

var Device = mongoose.model("Device", deviceSchema);

module.exports = { Device };
