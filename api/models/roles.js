const mongoose = require("mongoose");
var Schema = mongoose.Schema;

var rolesSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  role: { type: String, required: true },

  createdAt: { type: Number, default: Date.now() },
  updatedAt: { type: Number },
  isDelete: { type: Boolean, default: false },
  deletedAt: { type: Number }
});

var Roles = mongoose.model("Roles", rolesSchema);

module.exports = { Roles };
