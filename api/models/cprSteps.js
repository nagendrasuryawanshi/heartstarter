const mongoose = require("mongoose");
var Schema = mongoose.Schema;

var cprStepSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  step: { type: String, required: true },
  title: { type: String, required: true },
  stepImage: { type: String },
  createdAt: { type: Number, default: Date.now() },
  updatedAt: { type: Date },
  isDelete: { type: Boolean, default: false },
  deletedAt: { type: Number }
});

var CPRSteps = mongoose.model("CPRSteps", cprStepSchema);
module.exports = { CPRSteps };
