const mongoose = require("mongoose");
var Schema = mongoose.Schema;

var platformSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  deviceId: { type: String, required: true },
  location: {
    type: {
      type: String, // Don't do `{ location: { type: String } }`
      enum: ["Point"], // 'location.type' must be 'Point'
      required: true
    },
    coordinates: {
      type: [Number],
      required: true
    }
  },
  serial: { type: String },
  brand: { type: String },
  model: { type: String },

  createdAt: { type: Date, default: Date.now() },
  updatedAt: { type: Date },
  isDelete: { type: Boolean, default: false },
  deletedAt: { type: Date }
});

var Platform = mongoose.model("Platform", platformSchema);

module.exports = { Platform };
