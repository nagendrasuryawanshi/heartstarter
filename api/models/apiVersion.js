const mongoose = require("mongoose");
var Schema = mongoose.Schema;

var apiVersionSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  apiVersion: { type: String, required: true },
  isActive: { type: Boolean, required: true, default: false },
  statusCode: { type: String, required: true, default: "0" },
  //0 (still in work)301 (Moved Permanently) or 410 (Gone)
  createdAt: { type: Number, default: Date.now() },
  updatedAt: { type: Number },
  isDelete: { type: Boolean, default: false },
  deletedAt: { type: Number }
});

var ApiVersion = mongoose.model("Version", apiVersionSchema);

module.exports = { ApiVersion };
