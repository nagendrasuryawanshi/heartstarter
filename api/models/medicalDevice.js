const mongoose = require("mongoose");
var Schema = mongoose.Schema;

var medicalDeviceSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  uid: { type: mongoose.Schema.Types.ObjectId, required: true },
  deviceTypeId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  latitude: { type: Number, required: true },
  longitude: { type: Number, required: true },
  locationNote: { type: String, required: true },
  phoneNumber: { type: String, required: true },
  image1: { type: String },
  image2: { type: String },
  image3: { type: String },

  createdAt: { type: Number, default: Date.now() },
  updatedAt: { type: Number },
  isDelete: { type: Boolean, default: false },
  deletedAt: { type: Number }
});

var MedicalDevice = mongoose.model("MedicalDevice", medicalDeviceSchema);

module.exports = { MedicalDevice };
