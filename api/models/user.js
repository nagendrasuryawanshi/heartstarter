const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,

  roles: [{ type: mongoose.Schema.Types.ObjectId, ref: "Roles" }],

  firstName: { type: String, required: true },
  lastName: { type: String, required: true },

  phoneNumber: { type: String },
  deviceId: { type: String, required: true },
  deviceName: { type: String, required: true },
  profilePhoto: { type: String },
  latitude: { type: String, required: true },
  longitude: { type: String, required: true },
  verified: { type: Boolean, default: true },

  gender: { type: String },
  birthYear: { type: Number },
  notificationPerDay: { type: Number },

  createdAt: { type: Number, default: Date.now() },
  updatedAt: { type: Date },
  isDelete: { type: Boolean, default: false },
  deletedAt: { type: Date }
});

const User = mongoose.model("User", userSchema);

module.exports = { User };
