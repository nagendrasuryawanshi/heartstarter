const mongoose = require("mongoose");
var Schema = mongoose.Schema;

var problemDurationSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  duration: { type: String, required: true },
  createdAt: { type: Number, default: Date.now() },
  updatedAt: { type: Number },
  isDelete: { type: Boolean, default: false },
  deletedAt: { type: Number }
});

var ProblemDuration = mongoose.model("ProblemDuration", problemDurationSchema);

module.exports = { ProblemDuration };
