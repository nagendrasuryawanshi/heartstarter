const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const adminSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  firstName: { type: String },
  lastName: { type: String },
  username: { type: String },
  // email: {
  //     type: String,
  //     required: true,
  //     unique: true,
  //     match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
  // },
  password: {
    type: String,
    required: true
  },
  //emailVerified: { type: Boolean, default: false },

  createdAt: { type: Number, default: Date.now() },
  updatedAt: { type: Number },
  isDelete: { type: Boolean, default: false },
  deletedAt: { type: Number }
});

const Admin = mongoose.model("Admin", adminSchema);

module.exports = { Admin };
