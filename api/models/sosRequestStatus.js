const mongoose = require("mongoose");
var Schema = mongoose.Schema;

var sosRequestStatusSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  sosRequestStatus: { type: String, required: true },

  createdAt: { type: Number, default: Date.now() },
  updatedAt: { type: Date },
  isDelete: { type: Boolean, default: false },
  deletedAt: { type: Number }
});

var SOSRequestStatus = mongoose.model(
  "SOSRequestStatus",
  sosRequestStatusSchema
);

module.exports = { SOSRequestStatus };
