const app = require('./app');
const http = require('http');

//http.createServer(app).listen(process.env.PORT);

const port = process.env.PORT || 3000;
const server = http.createServer(app);

server.listen(port, () => {
  console.log(`Listening to port ${port}`);
});
