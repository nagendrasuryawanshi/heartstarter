const jwt = require("jsonwebtoken");
const { User } = require("../models/user");
const errorMsg = require("../controllers/error");
const config = require("../config");
module.exports = (request, response, next) => {
  const token = request.headers.token;
  try {
    jwt.verify(token, config.key.HS_USER_KEY);
    next();
  } catch (e) {
    errorMsg.errorMessage(response, 404, "Invalid token");
  }
};
