const { Roles } = require("../models/roles");

const errorMsg = require("./error");
const mongoose = require("mongoose");

exports.getAll = (request, response) => {
  Roles.find({ isDelete: { $eq: false } })
    .select("_Id role isDelete")
    .exec()
    .then(Roles => {
      response.status(200).json({
        Roles
      });
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found");
    });
};

exports.getAll_V2 = (request, response) => {
  Roles.find({ isDelete: { $eq: false } })
    .select("_Id role")
    .exec()
    .then(Roles => {
      response.status(200).json({
        Roles
      });
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found");
    });
};
exports.get = (request, response) => {
  console.log("request.params.Id", request.params.Id);
  var _roleId = "",
    _roleId = request.query.Id ? request.query.Id : null;

  if (request.params.Id) _roleId = request.params.Id ? request.params.Id : null;

  console.log("roleId", _roleId);
  // User.findOne({ userId: { $eq: _userId }, isDelete: { $eq: false } })
  Roles.findOne({ _id: _roleId })
    .select("_Id role isDelete")
    .exec()
    .then(Roles => {
      // response.send("Roles", _roleId);
      response.status(200).send(Roles);
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found " + err.message);
    });
};

exports.get_V2 = (request, response) => {
  console.log("request.params.Id", request.params.Id);
  var _roleId = "",
    _roleId = request.query.Id ? request.query.Id : null;

  if (request.params.Id) _roleId = request.params.Id ? request.params.Id : null;

  console.log("roleId", _roleId);
  // User.findOne({ userId: { $eq: _userId }, isDelete: { $eq: false } })
  Roles.findOne({ _id: _roleId })
    .select("_Id role")
    .exec()
    .then(Roles => {
      // response.send("Roles", _roleId);
      response.status(200).send(Roles);
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found " + err.message);
    });
};
exports.save = (request, response) => {
  //console.log("request", request.body);
  var body = request.body;

  Roles.find({ role: body.role, isDelete: { $eq: false } })
    .exec()
    .then(result => {
      if (result && result.length > 0) {
        console.log("User Type already exists");
        return response.status(401).json({
          status: 401,
          error: "Roles '" + body.role + "' already exists"
        });
      }
      var role = new Roles({
        _id: new mongoose.Types.ObjectId(),
        role: body.role ? body.role : ""
      });
      return role
        .save()
        .then(Roles => {
          //console.log(Roles);
          response.status(200).json({ Roles });
        })
        .catch(err => {
          console.log("error", err);
          return errorMsg.errorMessage(response, 400, "Error: " + err.message);
        });
    });
};

exports.update = (request, response) => {
  const id = request.params.Id;

  console.log("id", id);
  const updateOps = {};

  // for (const ops of request.body) {
  //   if (req.body.hasOwnProperty(ops)) {
  //     updateOps[ops.prop] = ops.value;
  //   }
  // }

  if (request.body.role) updateOps["role"] = request.body.role;

  if (request.body.isDelete) updateOps["isDelete"] = request.body.isDelete;

  updateOps["updatedAt"] = new Date();
  if (request.body.role) {
    Roles.find({ role: request.body.role, _id: { $ne: id } })
      .exec()
      .then(result => {
        if (result && result.length > 0) {
          console.log("User Type already exists");
          return response.status(401).json({
            status: 401,
            error: "Roles '" + request.body.role + "' already exists"
          });
        }
      });
  }
  Roles.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(Roles => {
      console.log(Roles);
      response.status(200).json({
        message: "User type updated",
        Roles
      });
    })
    .catch(err => {
      console.log(err);
      response.status(500).json({
        error: err
      });
    });
};

exports.delete = (request, response) => {
  const id = request.params.Id;
  console.log("role", id);

  Roles.findOne({ _id: id })
    .exec()
    .then(result => {
      if (!result) {
        return response.status(400).json({ message: "User type not found" });
      } else {
        var query = { _id: id };
        var update = {
          isDelete: true,
          deletedAt: new Date()
        };
        var options = { new: true };
        Roles.findByIdAndUpdate(query, update, options, function(err, doc) {
          if (err) return res.status(500).json({ error: err });
          return response.send(doc);
        });
      }
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found " + err.message);
    });
};
