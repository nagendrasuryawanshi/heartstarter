const { CPRSteps } = require("../models/cprSteps");
const errorMsg = require("./error");
const mongoose = require("mongoose");
const multer = require("multer");

exports.getAll = (request, response) => {
  CPRSteps.find({ isDelete: { $eq: false } })
    .select("_Id title step stepImage isDelete")
    .exec()
    .then(CPRSteps => {
      response.status(200).json({
        CPRSteps
      });
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found");
    });
};

exports.get = (request, response) => {
  console.log("request.params.Id", request.params.Id);
  var _stepId = "",
    _stepId = request.query.Id ? request.query.Id : null;

  if (request.params.Id) _stepId = request.params.Id ? request.params.Id : null;

  console.log("stepId", _stepId);
  // User.findOne({ userId: { $eq: _userId }, isDelete: { $eq: false } })
  CPRSteps.findOne({ _id: _stepId })
    .select("_Id title step stepImage isDelete")
    .exec()
    .then(CPRSteps => {
      // response.send("CPRSteps", _stepId);
      response.status(200).send(CPRSteps);
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found " + err.message);
    });
};
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function(req, file, cb) {
    // console.log("reqfile", req , "file", file);

    cb(null, file.originalname);
    //cb(null, file.fieldname + '-' + Date.now())
  }
});

exports.upload = multer({ storage: storage });

exports.save = (request, response) => {
  var fileName = "";

  if (request.file) {
    fileName = request.file.filename ? request.file.filename : "";
  }

  // console.log("requestfile", request.filename);

  //console.log("request", request.body);
  var body = request.body;

  CPRSteps.find({ step: body.step, isDelete: { $eq: false } })
    .exec()
    .then(result => {
      if (result && result.length > 0) {
        console.log("User Type already exists");
        return response.status(401).json({
          status: 401,
          error: "CPRSteps '" + body.step + "' already exists"
        });
      }
      var step = new CPRSteps({
        _id: new mongoose.Types.ObjectId(),
        step: body.step ? body.step : "",
        title: body.title ? body.title : "",
        stepImage: fileName ? fileName : body.stepImage
      });
      return step
        .save()
        .then(CPRSteps => {
          //console.log(CPRSteps);
          response.status(200).json({ CPRSteps });
        })
        .catch(err => {
          console.log("error", err);
          return errorMsg.errorMessage(response, 400, "Error: " + err.message);
        });
    });
};

exports.update = (request, response) => {
  var fileName = "";

  if (request.file) {
    fileName = request.file.filename ? request.file.filename : "";
  }
  const id = request.params.Id;

  console.log("id", id);
  const updateOps = {};

  // for (const ops of request.body) {
  //   if (req.body.hasOwnProperty(ops)) {
  //     updateOps[ops.prop] = ops.value;
  //   }
  // }
  if (fileName) updateOps["stepImage"] = fileName;

  if (request.body.step) updateOps["step"] = request.body.step;
  if (request.body.title) updateOps["title"] = request.body.title;

  if (request.body.isDelete) updateOps["isDelete"] = request.body.isDelete;

  updateOps["updatedAt"] = new Date();
  if (request.body.step) {
    CPRSteps.find({ step: request.body.step, _id: { $ne: id } })
      .exec()
      .then(result => {
        if (result && result.length > 0) {
          console.log("CPR step already exists");
          return response.status(401).json({
            status: 401,
            error: "CPR step '" + request.body.step + "' already exists"
          });
        }
      });
  }
  CPRSteps.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(CPRSteps => {
      console.log(CPRSteps);
      response.status(200).json({
        message: "User type updated",
        CPRSteps
      });
    })
    .catch(err => {
      console.log(err);
      response.status(500).json({
        error: err
      });
    });
};

exports.delete = (request, response) => {
  const id = request.params.Id;
  console.log("step", id);

  CPRSteps.findOne({ _id: id })
    .exec()
    .then(result => {
      if (!result) {
        return response.status(400).json({ message: "User type not found" });
      } else {
        var query = { _id: id };
        var update = {
          isDelete: true,
          deletedAt: new Date()
        };
        var options = { new: true };
        CPRSteps.findByIdAndUpdate(query, update, options, function(err, doc) {
          if (err) return res.status(500).json({ error: err });
          return response.send(doc);
        });
      }
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found " + err.message);
    });
};
