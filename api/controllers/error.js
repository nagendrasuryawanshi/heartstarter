exports.errorMessage = function(response, statusCode ,message){
    return response.status(statusCode).json({
        status: statusCode,
        error: message
    })
}