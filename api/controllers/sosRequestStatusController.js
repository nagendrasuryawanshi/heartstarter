const { SOSRequestStatus } = require("../models/SOSRequestStatus");

const errorMsg = require("./error");
const mongoose = require("mongoose");

exports.getAll = (request, response) => {
  SOSRequestStatus.find({ isDelete: { $eq: false } })
    .select("_Id sosRequestStatus isDelete")
    .exec()
    .then(SOSRequestStatus => {
      response.status(200).json({
        SOSRequestStatus
      });
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found");
    });
};
exports.getAll_V2 = (request, response) => {
  SOSRequestStatus.find({ isDelete: { $eq: false } })
    .select("_Id sosRequestStatus")
    .exec()
    .then(SOSRequestStatus => {
      response.status(200).json({
        SOSRequestStatus
      });
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found");
    });
};
exports.get = (request, response) => {
  console.log("request.params.Id", request.params.Id);
  var _sosRequestStatusId = "",
    _sosRequestStatusId = request.query.Id ? request.query.Id : null;

  if (request.params.Id)
    _sosRequestStatusId = request.params.Id ? request.params.Id : null;

  // User.findOne({ deviceId: { $eq: _deviceId }, isDelete: { $eq: false } })
  SOSRequestStatus.findOne({ _id: _sosRequestStatusId })
    .select("_Id sosRequestStatus isDelete")
    .exec()
    .then(SOSRequestStatus => {
      // response.send("SOSRequestStatus", _sosRequestStatusId);
      response.status(200).send(SOSRequestStatus);
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found " + err.message);
    });
};
exports.get_V2 = (request, response) => {
  console.log("request.params.Id", request.params.Id);
  var _sosRequestStatusId = "",
    _sosRequestStatusId = request.query.Id ? request.query.Id : null;

  if (request.params.Id)
    _sosRequestStatusId = request.params.Id ? request.params.Id : null;

  // User.findOne({ deviceId: { $eq: _deviceId }, isDelete: { $eq: false } })
  SOSRequestStatus.findOne({ _id: _sosRequestStatusId })
    .select("_Id sosRequestStatus")
    .exec()
    .then(SOSRequestStatus => {
      response.status(200).send(SOSRequestStatus);
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found " + err.message);
    });
};
exports.save = (request, response) => {
  //console.log("request", request.body);
  var body = request.body;

  SOSRequestStatus.find({
    sosRequestStatus: body.sosRequestStatus,
    isDelete: { $eq: false }
  })
    .exec()
    .then(result => {
      if (result && result.length > 0) {
        console.log("Device Type already exists");
        return response.status(401).json({
          status: 401,
          error:
            "SOSRequestStatus '" + body.sosRequestStatus + "' already exists"
        });
      }
      var sosRequestStatus = new SOSRequestStatus({
        _id: new mongoose.Types.ObjectId(),
        sosRequestStatus: body.sosRequestStatus ? body.sosRequestStatus : ""
      });
      return sosRequestStatus
        .save()
        .then(SOSRequestStatus => {
          //console.log(SOSRequestStatus);
          response.status(200).json({ SOSRequestStatus });
        })
        .catch(err => {
          console.log("error", err);
          return errorMsg.errorMessage(response, 400, "Error: " + err.message);
        });
    });
};

exports.update = (request, response) => {
  const id = request.params.Id;

  console.log("id", id);
  const updateOps = {};

  // for (const ops of request.body) {
  //   if (req.body.hasOwnProperty(ops)) {
  //     updateOps[ops.prop] = ops.value;
  //   }
  // }

  if (request.body.sosRequestStatus)
    updateOps["sosRequestStatus"] = request.body.sosRequestStatus;

  if (request.body.isDelete) updateOps["isDelete"] = request.body.isDelete;

  updateOps["updatedAt"] = new Date();
  if (request.body.sosRequestStatus) {
    SOSRequestStatus.find({
      sosRequestStatus: request.body.sosRequestStatus,
      _id: { $ne: id }
    })
      .exec()
      .then(result => {
        if (result && result.length > 0) {
          console.log("Device Type already exists");
          return response.status(401).json({
            status: 401,
            error:
              "SOSRequestStatus '" +
              request.body.sosRequestStatus +
              "' already exists"
          });
        }
      });
  }
  SOSRequestStatus.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(SOSRequestStatus => {
      console.log(SOSRequestStatus);
      response.status(200).json({
        message: "Device type updated",
        SOSRequestStatus
      });
    })
    .catch(err => {
      console.log(err);
      response.status(500).json({
        error: err
      });
    });
};

exports.delete = (request, response) => {
  const id = request.params.Id;
  console.log("sosRequestStatus", id);

  SOSRequestStatus.findOne({ _id: id })
    .exec()
    .then(result => {
      if (!result) {
        return response.status(400).json({ message: "Device type not found" });
      } else {
        var query = { _id: id };
        var update = {
          isDelete: true,
          deletedAt: new Date()
        };
        var options = { new: true };
        SOSRequestStatus.findByIdAndUpdate(query, update, options, function(
          err,
          doc
        ) {
          if (err) return res.status(500).json({ error: err });
          return response.send(doc);
        });
      }
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found " + err.message);
    });
};
