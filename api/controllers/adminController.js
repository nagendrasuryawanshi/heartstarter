const { Admin } = require("../models/admin");
const _ = require("lodash");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const errorMsg = require("./error");
const mongoose = require("mongoose");
const config = require("../config");
var nodemailer = require("nodemailer");

var options = {
  service: "Gmail",
  auth: {
    admin: config.key.HS_MAIL_SERVER_USER,
    pass: config.key.HS_MAIL_SERVER_PASSWORD
  }
};
var client = nodemailer.createTransport(options);

exports.get_Admin = (request, response) => {
  const uid = request.query.id;
  console.log("admin basic", uid);
  Admin.findOne({ _id: { $eq: uid }, isDelete: { $eq: false } })
    //.select('username')
    .exec()
    .then(result => {
      response.send(result);
      // response.status(200).( result)
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found");
    });
};

exports.sign_up = (request, response) => {
  console.log("signupRequest", request.body);
  var body = request.body;
  // if(body.password === body.confirmPassword){
  Admin.find({
    username: body.username
  })
    .exec()
    .then(result => {
      if (result.length > 0) {
        console.log("username already exists");
        return errorMsg.errorMessage(response, 400, "username already exists");
      }
      bcrypt.hash(body.password, 10, (err, hash) => {
        if (err) {
          console.log("Password enc error");
          return errorMsg.errorMessage(
            response,
            400,
            "Something went wrong. Please check enterd username/password"
          );
        }

        var admin = new Admin({
          _id: new mongoose.Types.ObjectId(),
          username: body.username,
          firstName: body.firstName ? body.firstName : "",
          lastName: body.lastName ? body.lastName : "",
          password: hash
        });

        return admin
          .save()
          .then(AdminRes => {
            console.log(AdminRes);
            response.status(200).json({ AdminRes });
          })
          .catch(err => {
            return errorMsg.errorMessage(response, 400, "Bad request");
          });
      });
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Bad request");
    });
};

exports.login = (request, response) => {
  var body = request.body;
  Admin.findOne({
    username: body.username,
    isDelete: false
  })
    .exec()
    .then(result => {
      console.log("login Res", result);
      if (!result) {
        return errorMsg.errorMessage(
          response,
          400,
          "Invalid username/Password"
        );
      }
      if (!body.password) {
        return errorMsg.errorMessage(response, 400, "Password required !!!");
      }
      bcrypt.compare(body.password, result.password, (err, res) => {
        if (err) {
          return errorMsg.errorMessage(response, 400, "Something went wrong");
        }
        if (res) {
          jwt.sign(
            {
              _id: result._id,
              username: result.username,
              firstName: result.firstName,
              lastName: result.lastName
            },
            config.key.HS_USER_KEY,
            (e, token) => {
              if (e) {
                return errorMsg.errorMessage(
                  response,
                  400,
                  "Something went wrong"
                );
              }
              console.log("ResID");
              console.log(result._id);
              response.status(200).json({
                username: result.username,
                firstName: result.firstName,
                lastName: result.lastName,
                token: token
              });
            }
          );
        } else {
          return errorMsg.errorMessage(response, 400, "Invalid Password");
        }
      });
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 404, "Not Found");
    });
};

// exports.verification = (request, response) => {
//   var body = request.body;
//   console.log("verification request", body);
//   const id = body._id;
//   console.log("verification admin id", id);
//   Admin.find({ _id: id })
//     .exec()
//     .then(result => {
//       console.log("verification result", result);
//       if (!result || result.length <= 0) {
//         return errorMsg.errorMessage(response, 400, "Admin not exists");
//       }
//       var query = { _id: id };
//       var update = { emailVerified: true, updatedAt: new Date() };
//       var options = { new: true };
//       Admin.findByIdAndUpdate(query, update, options, function(err, doc) {
//         if (err) return response.status(500).json({ error: err });
//         clientdata(id);
//         return response.send(doc);
//       });
//     })
//     .catch(err => {
//       return errorMsg.errorMessage(response, 400, "Invalid Request");
//     });
// };

// exports.password_reset = (request, response) => {
//   var body = request.body;
//   const id = body._id;
//   console.log("updated admin id", id);
//   // if(body.password === body.confirmPassword){
//   Admin.find({ _id: id })
//     .exec()
//     .then(result => {
//       console.log("Password reset result", result);
//       if (!result || result.length <= 0) {
//         return errorMsg.errorMessage(response, 400, "Admin not exists");
//       }
//       bcrypt.hash(body.password, 10, (err, hash) => {
//         if (err) {
//           return errorMsg.errorMessage(response, 400, "Enter password");
//         }

//         var query = { _id: id };
//         var update = { password: hash, updatedAt: new Date() };
//         var options = { new: true };
//         Admin.findByIdAndUpdate(query, update, options, function(err, doc) {
//           if (err) return response.status(500).json({ error: err });
//           return response.send(doc);
//         });
//       });
//     })
//     .catch(err => {
//       return errorMsg.errorMessage(response, 400, "Bad request");
//     });

// };
