const { User } = require("../models/user");
const _ = require("lodash");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const errorMsg = require("./error");
const mongoose = require("mongoose");
const config = require("../config");
var nodemailer = require("nodemailer");
const multer = require("multer");

var Base64 = {
  _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
  encode: function(e) {
    var t = "";
    var n, r, i, s, o, u, a;
    var f = 0;
    e = Base64._utf8_encode(e);
    while (f < e.length) {
      n = e.charCodeAt(f++);
      r = e.charCodeAt(f++);
      i = e.charCodeAt(f++);
      s = n >> 2;
      o = ((n & 3) << 4) | (r >> 4);
      u = ((r & 15) << 2) | (i >> 6);
      a = i & 63;
      if (isNaN(r)) {
        u = a = 64;
      } else if (isNaN(i)) {
        a = 64;
      }
      t =
        t +
        this._keyStr.charAt(s) +
        this._keyStr.charAt(o) +
        this._keyStr.charAt(u) +
        this._keyStr.charAt(a);
    }
    return t;
  },
  decode: function(e) {
    var t = "";
    var n, r, i;
    var s, o, u, a;
    var f = 0;
    e = e.replace(/[^A-Za-z0-9+/=]/g, "");
    while (f < e.length) {
      s = this._keyStr.indexOf(e.charAt(f++));
      o = this._keyStr.indexOf(e.charAt(f++));
      u = this._keyStr.indexOf(e.charAt(f++));
      a = this._keyStr.indexOf(e.charAt(f++));
      n = (s << 2) | (o >> 4);
      r = ((o & 15) << 4) | (u >> 2);
      i = ((u & 3) << 6) | a;
      t = t + String.fromCharCode(n);
      if (u != 64) {
        t = t + String.fromCharCode(r);
      }
      if (a != 64) {
        t = t + String.fromCharCode(i);
      }
    }
    t = Base64._utf8_decode(t);
    return t;
  },
  _utf8_encode: function(e) {
    e = e.replace(/rn/g, "n");
    var t = "";
    for (var n = 0; n < e.length; n++) {
      var r = e.charCodeAt(n);
      if (r < 128) {
        t += String.fromCharCode(r);
      } else if (r > 127 && r < 2048) {
        t += String.fromCharCode((r >> 6) | 192);
        t += String.fromCharCode((r & 63) | 128);
      } else {
        t += String.fromCharCode((r >> 12) | 224);
        t += String.fromCharCode(((r >> 6) & 63) | 128);
        t += String.fromCharCode((r & 63) | 128);
      }
    }
    return t;
  },
  _utf8_decode: function(e) {
    var t = "";
    var n = 0;
    var r = (c1 = c2 = 0);
    while (n < e.length) {
      r = e.charCodeAt(n);
      if (r < 128) {
        t += String.fromCharCode(r);
        n++;
      } else if (r > 191 && r < 224) {
        c2 = e.charCodeAt(n + 1);
        t += String.fromCharCode(((r & 31) << 6) | (c2 & 63));
        n += 2;
      } else {
        c2 = e.charCodeAt(n + 1);
        c3 = e.charCodeAt(n + 2);
        t += String.fromCharCode(
          ((r & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63)
        );
        n += 3;
      }
    }
    return t;
  }
};

var options = {
  service: "Gmail",
  auth: {
    user: config.key.MAIL_SERVER_USER,
    pass: config.key.MAIL_SERVER_PASSWORD
  }
};
var client = nodemailer.createTransport(options);

//Provider.find({ deviceId: { $eq: _deviceId }, isDelete: { $eq: false } })
exports.get_UserDevice = (request, response) => {
  const _deviceId = request.query.deviceId;
  console.log("deviceId", _deviceId);
  // User.findOne({ deviceId: { $eq: _deviceId }, isDelete: { $eq: false } })
  User.findOne({ deviceId: { $eq: _deviceId }, isDelete: { $eq: false } })
    .exec()
    .then(_device => {
      // response.send("result", _device);
      response
        .status(200)
        .send(_device ? _device : { message: "No data found" });
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found");
    });
};

exports.getAllUser = (request, response) => {
  User.find({ isDelete: { $eq: false } })
    .populate("roles")
    .exec()
    .then(result => {
      response.status(200).json({
        result
      });
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found");
    });
};

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function(req, file, cb) {
    // console.log("reqfile", req , "file", file);

    cb(null, file.originalname);
    //cb(null, file.fieldname + '-' + Date.now())
  }
});

exports.upload = multer({ storage: storage });

exports.save = (request, response) => {
  var fileName = "";
  console.log("requestfile", request.body);
  if (request.file) {
    fileName = request.file.filename ? request.file.filename : "";
  }
  // console.log("fileName", fileName);

  var body = request.body;
  var user = new User({
    _id: new mongoose.Types.ObjectId(),
    // email: body.email? body.email : "",
    roles: body.roles,
    firstName: body.firstName ? body.firstName : "",
    lastName: body.lastName ? body.lastName : "",
    phoneNumber: body.phoneNumber ? body.phoneNumber : "",
    profilePhoto: fileName ? fileName : body.profilePhoto,
    deviceId: body.deviceId ? body.deviceId : "",
    deviceName: body.deviceName ? body.deviceName : "",
    latitude: body.latitude ? body.latitude : "",
    longitude: body.longitude ? body.longitude : "",
    gender: body.gender ? body.gender : "",
    birthYear: body.birthYear ? body.birthYear : null,
    notificationPerDay: body.notificationPerDay ? body.notificationPerDay : null
    // password: hash
  });
  var userId = body.Id ? body.Id : null;
  // console.log("deviceId", request.body.deviceId);
  User.find({ deviceId: { $eq: request.body.deviceId } })
    .exec()
    .then(doc => {
      console.log("doc.length", doc.length);
      if (doc.length > 0) {
        // if (userId != null) {
        const updateOps = {};
        //  console.log("doc", doc);
        console.log("id", doc[0]._id);

        if (request.body.firstName)
          updateOps["firstName"] = request.body.firstName;
        if (request.body.lastName)
          updateOps["lastName"] = request.body.lastName;
        if (request.body.phoneNumber)
          updateOps["phoneNumber"] = request.body.phoneNumber;

        if (fileName) updateOps["profilePhoto"] = fileName;

        if (request.body.deviceId)
          updateOps["deviceId"] = request.body.deviceId;

        if (request.body.deviceName)
          updateOps["deviceName"] = request.body.deviceName;

        if (request.body.latitude)
          updateOps["latitude"] = request.body.latitude;
        if (request.body.longitude)
          updateOps["longitude"] = request.body.longitude;
        if (request.body.verified)
          updateOps["verified"] = request.body.verified;

        // var ObjectId = mongoose.Types.ObjectId;
        // if (request.body.roles)
        //   // updateOps["roles"] = {
        //   //   $pushAll: { roles: new ObjectId(request.body.role) }
        //   // }; //request.body.role;
        // updateOps["roles"] = {
        //   $addToSet: { roles: request.body.roles }
        // }; //request.body.role;
        updateOps["roles"] = request.body.roles;
        // var _roles = "";

        // if (request.body.roles) _roles = request.body.roles;

        if (request.body.gender) updateOps["gender"] = request.body.gender;
        if (request.body.birthYear)
          updateOps["birthYear"] = request.body.birthYear;

        if (request.body.notificationPerDay)
          updateOps["notificationPerDay"] = request.body.notificationPerDay;

        updateOps["updatedAt"] = new Date();
        // console.log("updateOps", updateOps["roles"]);
        // console.log("fileName", fileName);
        // console.log("profilePhoto", updateOps["profilePhoto"]);

        return User.update({ _id: doc[0]._id }, { $set: updateOps })
          .exec()
          .then(result => {
            // console.log("result", result);
            // console.log("_roles", _roles);
            // if (_roles) {
            //   console.log("userupdate");
            //   User.update({ _id: doc[0]._id }, { $addToSet: { roles: _roles } })
            //     .exec()
            //     .then(result => {
            //       return response.status(200).json({
            //         message: "User Updated",
            //         result: result
            //       });
            //     });
            // } else {
            return response.status(200).json({
              message: "User Updated",
              result: result
            });
            // }
          })
          .catch(err => {
            console.log(err);
            response.status(500).json({
              error: err
            });
          });
        // } else {
        //   return response.status(500).json({
        //     error: "user already exist"
        //   });
        // }
      } else {
        return user
          .save()
          .then(User => {
            console.log(User);
            response.status(200).json({ User });
          })
          .catch(err => {
            console.log("error", err);
            return errorMsg.errorMessage(
              response,
              400,
              "Error: " + err.message
            );
          });
      }
    })
    .catch(err => {
      console.log(err);
      response.status(500).json({
        error: err
      });
    });
};

exports.login = (request, response) => {
  var body = request.body;
  //  email: body.email,
  User.findOne({
    $or: [{ deviceId: body.deviceId }, { phoneNumber: body.deviceId }]
  })
    .exec()
    .then(result => {
      if (!result) {
        return errorMsg.errorMessage(
          response,
          400,
          "Unable to fetch user info!!!"
        );
      }
      response.status(200).json({
        user: result,
        roles: result.roles,
        firstName: result.firstName,
        lastName: result.lastName,
        refreshtoken: token,
        //email: result.email,
        phoneNumber: result.phoneNumber,
        deviceId: result.deviceId,
        deviceName: result.deviceName,
        uid: result._id,
        latitude: result.latitude,
        longitude: result.longitude,
        gender: result.gender,
        birthYear: result.birthYear,
        notificationPerDay: result.notificationPerDay
      });
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 404, "Not Found");
    });
};

exports.updateUser = (request, response) => {
  const id = request.params.Id;

  const updateOps = {};
  console.log("id", id);

  console.log(" request.body.verified", request.body.verified);

  if (request.body.firstName) updateOps["firstName"] = request.body.firstName;
  if (request.body.lastName) updateOps["lastName"] = request.body.lastName;
  // if (request.body.refreshtoken)
  //   updateOps["refreshtoken"] = request.body.refreshtoken;

  if (request.body.phoneNumber)
    updateOps["phoneNumber"] = request.body.phoneNumber;

  if (request.body.profilePhoto)
    updateOps["profilePhoto"] = request.body.profilePhoto;
  if (request.body.deviceId) updateOps["deviceId"] = request.body.deviceId;

  if (request.body.deviceName)
    updateOps["deviceName"] = request.body.deviceName;

  if (request.body.latitude) updateOps["latitude"] = request.body.latitude;
  if (request.body.longitude) updateOps["longitude"] = request.body.longitude;

  if (!(request.body.verified == null))
    updateOps["verified"] = request.body.verified;

  if (request.body.gender) updateOps["gender"] = request.body.gender;
  if (request.body.birthYear) updateOps["birthYear"] = request.body.birthYear;
  if (request.body.notificationPerDay)
    updateOps["notificationPerDay"] = request.body.notificationPerDay;

  console.log("updateOps", updateOps["verified"]);

  updateOps["updatedAt"] = new Date();
  User.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      console.log(result, result);
      //  response.status(200).json(result);
      response.status(200).json({
        message: "User Updated",
        result: result
      });
    })
    .catch(err => {
      console.log(err);
      response.status(500).json({
        error: err
      });
    });
};

exports.deleteUser = (request, response) => {
  const id = request.params.Id;
  console.log("userId", id);
  var query = { _id: id };
  var update = {
    isDelete: true,
    deletedAt: new Date()
  };
  var options = { new: true };
  User.findByIdAndUpdate(query, update, options, function(err, doc) {
    if (err) return response.status(500).json({ error: err });
    return response.send(doc);
  });
};
