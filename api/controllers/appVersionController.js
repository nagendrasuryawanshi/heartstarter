const { AppVersion } = require("../models/appVersion");
const mongoose = require("mongoose");
const config = require("../config");

exports.getAll = (request, response) => {
  // console.log(request);
  AppVersion.find({ isDelete: { $eq: false } })
    .exec()
    .then(docs => {
      response.status(200).json(docs);
    })
    .catch(err => {
      response.status(500).json({
        error: err
      });
    });
};

//check the app version.......................
exports.checkAppVersion = (request, response) => {
  AppVersion.findOne({ isDelete: { $eq: false } })
    .exec()
    .then(resVersion => {
      let latestApiNumber;
      let currentApiNumber;

      if (resVersion.apiVersion) {
        latestApiNumber = resVersion.apiVersion.toUpperCase().replace("V", "");
      }
      if (request.query.apiVersion) {
        currentApiNumber = request.query.apiVersion
          .toUpperCase()
          .replace("V", "");
      }

      console.log("latestApiNumber", latestApiNumber);
      console.log("currentApiNumber", currentApiNumber);

      var AppVersionDiff =
        parseFloat(resVersion.appVersion) -
        parseFloat(request.query.appVersion);
      console.log("AppVersionDiff", AppVersionDiff);
      //Api version check--------
      if (latestApiNumber > currentApiNumber) {
        return response.status(200).send({
          VersionStatus: config.key.HS_VERSION_STATUS_CODE.MOVED_PERMANENTLY
        });
      } else if (AppVersionDiff > 0 && AppVersionDiff < 1) {
        //App version check----------
        console.log("1");
        return response.status(200).send({
          VersionStatus: config.key.HS_VERSION_STATUS_CODE.LATEST_AVAILABLE
        });
      } else if (AppVersionDiff >= 1 && AppVersionDiff <= 2) {
        console.log("1");
        return response
          .status(200)
          .send({ VersionStatus: config.key.HS_VERSION_STATUS_CODE.GONE });
      } else if (AppVersionDiff > 2) {
        return response.status(200).send({
          VersionStatus: config.key.HS_VERSION_STATUS_CODE.MOVED_PERMANENTLY
        });
      } else {
        return response.status(200).send({
          VersionStatus: -1
        });
      }

      //return response.status(200).send(AppVersion);
    })
    .catch(err => {
      response.status(500).json({
        error: "Not found " + err.message
      });
    });
};
//......................................................

exports.save = (request, response) => {
  console.log("requestappVersions", request.body.appVersion);
  var appVersion = new AppVersion({
    _id: new mongoose.Types.ObjectId(),
    appVersion: request.body.appVersion ? request.body.appVersion : null,
    apiVersion: request.body.apiVersion
      ? request.body.apiVersion.toUpperCase()
      : null
  });
  //AppVersion.find({ appVersion: request.body.appVersion }).then(res => {
  AppVersion.find({ isDelete: { $eq: false } }).then(res => {
    console.log("res.lenght", res.lenght);

    if (Object.keys(res).length > 0) {
      //update the current version...............
      const updateOps = {};
      if (request.body.appVersion)
        updateOps["appVersion"] = request.body.appVersion
          ? request.body.appVersion
          : null;
      if (request.body.apiVersion)
        updateOps["apiVersion"] = request.body.apiVersion
          ? request.body.apiVersion.toUpperCase()
          : null;

      updateOps["updatedAt"] = new Date();
      console.log("updateoption", updateOps["isActive"]);
      AppVersion.update({ _id: res[0]._id }, { $set: updateOps })
        .exec()
        .then(AppVersion => {
          console.log(AppVersion);
          response.status(200).json({
            message: "AppVersion updated",
            AppVersion
          });
        })
        .catch(err => {
          console.log(err);
          response.status(500).json({
            error: err
          });
        });
    } else {
      //--save new version---------------------
      appVersion
        .save()
        .then(result => {
          console.log("resultresult", result);
          response.status(201).json(result);
        })
        .catch(err => {
          response.status(400).json({
            message: err
          });
          console.log("error", err);
        });
    }
  });
};

exports.update = (request, response) => {
  const id = request.params.Id;

  console.log("id", id);

  const updateOps = {};
  if (request.body.appVersion)
    updateOps["appVersion"] = request.body.appVersion
      ? request.body.appVersion
      : null;
  if (request.body.apiVersion)
    updateOps["apiVersion"] = request.body.apiVersion
      ? request.body.apiVersion
      : null;
  updateOps["updatedAt"] = new Date();
  console.log("updateoption", updateOps["isActive"]);
  AppVersion.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(AppVersion => {
      console.log(AppVersion);
      response.status(200).json({
        message: "AppVersion updated",
        AppVersion
      });
    })
    .catch(err => {
      console.log(err);
      response.status(500).json({
        error: err
      });
    });
};
