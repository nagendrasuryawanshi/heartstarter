const { ApiVersion } = require("../models/apiVersion");
const mongoose = require("mongoose");

exports.getAll = (request, response) => {
  // console.log(request);
  ApiVersion.find({ isDelete: { $eq: false } })
    //.select("isDelete _id apiVersion isActive")
    //.populate("user")
    .exec()
    .then(docs => {
      console.log("res.lenght", docs.Count);
      console.log("length", Object.keys(docs).length);

      console.log(docs);
      response.status(200).json(docs);
    })
    .catch(err => {
      console.log(err);
      response.status(500).json({
        error: err
      });
    });
};

exports.getlatestversion = (request, response) => {
  console.log(request);
  ApiVersion.find({ isActive: { $eq: true } })
    .select("_id apiVersion statusCode")
    //.populate("user")
    .exec()
    .then(docs => {
      console.log(docs);
      response.status(200).json(docs);
    })
    .catch(err => {
      console.log(err);
      response.status(500).json({
        error: err
      });
    });
};

exports.get = (request, response) => {
  const Id = request.params.Id;
  console.log("ID", Id);
  ApiVersion.find({ _id: Id })
    .exec()
    .then(ApiVersion => {
      response.status(200).send(ApiVersion);
    })
    .catch(err => {
      response.status(500).json({
        error: "Not found " + err.message
      });
    });
};

exports.getApiVersion = (request, response) => {
  const version = request.query.apiVersion;
  console.log("version", version);
  ApiVersion.findOne({ apiVersion: { $eq: version.toUpperCase() } })
    .exec()
    .then(ApiVersion => {
      console.log("api version :", ApiVersion);
      return response.status(200).send(ApiVersion);
    })
    .catch(err => {
      response.status(500).json({
        error: "Not found " + err.message
      });
    });
};

exports.save = (request, response) => {
  console.log("requestapiVersions", request.body.apiVersion);
  var apiVersion = new ApiVersion({
    _id: new mongoose.Types.ObjectId(),
    apiVersion: request.body.apiVersion
      ? request.body.apiVersion.toUpperCase()
      : "",
    statusCode: request.body.statusCode ? request.body.statusCode : "0",
    //0 (Still in work) 301 (Moved Permanently) or 410 (Gone)
    isActive: request.body.isActive ? request.body.isActive : false
  });
  ApiVersion.find({ apiVersion: request.body.apiVersion }).then(res => {
    console.log("res.lenght", res.lenght);
    if (Object.keys(res).length > 0) {
      response.status(500).json({
        message: "Version already exist."
      });
    } else {
      apiVersion
        .save()
        .then(result => {
          console.log("resultresult", result);
          response.status(201).json(result);
        })
        .catch(err => {
          response.status(400).json({
            message: err
          });
          console.log("deverr", err);
        });
    }
  });
};

exports.update = (request, response) => {
  const id = request.params.Id;

  console.log("id", id);
  console.log("isActive", request.body.isActive);
  const updateOps = {};

  if (request.body.apiVersion)
    updateOps["apiVersion"] = request.body.apiVersion
      ? request.body.apiVersion.toUpperCase()
      : null;

  if (request.body.isActive)
    updateOps["isActive"] = request.body.isActive
      ? request.body.isActive
      : null;

  if (request.body.statusCode)
    updateOps["statusCode"] = request.body.statusCode
      ? request.body.statusCode
      : null;

  updateOps["updatedAt"] = new Date();
  console.log("updateoption", updateOps["isActive"]);
  ApiVersion.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(ApiVersion => {
      console.log(ApiVersion);
      response.status(200).json({
        message: "ApiVersion updated",
        ApiVersion
      });
    })
    .catch(err => {
      console.log(err);
      response.status(500).json({
        error: err
      });
    });
};

exports.delete = (request, response) => {
  const id = request.params.Id;
  console.log("userId", id);
  var query = { _id: id };
  var update = {
    isDelete: true,
    deletedAt: new Date()
  };
  var options = { new: true };
  ApiVersion.findByIdAndUpdate(query, update, options, function(err, doc) {
    if (err) return response.status(500).json({ error: err });
    return response.send(doc);
  });
};
