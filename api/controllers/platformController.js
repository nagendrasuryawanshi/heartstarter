const { Platform } = require("../models/platform");
const mongoose = require("mongoose");

exports.getAll = (request, response) => {
  Platform.find({ isDelete: { $eq: false } })
    .select("_id  deviceId serial brand model")
    .exec()
    .then(Platforms => {
      console.log(Platforms);
      response.status(200).json({ Platforms });
    })
    .catch(err => {
      console.log(err);
      response.status(500).json({
        error: err
      });
    });
};

exports.get = (request, response) => {
  const _deviceId = request.query.deviceId;
  console.log("id: ", _deviceId);

  Platform.findOne({ deviceId: _deviceId })
    .exec()
    .then(Platform => {
      response.status(200).send(Platform);
    })
    .catch(err => {
      //response, 400, "Not found " + err.message);
      response.status(500).json({
        error: "Not found " + err.message
      });
    });
};

exports.save = (request, response) => {
  // const denver = { type: 'Point', coordinates: [-104.9903, 39.7392] };
  var platform = new Platform({
    _id: new mongoose.Types.ObjectId(),
    deviceId: request.body.deviceId ? request.body.deviceId : "",
    location: request.body.location
      ? { type: "Point", coordinates: request.body.location }
      : null,
    serial: request.body.serial ? request.body.serial : "",
    brand: request.body.brand ? request.body.brand : "",
    model: request.body.model ? request.body.model : ""
  });

  platform
    .save()
    .then(result => {
      console.log("resultresult", result);
      return response.status(200).json(result);
    })
    .catch(err => {
      response.status(401).json({
        message: err
      });
      console.log("deverr", err);
    });
};

exports.update = (request, response) => {
  const id = request.params.Id;

  console.log("id", id);

  const updateOps = {};

  updateOps["deviceId"] = request.body.deviceId ? request.body.deviceId : null;
  updateOps["location"] = request.body.location
    ? { type: "Point", coordinates: request.body.location }
    : null;
  updateOps["serial"] = request.body.serial ? request.body.serial : null;
  updateOps["brand"] = request.body.brand ? request.body.brand : null;
  updateOps["model"] = request.body.model ? request.body.model : null;

  updateOps["updatedAt"] = new Date();
  Platform.update({ deviceId: id }, { $set: updateOps })
    .exec()
    .then(Platform => {
      console.log(Platform);
      response.status(200).json({
        message: "Platform updated",
        Platform
      });
    })
    .catch(err => {
      console.log(err);
      response.status(500).json({
        error: err
      });
    });
};

exports.delete = (request, response) => {
  const id = request.params.Id;
  console.log("userId", id);
  var query = { _id: id };
  var update = {
    isDelete: true,
    deletedAt: new Date()
  };
  var options = { new: true };
  Platform.findByIdAndUpdate(query, update, options, function(err, doc) {
    if (err) return response.status(500).json({ error: err });
    return response.send(doc);
  });
};
