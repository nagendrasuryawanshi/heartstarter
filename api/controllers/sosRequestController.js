const { SOSRequest } = require("../models/SOSRequest");

const errorMsg = require("./error");
const mongoose = require("mongoose");

exports.getAll = (request, response) => {
  SOSRequest.find({ isDelete: { $eq: false } })
    .select(
      "_id deviceId phoneNumber  uid  status requestTime location  isDelete"
    )
    .exec()
    .then(SOSRequest => {
      response.status(200).json({
        SOSRequest
      });
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found");
    });
};

exports.getAllActive = (request, response) => {
  SOSRequest.find({
    status: { $eq: "Active" },
    isDelete: { $eq: false }
  })
    .select(
      "_id deviceId phoneNumber  uid  status requestTime location  isDelete"
    )
    .exec()
    .then(SOSRequest => {
      console.log("SOSRequest", SOSRequest);
      response.status(200).json({
        SOSRequest
      });
    })
    .catch(err => {
      console.log("err", err);
      return errorMsg.errorMessage(response, 400, "Not found");
    });
};

exports.getAll_V2 = (request, response) => {
  SOSRequest.find({ isDelete: { $eq: false } })
    .select(
      "_id deviceId phoneNumber  uid  status requestTime  location isDelete"
    )
    .exec()
    .then(SOSRequest => {
      response.status(200).json({
        SOSRequest
      });
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found");
    });
};
exports.get = (request, response) => {
  console.log("request.params.Id", request.params.Id);
  var _sosRequestId = "",
    _sosRequestId = request.query.Id ? request.query.Id : null;

  if (request.params.Id)
    _sosRequestId = request.params.Id ? request.params.Id : null;

  // User.findOne({ deviceId: { $eq: _deviceId }, isDelete: { $eq: false } })
  SOSRequest.findOne({ _id: _sosRequestId })
    .select(
      "_id deviceId phoneNumber  uid  status requestTime location  isDelete"
    )
    .exec()
    .then(SOSRequest => {
      // response.send("SOSRequest", _sosRequestId);
      response.status(200).send(SOSRequest);
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found " + err.message);
    });
};
exports.get_V2 = (request, response) => {
  console.log("request.params.Id", request.params.Id);
  var _sosRequestId = "",
    _sosRequestId = request.query.Id ? request.query.Id : null;

  if (request.params.Id)
    _sosRequestId = request.params.Id ? request.params.Id : null;

  // User.findOne({ deviceId: { $eq: _deviceId }, isDelete: { $eq: false } })
  SOSRequest.findOne({ _id: _sosRequestId })
    .exec()
    .then(SOSRequest => {
      response.status(200).send(SOSRequest);
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found " + err.message);
    });
};
exports.save = (request, response) => {
  //console.log("request", request.body);
  var body = request.body;

  // SOSRequest.find({
  //   phoneNumber,
  //   uid,
  //   status,
  //   requestTime
  // })
  //   .exec()
  //   .then(result => {
  //     if (result && result.length > 0) {
  //       console.log("sos Request already exists");
  //       return response.status(401).json({
  //         status: 401,
  //         error: "SOSRequest '" + body.sosRequest + "' already exists"
  //       });
  //     }
  var sosRequest = new SOSRequest({
    _id: new mongoose.Types.ObjectId(),
    phoneNumber: body.phoneNumber ? body.phoneNumber : null,
    uid: body.uid ? body.uid : null,
    deviceId: body.deviceId ? body.deviceId : null,
    //status: body.status ? body.status : null,
    requestTime: body.requestTime ? body.requestTime : new Date(),

    location: request.body.location
      ? { type: "Point", coordinates: request.body.location }
      : null
  });
  return sosRequest
    .save()
    .then(SOSRequest => {
      //console.log(SOSRequest);
      response.status(200).json({ SOSRequest });
    })
    .catch(err => {
      console.log("error", err);
      return errorMsg.errorMessage(response, 400, "Error: " + err.message);
    });
  // });
};

exports.update = (request, response) => {
  const id = request.params.Id;

  console.log("id", id);
  const updateOps = {};

  if (request.body.phoneNumber)
    updateOps["phoneNumber"] = request.body.phoneNumber;

  if (request.body.uid) updateOps["uid"] = request.body.uid;

  if (request.body.status) updateOps["status"] = request.body.status;

  if (request.body.requestTime)
    updateOps["requestTime"] = request.body.requestTime;

  if (request.body.deviceId) updateOps["deviceId"] = request.body.deviceId;

  if (request.body.isDelete != null)
    updateOps["isDelete"] = request.body.isDelete;

  if (request.body.location != null)
    (updateOps["location"] = request.body.location
      ? { type: "Point", coordinates: request.body.location }
      : null),
      (updateOps["updatedAt"] = new Date());
  // if (request.body.sosRequest) {
  //   SOSRequest.find({
  //     _id: { $ne: id }
  //   })
  //     .exec()
  //     .then(result => {
  //       if (result && result.length > 0) {
  //         console.log("SOS Request already exists");
  //         return response.status(401).json({
  //           status: 401,
  //           error: "SOSRequest '" + request.body.sosRequest + "' already exists"
  //         });
  //       }
  //     });
  // }
  SOSRequest.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(SOSRequest => {
      console.log(SOSRequest);
      response.status(200).json({
        message: "SOS request updated",
        SOSRequest
      });
    })
    .catch(err => {
      console.log(err);
      response.status(500).json({
        error: err
      });
    });
};

exports.delete = (request, response) => {
  const id = request.params.Id;
  console.log("SOSRequest", id);

  SOSRequest.findOne({ _id: id })
    .exec()
    .then(result => {
      if (!result) {
        return response.status(400).json({ message: "SOS request not found" });
      } else {
        var query = { _id: id };
        var update = {
          isDelete: true,
          deletedAt: new Date()
        };
        var options = { new: true };
        SOSRequest.findByIdAndUpdate(query, update, options, function(
          err,
          doc
        ) {
          if (err) return res.status(500).json({ error: err });
          return response.send(doc);
        });
      }
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found " + err.message);
    });
};
