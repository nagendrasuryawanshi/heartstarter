const { DeviceType } = require("../models/deviceType");

const errorMsg = require("./error");
const mongoose = require("mongoose");

exports.getAll = (request, response) => {
  DeviceType.find({ isDelete: { $eq: false } })
    .select("_Id deviceType isDelete")
    .exec()
    .then(DeviceType => {
      response.status(200).json({
        DeviceType
      });
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found");
    });
};
exports.getAll_V2 = (request, response) => {
  DeviceType.find({ isDelete: { $eq: false } })
    .select("_Id deviceType")
    .exec()
    .then(DeviceType => {
      response.status(200).json({
        DeviceType
      });
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found");
    });
};
exports.get = (request, response) => {
  console.log("request.params.Id", request.params.Id);
  var _devicetypeId = "",
    _devicetypeId = request.query.Id ? request.query.Id : null;

  if (request.params.Id)
    _devicetypeId = request.params.Id ? request.params.Id : null;

  console.log("deviceTypeId", _devicetypeId);
  // User.findOne({ deviceId: { $eq: _deviceId }, isDelete: { $eq: false } })
  DeviceType.findOne({ _id: _devicetypeId })
    .select("_Id deviceType isDelete")
    .exec()
    .then(DeviceType => {
      // response.send("DeviceType", _devicetypeId);
      response.status(200).send(DeviceType);
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found " + err.message);
    });
};
exports.get_V2 = (request, response) => {
  console.log("request.params.Id", request.params.Id);
  var _devicetypeId = "",
    _devicetypeId = request.query.Id ? request.query.Id : null;

  if (request.params.Id)
    _devicetypeId = request.params.Id ? request.params.Id : null;

  console.log("deviceTypeId", _devicetypeId);
  // User.findOne({ deviceId: { $eq: _deviceId }, isDelete: { $eq: false } })
  DeviceType.findOne({ _id: _devicetypeId })
    .select("_Id deviceType")
    .exec()
    .then(DeviceType => {
      response.status(200).send(DeviceType);
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found " + err.message);
    });
};
exports.save = (request, response) => {
  //console.log("request", request.body);
  var body = request.body;

  DeviceType.find({ deviceType: body.deviceType, isDelete: { $eq: false } })
    .exec()
    .then(result => {
      if (result && result.length > 0) {
        console.log("Device Type already exists");
        return response.status(401).json({
          status: 401,
          error: "DeviceType '" + body.deviceType + "' already exists"
        });
      }
      var deviceType = new DeviceType({
        _id: new mongoose.Types.ObjectId(),
        deviceType: body.deviceType ? body.deviceType : ""
      });
      return deviceType
        .save()
        .then(DeviceType => {
          //console.log(DeviceType);
          response.status(200).json({ DeviceType });
        })
        .catch(err => {
          console.log("error", err);
          return errorMsg.errorMessage(response, 400, "Error: " + err.message);
        });
    });
};

exports.update = (request, response) => {
  const id = request.params.Id;

  console.log("id", id);
  const updateOps = {};

  // for (const ops of request.body) {
  //   if (req.body.hasOwnProperty(ops)) {
  //     updateOps[ops.prop] = ops.value;
  //   }
  // }

  if (request.body.deviceType)
    updateOps["deviceType"] = request.body.deviceType;

  if (request.body.isDelete) updateOps["isDelete"] = request.body.isDelete;

  updateOps["updatedAt"] = new Date();
  if (request.body.deviceType) {
    DeviceType.find({ deviceType: request.body.deviceType, _id: { $ne: id } })
      .exec()
      .then(result => {
        if (result && result.length > 0) {
          console.log("Device Type already exists");
          return response.status(401).json({
            status: 401,
            error: "DeviceType '" + request.body.deviceType + "' already exists"
          });
        }
      });
  }
  DeviceType.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(DeviceType => {
      console.log(DeviceType);
      response.status(200).json({
        message: "Device type updated",
        DeviceType
      });
    })
    .catch(err => {
      console.log(err);
      response.status(500).json({
        error: err
      });
    });
};

exports.delete = (request, response) => {
  const id = request.params.Id;
  console.log("deviceType", id);

  DeviceType.findOne({ _id: id })
    .exec()
    .then(result => {
      if (!result) {
        return response.status(400).json({ message: "Device type not found" });
      } else {
        var query = { _id: id };
        var update = {
          isDelete: true,
          deletedAt: new Date()
        };
        var options = { new: true };
        DeviceType.findByIdAndUpdate(query, update, options, function(
          err,
          doc
        ) {
          if (err) return res.status(500).json({ error: err });
          return response.send(doc);
        });
      }
    })
    .catch(err => {
      return errorMsg.errorMessage(response, 400, "Not found " + err.message);
    });
};
