const { Device } = require("../models/device");
const mongoose = require("mongoose");
//const reqPath = "/devices";
var multer = require("multer");
path = require("path");

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function(req, file, cb) {
    // console.log("reqfile", req , "file", file);

    cb(null, file.originalname);
    //cb(null, file.fieldname + '-' + Date.now())
  }
});

exports.upload = multer({ storage: storage });

// exports.upload = multer({
//   storage: multer.diskStorage({
//     destination: function(req, file, callback) {
//       console.log("file.originalname1", file.originalname);
//       callback(null, "./uploads");
//     },
//     filename: function(req, file, callback) {
//       console.log("file.originalname2", file.originalname);
//       callback(
//         null,
//         file.fieldname + "-" + Date.now() + path.extname(file.originalname)
//       );
//     }
//   }),

//   fileFilter: function(req, file, callback) {
//     var ext = path.extname(file.originalname);
//     if (ext !== ".png" && ext !== ".jpg" && ext !== ".gif" && ext !== ".jpeg") {
//       return callback(/*res.end('Only images are allowed')*/ null, false);
//     }
//     callback(null, true);
//   }
// });

exports.getAll = (request, response) => {
  Device.find({ isDelete: { $eq: false } })
    .select(
      "isDelete _id user deviceType latitude longitude locationNote phoneNumber image1 image2 image3"
    )
    .populate("deviceType")
    .populate("user")
    .exec()
    .then(docs => {
      console.log(docs);
      response.status(200).json(docs);
    })
    .catch(err => {
      console.log(err);
      response.status(500).json({
        error: err
      });
    });
};

exports.getUserDevice = (request, response) => {
  const uid = request.query.uid;
  console.log("uid", uid);
  if (uid) {
    Device.find({ user: uid, isDelete: { $eq: false } }) //, { isDelete: { $eq: false } }
      .select(
        "isDelete _id user deviceType latitude longitude locationNote phoneNumber image1 image2 image3"
      )
      .populate("user")
      .exec()
      .then(docs => {
        console.log(docs);
        response.status(200).json(docs);
      })
      .catch(err => {
        console.log(err);
        response.status(500).json({
          error: err
        });
      });
  } else {
    response.status(500).json({
      error: "User not defined! Select user."
    });
  }
};
exports.get = (request, response) => {
  const _deviceId = request.params.Id;
  // const _uid = request.params.uid;
  console.log("id: ", _deviceId);
  // User.findOne({ deviceId: { $eq: _deviceId }, isDelete: { $eq: false } })
  Device.find({ _id: _deviceId })
    .exec()
    .then(Device => {
      response.status(200).send(Device);
    })
    .catch(err => {
      //response, 400, "Not found " + err.message);
      response.status(500).json({
        error: "Not found " + err.message
      });
    });
};

exports.save = (request, response) => {
  console.log("requestfile", request.file);
  console.log("requestdevices", request.files);
  const id = request.params.Id;
  const uid = request.params.uid;
  // console.log("filename1", request.files[0].filename);
  // console.log("filename2", request.files[1].filename);
  // console.log("filename3", request.files[2].filename);


  var fileName = "";
  if (request.file) {
    fileName = request.file.filename ? request.file.filename : "";
  }

  var device = new Device({
    _id: new mongoose.Types.ObjectId(),
    user: request.body.uid ? request.body.uid : "",
    deviceType: request.body.deviceTypeId ? request.body.deviceTypeId : "",
    latitude: request.body.latitude ? request.body.latitude : "",
    longitude: request.body.longitude ? request.body.longitude : "",
    locationNote: request.body.locationNote ? request.body.locationNote : "",
    phoneNumber: request.body.phoneNumber ? request.body.phoneNumber : "",
    image1:fileName
    // image1: request.files[0] ? request.files[0].filename : "",
    // image2: request.files[1] ? request.files[1].filename : "",
    // image3: request.files[2] ? request.files[2].filename : ""
  });

  console.log("request.body.phoneNumber", request.body.phoneNumber);
  // Device.findOne({ phoneNumber: { $eq: request.body.phoneNumber } })
  //   .exec()
  //   .then(doc => {
  //     console.log(doc);
  //     if (!doc) {
  device
    .save()
    .then(result => {
      console.log("resultresult", result);
      return response.status(200).json(result);
      console.log("devesss");
    })
    .catch(err => {
      response.status(401).json({
        message: err
      });
      console.log("deverr", err);
    });
  //   } else {
  //     response.status(400).json({
  //       message: "Device already exist."
  //     });
  //   }
  // })
  // .catch(err => {
  //   console.log(err);
  //   response.status(500).json({
  //     error: err
  //   });
  // });
};

exports.update = (request, response) => {
  const id = request.params.Id;
  const uid = request.params.uid;
  console.log("filename1", request.files[0].filename);
  console.log("filename2", request.files[1].filename);
  console.log("id", id);
  //console.log("Device", request.body.Device);
  const updateOps = {};

  updateOps["latitude"] = request.body.latitude ? request.body.latitude : null;
  updateOps["longitude"] = request.body.longitude
    ? request.body.longitude
    : null;
  updateOps["locationNote"] = request.body.locationNote
    ? request.body.locationNote
    : null;
  updateOps["phoneNumber"] = request.body.phoneNumber
    ? request.body.phoneNumber
    : null;
  updateOps["image1"] = request.body.image1 ? request.body.image1 : null;

  updateOps["updatedAt"] = new Date();
  Device.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(Device => {
      console.log(Device);
      response.status(200).json({
        message: "Device updated",
        Device
      });
    })
    .catch(err => {
      console.log(err);
      response.status(500).json({
        error: err
      });
    });
};

exports.delete = (request, response) => {
  const id = request.params.Id;
  console.log("userId", id);
  var query = { _id: id };
  var update = {
    isDelete: true,
    deletedAt: new Date()
  };
  var options = { new: true };
  Device.findByIdAndUpdate(query, update, options, function(err, doc) {
    if (err) return response.status(500).json({ error: err });
    return response.send(doc);
  });
};
