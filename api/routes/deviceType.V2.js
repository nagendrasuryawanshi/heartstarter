const express = require("express");
const router = express.Router();
const deviceTypeController = require("../controllers/deviceTypeController");

router.get("/deviceType/getAll", deviceTypeController.getAll_V2);
router.get("/deviceType/get", deviceTypeController.get_V2);

router.post("/deviceType/save", deviceTypeController.save);
router.patch("/deviceType/update/:Id", deviceTypeController.update);
router.delete("/deviceType/delete/:Id", deviceTypeController.delete);

module.exports = router;
