const express = require("express");
const router = express.Router();
const medicalDeviceController = require("../controllers/medicalDeviceController");

router.get("/medicaldevice/getAll", medicalDeviceController.getAll);
router.get(
  "/medicaldevice/getUserDevice",
  medicalDeviceController.getUserDevice
);
router.get("/medicaldevice/get/:Id", medicalDeviceController.get);
router.post(
  "/medicaldevice/save",
  //medicalDeviceController.upload.array("image"),
  medicalDeviceController.upload.any(),
  medicalDeviceController.save
);
router.patch(
  "/medicaldevice/update/:Id",
  medicalDeviceController.upload.single("image"),
  medicalDeviceController.update
);
router.patch("/medicaldevice/delete/:Id", medicalDeviceController.delete);

module.exports = router;
