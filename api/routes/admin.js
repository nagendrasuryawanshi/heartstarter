const express = require("express");
const router = express.Router();
const adminController = require("../controllers/admincontroller");
const authMiddleware = require("../middleware/auth");

router.get("/admin/get/:Id", adminController.get_Admin);
router.post("/admin/signup", adminController.sign_up);
router.post("/admin/login", adminController.login);
router.post("/admin/update/:Id", adminController.login);
//router.delete("/admin/delete/:Id", adminController.deleteAdmin);
module.exports = router;
