const express = require("express");
const router = express.Router();
const rolesController = require("../controllers/rolesController");

router.get("/roles/getAll", rolesController.getAll);
router.get("/roles/get", rolesController.get);

router.post("/roles/save", rolesController.save);
router.patch("/roles/update/:Id", rolesController.update);
router.delete("/roles/delete/:Id", rolesController.delete);

module.exports = router;
