const express = require("express");
const router = express.Router();

const appVersionControl = require("../controllers/appVersionController");

router.get("/appversion/getAll", appVersionControl.getAll);
router.get("/appversion/checkAppVersion", appVersionControl.checkAppVersion);
router.post("/appversion/save", appVersionControl.save);
router.patch("/appversion/update/:Id", appVersionControl.update);

module.exports = router;
