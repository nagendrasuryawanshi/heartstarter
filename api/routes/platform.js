const express = require("express");
const router = express.Router();
const platformControl = require("../controllers/platformController");

router.get("/platform/getAll", platformControl.getAll);
router.get("/platform/getbydeviceId", platformControl.get);
router.post("/platform/save", platformControl.save);
router.patch("/platform/update/:Id", platformControl.update);
router.delete("/platform/delete/:Id", platformControl.delete);

module.exports = router;
