const express = require("express");
const router = express.Router();
const deviceControl = require("../controllers/devicecontroller");

router.get("/device/getAll", deviceControl.getAll);
router.get("/device/getUserDevice", deviceControl.getUserDevice);
router.get("/device/get/:Id", deviceControl.get);
router.post(
  "/device/save",
  //deviceControl.upload.array("image"),
  deviceControl.upload.single("image1"),
  //deviceControl.upload.any(),
  deviceControl.save
);
router.patch(
  "/device/update/:Id",
  deviceControl.upload.single("image"),
  deviceControl.update
);
router.patch("/device/delete/:Id", deviceControl.delete);

module.exports = router;
