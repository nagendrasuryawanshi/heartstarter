const express = require("express");
const router = express.Router();
const sosRequestController = require("../controllers/sosRequestController");
const authMiddleware = require("../middleware/auth");

router.get("/sosrequest/getAll", sosRequestController.getAll);
router.get("/sosrequest/getAllActive", sosRequestController.getAllActive);
router.get("/sosrequest/get/:Id", sosRequestController.get);
router.post("/sosrequest/save", sosRequestController.save);
router.post("/sosrequest/update/:Id", sosRequestController.update);
router.delete("/sosrequest/delete/:Id", sosRequestController.delete);
module.exports = router;
