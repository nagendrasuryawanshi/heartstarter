const express = require("express");
const router = express.Router();
const userController = require("../controllers/usercontroller");
const authMiddleware = require("../middleware/auth");

router.get("/users/getAll", userController.getAllUser);
router.get("/users/get", userController.get_UserDevice);

router.post(
  "/users/save",
  userController.upload.single("profilePhoto"),
  userController.save
);
router.post("/users/update/:Id", userController.updateUser);
router.delete("/users/delete/:Id", userController.deleteUser);
module.exports = router;
