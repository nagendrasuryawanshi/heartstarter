const express = require("express");
const router = express.Router();
const rolesController = require("../controllers/rolesController");

router.get("/userType/getAll", rolesController.getAll_V2);
router.get("/userType/get", rolesController.get_V2);

router.post("/userType/save", rolesController.save);
router.patch("/userType/update/:Id", rolesController.update);
router.delete("/userType/delete/:Id", rolesController.delete);

module.exports = router;
