const express = require("express");
const router = express.Router();
const cprStepsController = require("../controllers/cprStepsController");

router.get("/cprsteps/getAll", cprStepsController.getAll);
router.get("/cprsteps/get:Id", cprStepsController.get);

router.post(
  "/cprsteps/save",
  cprStepsController.upload.single("stepImage"),
  cprStepsController.save
);
router.patch(
  "/cprsteps/update/:Id",
  cprStepsController.upload.single("stepImage"),
  cprStepsController.update
);
router.delete("/cprsteps/delete/:Id", cprStepsController.delete);

module.exports = router;
