const express = require("express");
const router = express.Router();
const sosRequestStatusController = require("../controllers/sosRequestStatusController");
const authMiddleware = require("../middleware/auth");

router.get("/sosrequeststatus/getAll", sosRequestStatusController.getAll);
router.get("/sosrequeststatus/get/:Id", sosRequestStatusController.get);
router.post("/sosrequeststatus/save", sosRequestStatusController.save);
router.post("/sosrequeststatus/update/:Id", sosRequestStatusController.update);
router.delete(
  "/sosrequeststatus/delete/:Id",
  sosRequestStatusController.delete
);
module.exports = router;
