const express = require("express");
const router = express.Router();
const versionControl = require("../controllers/apiVersionController");

router.get("/apiversion/getApiVersion", versionControl.getApiVersion);
router.get("/apiversion/getlatestversion", versionControl.getlatestversion);
router.get("/apiversion/getAll", versionControl.getAll);
router.get("/apiversion/get", versionControl.get);
router.post("/apiversion/save", versionControl.save);
router.patch("/apiversion/update/:Id", versionControl.update);
router.patch("/apiversion/delete/:Id", versionControl.delete);

module.exports = router;
