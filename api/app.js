//const express = require("express");
//const app = express();
const morgan = require("morgan");
//const bodyParser = require("body-parser");
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
//------------
var express = require("express");
var path = require("path");
var favicon = require("serve-favicon");
var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");

var routes = require("./routes/index");
var usersdemo = require("./routes/usersdemo");

const apiVersionRouter = require("./routes/apiVersion");
const appVersionRouter = require("./routes/appVersion");
// -------------V1-----------------
const userRouter = require("./routes/users");
//const medicalDeviceRouter = require("./routes/medicalDevice");
const deviceRouter = require("./routes/devices");
const rolesRouter = require("./routes/roles");
const deviceTypeRouter = require("./routes/deviceType");
const adminRouter = require("./routes/admin");
const cprStepsRouter = require("./routes/cprSteps");
const sosRequestStatusRouter = require("./routes/sosRequestStatus");
const sosRequestRouter = require("./routes/sosRequest");
const platformRouter = require("./routes/platform");
//-----------------------------------------------------

//-----------V2------------------------------------------
const rolesRouter_V2 = require("./routes/roles.V2");
const deviceTypeRouter_V2 = require("./routes/deviceType.V2");
//-------------------------------------------------------

var app = express();

const config = require("./config");

var db = config.key.MONGODB_URL;

mongoose.connect(
  db,
  function(err, response) {
    if (err) {
      console.log(" Failed to connected to " + db);
    } else {
      console.log(" Connected to " + db);
    }
  },
  { useNewUrlParser: true }
);

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use("/uploads", () => {
  console.log(path.join(), "+++++++++");
});

app.use((request, response, next) => {
  response.header("Access-Control-Allow-Origin", "*");
  response.header("Access-Control-Allow-Headers", "*");
  response.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  response.header(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, DELETE, OPTIONS"
  );
  if (request.method === "OPTIONS") {
    response.header(
      "Access-Control-Allow-Method",
      "PUT, PATCH, DELETE, GET, POST"
    );
    // console.log("apiRequest", request);
    return response.status(200).json({});
  }
  next();
});

//make public uploads folder--
app.use(express.static("uploads"));
app.use("/uploads", express.static("uploads"));
//------------

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", routes);
app.use("/users", usersdemo);
app.use("/v1", apiVersionRouter);
app.use("/v2", apiVersionRouter);
app.use("/v3", apiVersionRouter);
app.use("/v4", apiVersionRouter);

app.use("/v1", appVersionRouter);
app.use("/v2", appVersionRouter);
app.use("/v3", appVersionRouter);
app.use("/v4", appVersionRouter);

app.use("/v1", adminRouter);
app.use("/v1", rolesRouter);
app.use("/v1", userRouter);
app.use("/v1", deviceRouter);
app.use("/v1", deviceTypeRouter);
app.use("/v1", cprStepsRouter);
app.use("/v1", sosRequestStatusRouter);
app.use("/v1", sosRequestRouter);
app.use("/v1", platformRouter);

app.use("/v2", rolesRouter_V2);
app.use("/v2", userRouter);
app.use("/v2", deviceTypeRouter_V2);
app.use("/v2", deviceRouter);

app.use("/v3", rolesRouter);
app.use("/v3", userRouter);
app.use("/v3", deviceRouter);
app.use("/v3", deviceTypeRouter);

app.use("/v4", rolesRouter);
app.use("/v4", userRouter);
app.use("/v4", deviceRouter);
app.use("/v4", deviceTypeRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get("env") === "development") {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render("error", {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render("error", {
    message: err.message,
    error: {}
  });
});

module.exports = app;
