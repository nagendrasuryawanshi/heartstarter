exports.key = {
  API_DEFAULT_VERSIONS: "v1",
  HS_USER_KEY: "heartstarter_pass_key",
  MONGODB_URL: "mongodb://localhost:27017/HeartStarter",
  HS_VERSION_STATUS_CODE: {
    LATEST_AVAILABLE: 0, // Only Alert
    MOVED_PERMANENTLY: 301, // Must Need to Update
    GONE: 410 //Read Only,
  }
};
