import React from 'react';
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { history } from '../_helpers';
import { alertActions } from '../_actions';
import { PrivateRoute } from '../_components';
import { HomePage } from '../HomePage';
import { LoginPage } from '../LoginPage';
import { RegisterPage } from '../RegisterPage';
// import { SidebarPage } from '../SidebarPage';
import { UsersPage } from '../UsersPage';
import { DevicePage } from '../DevicesPage';
import { ForgotPasswordPage } from '../ForgotPasswordPage';
import { SOSPage } from '../SOSPage';
import { SettingPage } from '../SettingPage';

class App extends React.Component {
    constructor(props) {
        super(props);

        const { dispatch } = this.props;
        history.listen((location, action) => {
            // clear alert on location change
            dispatch(alertActions.clear());
        });
    }

    render() {
        const { alert } = this.props;
        return (
            // <div>
            //     {alert.message &&
            //         <div className={`alert ${alert.type}`}>{alert.message}</div>
            //     }
                <Router history={history}>
                    <div>
                        <PrivateRoute exact path="/" component={HomePage} />
                        <Route path="/login" component={LoginPage} />                             
                        <Route path="/register" component={RegisterPage} />
                        {/* <Route path="/sidebar" component={SidebarPage} />*/}
                        <Route path="/users" component={UsersPage} /> 
                        <Route path="/device" component={DevicePage} />
                        <Route path="/sos" component={SOSPage} />
                        <Route path="/setting" component={SettingPage} />
                        <Route path="/forgot" component={ForgotPasswordPage} />
                    </div>
                </Router>
            // </div>
        );
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 