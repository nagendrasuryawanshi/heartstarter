import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../_actions';

class SidebarPage extends React.Component {


    render() {
        return (
            <div className="sidebar" id="sidebar">
            <div className="block-sidebar">
              <div className="block--elm">
                 <div className="block--elm-header">
                    <img src="../src/assets/img/logo.png"/>
                 </div> 
                 <div className="block--elm-list">
                    <aside className="sidebar-left-collapse">
                      <div className="sidebar-links nav nav-pills flex-column">
                        <div className="nav-item selected">
                          <a href="index.html" className="nav-link">
                            Dashboard
                          </a>
                        </div>
                        <div className="nav-item">
                          <a href="SOS.html"  className="nav-link">
                            SOS Signals
                          </a>
                        </div>
                        <div className="nav-item">
                          <a href="/users"  className="nav-link">
                            Users
                          </a>
                        </div>
                        <div className="nav-item">
                          <a href="#" className="nav-link">
                            Settings
                          </a>
                          <ul className="nav nav-pills flex-column sub-links">
                                <li className="nav-item">
                                  <a className="nav-link" href="settings.html">Profile</a>
                                </li>
                                <li className="nav-item">
                                  <a className="nav-link" href="/login">Logout</a>
                                </li>
                          </ul>
                        </div>
                        <div className="nav-item">
                          <a href="/device"  className="nav-link">
                            Device List
                          </a>
                        </div>
                      </div>
                    </aside>
                 </div> 
              </div>
            </div>
          </div>
        );
    }


}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedSidebarPage = connect(mapStateToProps)(SidebarPage);
export { connectedSidebarPage as SidebarPage };