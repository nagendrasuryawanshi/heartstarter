import React from "react";
import { connect } from "react-redux";
import { deviceActions } from "../_actions";
import Select from 'react-select';
const selectedStatusFilter = [];

class DevicePage extends React.Component {

  constructor() {
    super();
    this.state = {
      selectedDeviceType: null,
      selectedStatus: null,
      selectedUpdated: null,
      selectedNumber: null,
      selectedUsername: null,
      searchByStatus:[],
      searchByStatus1:false,
      search:''
    }
    this.updateInput = this.updateInput.bind(this);
  }


  handleChange(selectedDeviceType) {

    this.setState({ selectedDeviceType })
    console.log('Device Type selected:', selectedDeviceType);
    const searchByDevice =  this.props.devices;
    const source = searchByDevice.items;
    console.log("source",source)
    source.map((item)=>{
         if(item.deviceType && (item.deviceType.deviceType === selectedDeviceType.label)){
          selectedStatusFilter.push(item)
         }
    })
    //console.log("%csearchByStatus",'color:pink',selectedStatusFilter)
    console.log("%csearchByStatus",'color:pink',selectedStatusFilter)
      this.setState({
        searchByStatus:selectedStatusFilter,
        searchByStatus1:true
      })
       if(selectedStatusFilter.length == 0 ){
        this.setState({
          searchByStatus:source,
          searchByStatus1:true
        })
      }
  }


  handleChange3(selectedNumber) {
    this.setState({ selectedNumber })
    console.log('Number selected:', selectedNumber);
    const searchByNumber =  this.props.devices;
    const source = searchByNumber.items;
    console.log("source",source)
    source.map((item)=>{
         if(item.phoneNumber === selectedNumber.value){
          selectedStatusFilter.push(item)
         }
    })
    //console.log("%csearchByStatus",'color:pink',selectedStatusFilter)
    console.log("%csearchByStatus",'color:pink',selectedStatusFilter)
      this.setState({
        searchByStatus:selectedStatusFilter,
        searchByStatus1:true
      })
      if(selectedStatusFilter.length == 0 ){
        this.setState({
          searchByStatus:source,
          searchByStatus1:true
        })
      }
  }


  handleChange2(selectedUsername) {
    this.setState({ selectedUsername })
    console.log('Username selected:', selectedUsername);
    const searchByUsername =  this.props.devices;
    const source = searchByUsername.items;
    console.log("source",source)
    source.map((item)=>{
         if(item.user && (item.user.firstName+' '+item.user.lastName === selectedUsername.label)){
          selectedStatusFilter.push(item)
         }
    })
    //console.log("%csearchByStatus",'color:pink',selectedStatusFilter)
    console.log("%csearchByStatus",'color:pink',selectedStatusFilter)
      this.setState({
        searchByStatus:selectedStatusFilter,
        searchByStatus1:true
      })
      if(selectedStatusFilter.length == 0 ){
        this.setState({
          searchByStatus:source,
          searchByStatus1:true
        })
      }
  }

  componentDidMount() {
    this.props.dispatch(deviceActions.getAllDevice());
    this.props.dispatch(deviceActions.getAllDeviceList());
  }

  updateInput(event){
    const  selectedStatusFilter = [];
    const searchItem = event.target.value;
    console.log("searchFilter",event.target.value)
    const searchByUsername =  this.props.devices;
    const source = searchByUsername.items;
    console.log("%cINPUT SEARCH IN ALL SOURCE","color:orange",source)
    source.map((item)=>{
         if(item.phoneNumber === searchItem || item.deviceType && item.deviceType.deviceType === searchItem){
          selectedStatusFilter.push(item)
         }
    })
    console.log("%csearchByStatus",'color:blue',selectedStatusFilter)
    this.setState({
      searchByStatus:selectedStatusFilter,
      searchByStatus1:true
    })
    if(selectedStatusFilter.length==0 ){
      this.setState({
        searchByStatus:source,
        searchByStatus1:true
      })
    }
  }


  render() {
    const deviceType = [];
    const userName = [];
    const numberList = [];
    const { selectedDeviceType, selectedUsername, selectedNumber ,searchByStatus1,searchByStatus} = this.state;
    const { devices, device_list } = this.props;


    if (device_list && device_list.items && device_list.items.DeviceType) {
      device_list.items.DeviceType.map((item, index) => {
        deviceType.push({ value: item._id, label: item.deviceType })
      })
    }

    if (devices && devices.items) {
      let uniqIds = {}, source = devices.items;
      let filtered = source.filter(obj => !uniqIds[obj.user && obj.user.firstName + +obj.user.lastName ] && (uniqIds[obj.user && obj.user.firstName + +obj.user.lastName ] = true));
      let filteredNumber = source.filter(obj => !uniqIds[obj.phoneNumber] && (uniqIds[obj.phoneNumber] = true));
      //console.log("filtered",filtered);
     // console.log("filteredDate",filteredNumber);
      filtered.map((item, index) => {
        userName.push({ value: item.user && item.user?item.user.firstName+' '+item.user.lastName:'Not Available', label: item.user && item.user?item.user.firstName+' '+item.user.lastName:'Not Available' })
      })
      filteredNumber.map((item, index) => {
        numberList.push({ value: item.phoneNumber, label: item.phoneNumber })
      })
    
    }
    console.log("getall devices", device_list)
    return (
      <div>
        <div className="loader-fixed" id="loading">
          <div className="loader-centered">
            <div className="loader-inner-centered emphasize-dark">
              <div className="loader">
                <div className="bg-loading-img">
                </div>
              </div>
              <p className="loading-title">Please Wait...</p>
            </div>
          </div>
        </div>
        {/* RightPanel */}


        {/* LeftSideBar */}
        <div className="sidebar" id="sidebar">
          <div className="block-sidebar">
            <div className="block--elm">
              {/* onClick={closeNav()} */}
              <span className="closebtn m-2 float-right closeUserButton">&#10005;</span>
              <div className="block--elm-header">
                <img src="../src/assets/img/logo.png" />
              </div>
            </div>
            <div className="block--elm-list">
              <aside className="sidebar-left-collapse">
                <div className="sidebar-links nav nav-pills flex-column">
                  <div className="nav-item">
                    <a href="/" className="nav-link">
                      Deshboard
                                    </a>
                  </div>
                  <div className="nav-item">
                    <a href="/sos" className="nav-link">
                      SOS Signals
                                    </a>
                  </div>
                  <div className="nav-item">
                    <a href="/users" className="nav-link">
                      Users
                                    </a>
                  </div>
                  <div className="nav-item">
                    <a href="/setting" className="nav-link">
                      Settings
                                    </a>
                  </div>
                  <div className="nav-item">
                    <a href="#" className="nav-link">
                      Profile
                                    </a>
                    <ul className="nav nav-pills flex-column sub-links">
                      <li className="nav-item">
                        <a className="nav-link" href="/setting">Personal Info</a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link" href="/login">Logout</a>
                      </li>
                    </ul>
                  </div>
                  <div className="nav-item selected">
                    <a href="/device" className="nav-link">
                      Device List
                                        </a>
                  </div>
                </div>
              </aside>
            </div>
          </div>
        </div>
        {/* LeftSideBar Close */}
        {/* PageContentStartHere */}
        <div className="content">
          <div className="block-header">
            <div className="container-fluid">
              <div className="row pageHeagingSmallScreen">
                <div className="col-12">
                  <div className="row">
                    <div className="col-6">
                      <h2 className="heading-text mr-3"><span className="openNav pointer">&#9776;</span>
                        Devices</h2>
                    </div>
                    <div className="col-6 text-right">
                      <input type="search" placeholder="Search" className="search_input bg_search" />
                    </div>
                  </div>
                  <div className="row mt-3">
                    <div className="col-12 text-right">
                      {/* <button type="button" className="btn btn-primary mr-1">Add New Entry <i className="fa fa-plus"></i> </button> */}
                      <button type="button" className="btn btn-outline-primary mr-1">Print <i className="fa fa-print ml-3"></i></button>
                      <button type="button" className="btn btn-outline-primary">Export <i className="fa fa-download ml-3" aria-hidden="true"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row pageHeagingHideSmallScreen">
                <div className="col-sm-8">
                  <div className="block-inline">
                    <div className="block-inline_elm">
                      <h2 className="heading-text mr-3">Device</h2>
                    </div>
                    {/* <div className="block-inline_elm">
                <button type="button" className="btn btn-primary mr-3">Add New User <i className="fa fa-plus"></i> </button>
              </div> */}
                    <div className="block-inline_elm">
                      <input type="text" placeholder="Search" name='search' onChange={this.updateInput}  className="search_input bg_search" />
                    </div>
                  </div>
                </div>
                <div className="col-sm-4 text-right">
                  <div className="block-inline">
                    <div className="block-inline_elm float-right">
                      <button type="button" className="btn btn-outline-primary mr-3">Print <i className="fa fa-print ml-3"></i></button>
                      <button type="button" className="btn btn-outline-primary">Export <i className="fa fa-download ml-3" aria-hidden="true"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="block-content">
            <div className="block-activity">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-sm-7">
                    <div className="">
                      <div className="d-select">
                        <div className="form-group multiselect_dropdown mb-0 mb-xs-2">
                          {/* <select id="ms" multiple="multiple" className="hidden">
                      <option value="1">Atlantic Drone</option>
                      <option value="2">Drone NL</option>
                      <option value="3">Drone NL</option>
                      <option value="4">Fighter Drones</option>
                      <option value="5">Gregs´s Drones</option>
                      <option value="6">Wahuuu SOS Signals</option>
                      <option value="1">Atlantic Drone</option>
                      <option value="2">Drone NL</option>
                      <option value="3">Drone NL</option>
                      <option value="4">Fighter Drones</option>
                      <option value="5">Gregs´s Drones</option>
                      <option value="6">Wahuuu SOS Signals</option>
                    </select> */}
                          <Select
                            //isMulti={true}
                            placeholder="DeviceType"
                            value={selectedDeviceType}
                            onChange={this.handleChange.bind(this)}
                            options={deviceType}
                          />
                        </div>
                      </div>
                      <div className="d-select">
                        <div className="form-group multiselect_dropdown mb-0 mb-xs-2">
                          {/* <select id="ms1" multiple="multiple" className="hidden">
                            <option value="1">Atlantic Drone</option>
                            <option value="2">Drone NL</option>
                            <option value="3">Drone NL</option>
                            <option value="4">Fighter Drones</option>
                            <option value="5">Gregs´s Drones</option>
                            <option value="6">Wahuuu SOS Signals</option>
                          </select> */}
                           <Select
                            //isMulti={true}
                            placeholder="UserName"
                            value={selectedUsername}
                            onChange={this.handleChange2.bind(this)}
                            options={userName}
                          />
                        </div>
                      </div>
                      <div className="d-select">
                        <div className="form-group multiselect_dropdown mb-0 mb-xs-2">
                          {/* <select id="ms2" multiple="multiple" className="hidden">
                            <option value="1">Atlantic Drone</option>
                            <option value="2">Drone NL</option>
                            <option value="3">Drone NL</option>
                            <option value="4">Fighter Drones</option>
                            <option value="5">Gregs´s Drones</option>
                            <option value="6">Wahuuu SOS Signals</option>
                          </select> */}
                           <Select
                            //isMulti={true}
                            value={selectedNumber}
                            onChange={this.handleChange3.bind(this)}
                            options={numberList}
                          />
                        </div>
                      </div>
                      {/* <div className="d-select">
                  <div className="form-group multiselect_dropdown mb-0 mb-xs-2">
                    <select id="ms3" multiple="multiple" className="hidden">
                      <option value="1">Atlantic Drone</option>
                      <option value="2">Drone NL</option>
                      <option value="3">Drone NL</option>
                      <option value="4">Fighter Drones</option>
                      <option value="5">Gregs´s Drones</option>
                      <option value="6">Wahuuu SOS Signals</option>
                    </select>
                  </div>
                </div> */}
                    </div>
                  </div>
                  <div className="col-sm-5 text-right">
                    <p className="normal-text inline mr-3 mb-0">Viewing<strong> 1-20 </strong> of <strong>36</strong></p>
                    <div className="btn-group tbl-next" role="group">
                      <button type="button" className="btn btn-outline-secondary">
                        <img src="../src/assets/img/tbl_next_left.png" className="tbl_next" />
                      </button>
                      <button type="button" className="btn btn-outline-secondary">
                        <img src="../src/assets/img/tbl_next_right.png" className="tbl_next" />
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="block-list">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-sm-12">
                    <div className="tableresponsive">
                      <table className="table table-borderless table-striped mb-0 tr56" id="flightTable" cellSpacing="0"
                        cellPadding="0">
                        <thead>
                          <tr>
                            <th>Device Type</th>
                            {/* <th>Latitude</th>
                      <th>Longitude</th> */}
                            <th>User Name</th>
                            <th>Phone Number</th>
                            <th>Notes</th>
                          </tr>
                        </thead>

                        <tbody>
                          {searchByStatus1==true?
                           searchByStatus.map((device, index) => (
                            <tr key={index}>
                              <td>{device.deviceType && device.deviceType ? device.deviceType.deviceType : 'Not Available'}</td>
                              <td>{device.user && device.user ? device.user.firstName + " " + device.user.lastName : 'Not Available'}</td>
                              <td>{device.phoneNumber}</td>
                              <td>{device.locationNote}</td>
                            </tr>
                             ))
                          
                            :
                            devices.items && devices.items.map((device, index) => (
                            <tr key={index}>
                              <td>{device.deviceType && device.deviceType ? device.deviceType.deviceType : 'Not Available'}</td>
                              {/* <td>{device.latitude}</td>
                      <td>{device.longitude}</td> */}
                              <td>{device.user && device.user ? device.user.firstName + " " + device.user.lastName : 'Not Available'}</td>
                              <td>{device.phoneNumber}</td>
                              <td>{device.locationNote}</td>
                            </tr>
                          ))}
                        </tbody>

                      </table>
                      <div className="clearfix"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* EndPageContentStartHere */}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { devices, device_list, authentication } = state;
  // const { user } = authentication;
  return {
    // user,
    devices,
    device_list
  };
}

const connectedDevicePage = connect(mapStateToProps)(DevicePage);
export { connectedDevicePage as DevicePage };