import React from "react";
import { connect } from "react-redux";
//import { forgotActions } from "../_actions";

class ForgotPasswordPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username } = this.state;
        const { dispatch } = this.props;
        if (username) {
           /// dispatch(forgotActions.login(username));
        }
    }

    render() {
        const { username, submitted } = this.state;
        return (
            <div className="container h-100">
                <div className="row h-100 justify-content-center align-items-center">
                    <form className="login_view" onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <h2>Recover Password <br /></h2>
                        </div>
                        <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
                            <label htmlFor="formGroupExampleInput">Username</label>
                            <input type="text" className="form-control" id="formGroupExampleInput" placeholder="User name" name="username" value={username} onChange={this.handleChange}/>
                            { submitted && !username &&
                            <div className="help-block">Username is required</div>
                           }
                        </div>
                        <div className="form-group clearfix">
                            <button type="submit" className="btn btn-primary btn-block float-left mt-3 mb-3">
                                <span className="float-left">
                                    Reset Password
                                </span>
                                <span className="float-right">
                                    <i className="fa fa-caret-right ml-4"></i>
                                </span>
                            </button>
                        </div>
                        <div className="form-group mb-0 text-center">
                            <a href="/login" className="back_titile">Login now</a>
                            <a href="javascript:void(0)" className="back_titile_seprator">|</a>
                            <a href="/register" className="back_titile">sign up now</a>
                        </div>
                    </form>
                </div>
            </div> 
          );
    }

}

function mapStateToProps(state) {
    const { devices, authentication } = state;
    // const { user } = authentication;
    return {
        // user,
        devices
    };
}

const connectedForgotPasswordPage = connect(mapStateToProps)(ForgotPasswordPage);
export { connectedForgotPasswordPage as ForgotPasswordPage };