import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { userActions } from "../_actions";
import Select from 'react-select';



class UsersPage extends React.Component {
  constructor() {
    super();
    this.state = {
      selectedOption: null,
      selectedStatus: null,
      selectedUpdated:null,
      searchByStatus:[],
      searchByStatus1:false,
      search:''
    }
    this.updateInput = this.updateInput.bind(this);
  }


  handleChange2(selectedStatus) {
    const  selectedStatusFilter = [];
    this.setState({ selectedStatus })
    console.log('Status selected:', selectedStatus.value);
    const searchByStatus = this.props.users;
    const source = searchByStatus.items.result;
  //  console.log("source",source)
    source.map((item)=>{
       if(item.verified===selectedStatus.value){
       
        selectedStatusFilter.push(item)
       }
    })
    //console.log("%csearchByStatus",'color:pink',selectedStatusFilter)
    //console.log("%csearchByStatus",'color:pink',search_data)
      this.setState({
        searchByStatus:selectedStatusFilter,
        searchByStatus1:true
      })
      // console.log("%csearchByStatus",'color:pink',source)
  
    
  }
  handleChange3(selectedUpdated) {
    const  sselectedUpdatedFilter = [];
    this.setState({ selectedUpdated })
    console.log('updated selected:', selectedUpdated);
    const searchByUpdate =  this.props.users;
    const source = searchByUpdate.items.result;
    console.log("source",source)
    source.map((item)=>{
         if(this.dateData(item.updatedAt) === selectedUpdated.label){
          sselectedUpdatedFilter.push(item)
         }
      
    })
    //console.log("%csearchByStatus",'color:pink',selectedStatusFilter)
    console.log("%csearchByStatus",'color:pink',sselectedUpdatedFilter)
      this.setState({
        searchByStatus:sselectedUpdatedFilter,
        searchByStatus1:true
      })
  }

  handleChange(selectedOption) {
    const  selectedStatusFilter = [];
    this.setState({ selectedOption })
    //console.log('Option selected:', selectedOption);
    const searchByRole =  this.props.users;
    const source = searchByRole.items.result;
    //console.log("source",source)
    source.map((item)=>{
        item.roles && item.roles.map(rol=>{
        if(rol._id === selectedOption.value){
          selectedStatusFilter.push(item)
         }
      })
      
    })
    //console.log("%csearchByStatus",'color:pink',selectedStatusFilter)
    console.log("%csearchByStatus",'color:pink',selectedStatusFilter)
      this.setState({
        searchByStatus:selectedStatusFilter,
        searchByStatus1:true
      })
  }

  componentDidMount() {
    this.props.dispatch(userActions.getAll());
    this.props.dispatch(userActions.getRole());
   
  }

  handleDeleteUser(status, id) {
    let data = {
      verified: status,
    }
    //console.log("USERS_REQUEST", data, id)
    return e => this.props.dispatch(userActions.update(data, id));
  }

  // userRole(role) {
  //   console.log("roleofuser ouuter",role)
  //   var roleofuser = [];
  //   const all_role = this.props.roles.items;
  //   console.log("all_role",all_role)
  //   role && role.map(function (item, index) {
  //     console.log("item",item)
  //     all_role && all_role.Roles && all_role.Roles.map(function (item1, index1) {
  //       console.log("item1",item1._id)
       
  //       if (item1._id === item) {
  //         console.log("item2",item1)
  //         console.log("roleofuser", roleofuser.push(item1.role))
  //        ;
  //       }
  //     })
  //   })
   
  //   return roleofuser.map(value => <tr>{value}</tr>)
  // }

  dateData(date) {
    // console.log('date',date)
    const price = typeof date === "string" ? date.split('T')[0] : ""
    return price;
  }

  updateInput(event){
    const  selectedStatusFilter = [];
    const searchItem = event.target.value;
    console.log("searchFilter",event.target.value)
    const searchByRole =  this.props.users;
    const source = searchByRole.items.result;
    console.log("source",source)
    source.map((item)=>{
         if(item.firstName === searchItem || item.lastName === searchItem || item.phoneNumber === searchItem){
          selectedStatusFilter.push(item)
         }
    })
    console.log("%csearchByStatus",'color:blue',selectedStatusFilter)
    this.setState({
      searchByStatus:selectedStatusFilter,
      searchByStatus1:true
    })
    if(selectedStatusFilter.length==0 ){
      this.setState({
        searchByStatus:source,
        searchByStatus1:true
      })
    }
  }

  render() {
    const options = [];
    const status = [];
    const dateWise = [];

    const { user, users, roles } = this.props;
    const { selectedOption ,selectedStatus,selectedUpdated,searchByStatus,searchByStatus1 ,search} = this.state;
    if (roles && roles.items && roles.items.Roles) {
      roles.items.Roles.map((item, index) => {
        options.push({ value: item._id, label: item.role })
      })
    }

    if (users && users.items && users.items.result) {
      let uniqIds = {}, source = users.items.result;
      let filtered = source.filter(obj => !uniqIds[obj.verified] && (uniqIds[obj.verified] = true));
      let filteredDate = source.filter(obj => !uniqIds[this.dateData(obj.updatedAt)] && (uniqIds[this.dateData(obj.updatedAt)] = true));
      //console.log("%cfiltered",'color: green',filtered);
      //console.log('%cfilteredDate','color: deeppink',filteredDate);
      filtered.map((item, index) => {
        status.push({ value: item.verified, label: item.verified == true ? 'Verified' : 'Not Verfied' })
      })
      filteredDate.map((item, index) => {
         dateWise.push({ value: item.updatedAt, label: this.dateData(item.updatedAt) })
      })
    }
 

    return (
     
      <div>
        <div className="loader-fixed" id="loading">
          <div className="loader-centered">
            <div className="loader-inner-centered emphasize-dark">
              <div className="loader">
                <div className="bg-loading-img">
                </div>
              </div>
              <p className="loading-title">Please Wait...</p>
            </div>
          </div>
        </div>
        {/* RightPanel */}


        {/* LeftSideBar */}
        <div className="sidebar" id="sidebar">
          <div className="block-sidebar">
            <div className="block--elm">
              {/* onclick="closeNav()" */}
              <span className="closebtn m-2 float-right closeUserButton" >&#10005;</span>
              <div className="block--elm-header">
                <img src="../src/assets/img/logo.png" />
              </div>
            </div>
            <div className="block--elm-list">
              <aside className="sidebar-left-collapse">
                <div className="sidebar-links nav nav-pills flex-column">
                  <div className="nav-item">
                    <a href="/" className="nav-link">
                      Deshboard
                                    </a>
                  </div>
                  <div className="nav-item">
                    <a href="/sos" className="nav-link">
                      SOS Signals
                                    </a>
                  </div>
                  <div className="nav-item selected">
                    <a href="/users" className="nav-link">
                      Users
                                    </a>
                  </div>
                  <div className="nav-item">
                    <a href="/setting" className="nav-link">
                      Settings
                                    </a>
                  </div>
                  <div className="nav-item">
                    <a href="#" className="nav-link">
                      Profile
                                    </a>
                    <ul className="nav nav-pills flex-column sub-links">
                      <li className="nav-item">
                        <a className="nav-link" href="/setting">Personal Info</a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link" href="/login">Logout</a>
                      </li>
                    </ul>
                  </div>
                  <div className="nav-item">
                    <a href="/device" className="nav-link">
                      Device List
                                        </a>
                  </div>
                </div>
              </aside>
            </div>
          </div>
        </div>
        {/* LeftSideBar Close */}
        {/* PageContentStartHere */}
        <div className="content">
          <div className="block-header">
            <div className="container-fluid">
              <div className="row pageHeagingSmallScreen">
                <div className="col-12">
                  <div className="row">
                    <div className="col-6">
                      <h2 className="heading-text mr-3"><span className="openNav pointer">&#9776;</span>
                        Users</h2>
                    </div>
                    <div className="col-6 text-right">
                      <input type="search" placeholder="Search" className="search_input bg_search" />
                    </div>
                  </div>
                  <div className="row mt-3">
                    <div className="col-12 text-right">
                      <button type="button" className="btn btn-primary mr-1">Add New Entry <i className="fa fa-plus"></i> </button>
                      <button type="button" className="btn btn-outline-primary mr-1">Print <i className="fa fa-print ml-3"></i></button>
                      <button type="button" className="btn btn-outline-primary">Export <i className="fa fa-download ml-3" aria-hidden="true"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row pageHeagingHideSmallScreen">
                <div className="col-sm-8">
                  <div className="block-inline">
                    <div className="block-inline_elm">
                      <h2 className="heading-text mr-3">Users</h2>
                    </div>
                    <div className="block-inline_elm">
                      <button type="button" className="btn btn-primary mr-3">Add New User <i className="fa fa-plus"></i> </button>
                    </div>
                    <div className="block-inline_elm">
                      <input type="text" placeholder="Search" name='search' onChange={this.updateInput}   className="search_input bg_search" />
                    </div>
                  </div>
                </div>
                <div className="col-sm-4 text-right">
                  <div className="block-inline">
                    <div className="block-inline_elm float-right">
                      <button type="button" className="btn btn-outline-primary mr-3">Print <i className="fa fa-print ml-3"></i></button>
                      <button type="button" className="btn btn-outline-primary">Export <i className="fa fa-download ml-3" aria-hidden="true"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="block-content">
            <div className="block-activity">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-sm-7">
                    <div className="">
                      <div className="d-select">
                        <div className="form-group multiselect_dropdown mb-0 mb-xs-2">

                          {/* <select id="ms" multiple="multiple"  className="hidden">
                             {roles && roles.items && roles.items.Roles && roles.items.Roles.map((itemValue, index)=> {
                               return(
                               <option key={index} value={itemValue._id}>{itemValue.role}</option>
                               )
                               })}
                           </select> */}

                        <Select
                            //isMulti={true}
                            placeholder="Roles"
                            value={selectedOption}
                            onChange={this.handleChange.bind(this)}
                            options={options}
                          /> 

                        </div>
                      </div>
                      <div className="d-select">
                        <div className="form-group multiselect_dropdown mb-0 mb-xs-2">
                        {/* <select id="ms1"  multiple='multiple' options={options}>                        */}
                        {/* {options.map((user, index) => (     
                         <option value={index}>{user.firstName}</option>
                        ))} */}
                        {/* </select> */}
                        
                      
                        

                          <Select
                            //isMulti={true}
                            placeholder="Status"
                            value={selectedStatus}
                            onChange={this.handleChange2.bind(this)}
                            options={status}
                          />
                        </div>
                      </div>
                      <div className="d-select">
                        <div className="form-group multiselect_dropdown mb-0 mb-xs-2">
                          {/* <select id="ms2" multiple="multiple" className="hidden">
                            {users.items && users.items.result.map((user, index) => (
                              <option key={index} value={user.updatedAt}>{user.updatedAt}</option>
                            ))}
                          </select> */}
                           <Select
                            //isMulti={true}
                            placeholder="Updated"
                            value={selectedUpdated}
                            onChange={this.handleChange3.bind(this)}
                            options={dateWise}
                          />
                        </div>
                      </div>
                      {/* <div className="d-select">
                  <div className="form-group multiselect_dropdown mb-0 mb-xs-2">
                    <select id="ms3" multiple="multiple" className="hidden">
                      <option value="1">Atlantic Drone</option>
                      <option value="2">Drone NL</option>
                      <option value="3">Drone NL</option>
                      <option value="4">Fighter Drones</option>
                      <option value="5">Gregs´s Drones</option>
                      <option value="6">Wahuuu SOS Signals</option>
                    </select>
                  </div>
                </div> */}
                    </div>
                  </div>
                  <div className="col-sm-5 text-right">
                    <p className="normal-text inline mr-3 mb-0">Viewing<strong> 1-20 </strong> of <strong>36</strong></p>
                    <div className="btn-group tbl-next" role="group">
                      <button type="button" className="btn btn-outline-secondary">
                        <img src="../src/assets/img/tbl_next_left.png" className="tbl_next" />
                      </button>
                      <button type="button" className="btn btn-outline-secondary">
                        <img src="../src/assets/img/tbl_next_right.png" className="tbl_next" />
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="block-list">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-sm-12">
                    <div className="tableresponsive">
                      <table className="table table-borderless table-striped mb-0 tr56" id="flightTable" cellSpacing="0"
                        cellPadding="0">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Role</th>
                            <th>Phone Number</th>
                            <th>Last Updated</th>
                            <th>Status</th>
                            <th></th>
                          </tr>
                        </thead>

                        <tbody>
                          {searchByStatus1==true?
                            searchByStatus.map((user, index) => (
                              <tr key={index}>
                                <td>{user.firstName + " " + user.lastName}</td>
                                <td>{user.roles && user.roles.map(rol=><tr>{rol.role}</tr>)}</td>
                                <td>{user.phoneNumber}</td>
                                <td>{this.dateData(user.updatedAt)}</td>
                                <td>{user.verified == true ? 'Verfied' : 'Not Verified'}</td>
                                <td className="dropdown tbl-dropdown p-0">
                                  <button className="btn btn-link dropdown-toggle area72" type="button" id="about-us" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <i className="fa fa-ellipsis-v" aria-hidden="true"></i>
                                  </button>
                                  <div className="dropdown-menu dropdown-menu-right" aria-labelledby="about-us">
                                    <h6 className="dropdown-header">Action</h6>
                                    <a className="dropdown-item" href="#"><img src="../src/assets/img/edit.png" className="mr-2" /> Edit</a>
                                    <a className="dropdown-item" href="#"><img src="../src/assets/img/delete.png" className="mr-2" /> Delete</a>
                                    <a className="dropdown-item" href="#"><img src="../src/assets/img/report.png" className="mr-2" /> Report</a>
                                  </div>
                                </td>
                              </tr>
                            ))
                          
                          
                          :
                          users.items && users.items.result.map((user, index) => (
                            <tr key={index}>
                              <td>{user.firstName + " " + user.lastName}</td>
                              <td>{user.roles && user.roles.map(rol=><tr>{rol.role}</tr>)}</td>
                              <td>{user.phoneNumber}</td>
                              <td>{this.dateData(user.updatedAt)}</td>
                              <td>{user.verified == true ? 'Verfied' : 'Not Verified'}</td>
                              <td className="dropdown tbl-dropdown p-0">
                                <button className="btn btn-link dropdown-toggle area72" type="button" id="about-us" data-toggle="dropdown"
                                  aria-haspopup="true" aria-expanded="false">
                                  <i className="fa fa-ellipsis-v" aria-hidden="true"></i>
                                </button>
                                <div className="dropdown-menu dropdown-menu-right" aria-labelledby="about-us">
                                  <h6 className="dropdown-header">Action</h6>
                                  <a className="dropdown-item" href="#"><img src="../src/assets/img/edit.png" className="mr-2" /> Edit</a>
                                  <a className="dropdown-item" href="#"><img src="../src/assets/img/delete.png" className="mr-2" /> Delete</a>
                                  <a className="dropdown-item" href="#"><img src="../src/assets/img/report.png" className="mr-2" /> Report</a>
                                </div>
                              </td>
                            </tr>
                          ))
                          }
                        </tbody>

                      </table>
                      <div className="clearfix"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* EndPageContentStartHere */}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { users, authentication, roles } = state;
  const { user } = authentication;
  return {
    user,
    users,
    roles
  };
}

const connectedUsersPage = connect(mapStateToProps)(UsersPage);
export { connectedUsersPage as UsersPage };