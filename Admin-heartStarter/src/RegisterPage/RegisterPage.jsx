import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../_actions';

class RegisterPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {
                firstName: '',
                lastName: '',
                username: '',
                password: '',
                confirmPassword:'',
                phone:'',
                companyName:'',
                email:''

            },
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({ submitted: true });
        const { user } = this.state;
        const { dispatch } = this.props;
        if (user.firstName && 
            user.lastName && 
            user.username && 
            user.password && 
            user.phone && 
            user.confirmPassword && 
            user.email && 
            user.companyName) {
            dispatch(userActions.register(user));
        }
    }

    render() {
        const { registering  } = this.props;
        const { user, submitted } = this.state;
        return (
            <body class="body"> 
            <div class="container h-100">
              <div class="row h-100 justify-content-center align-items-center">
                <form class="login_view" onSubmit={this.handleSubmit}>
                  <div class="form-group">
                    <h2>Sing Up</h2>
                  </div>
                  <h6 class="mb-2">Login Info</h6>
                  <div class={'form-group' + (submitted && !user.UserName ? ' has-error' : '')}>
                    <label htmlFor="formGroupExampleInput">Username</label>
                    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="User Name" name="username" value={user.username} onChange={this.handleChange}/>
                    {submitted && !user.UserName &&
                       <div className="help-block">User Name is required</div>
                    }
                  </div>
                  <div class={'form-group' + (submitted && !user.firstName ? ' has-error' : '')}>
                    <label htmlFor="formGroupExampleInput">First name</label>
                    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="First Name" name="firstName" value={user.firstName} onChange={this.handleChange}/>
                    {submitted && !user.firstName &&
                       <div className="help-block">First Name is required</div>
                    }
                  </div>
                  <div class={'form-group' + (submitted && !user.lastName ? ' has-error' : '')}>
                    <label htmlFor="formGroupExampleInput">Last Name</label>
                    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Last Name" name="lastName" value={user.lastName} onChange={this.handleChange}/>
                    {submitted && !user.lastName &&
                       <div className="help-block"> Last Name is required</div>
                    }
                  </div>
                  <div class={'form-group' + (submitted && !user.password ? ' has-error' : '')}>
                    <label htmlFor="formGroupExampleInput2">Password</label>
                    <input type="password" class="form-control" id="formGroupExampleInput2" placeholder="password" name="password" value={user.password} onChange={this.handleChange}/>
                    {submitted && !user.password &&
                       <div className="help-block"> Password is required</div>
                    }
                  </div>
                  <div class={'form-group' + (submitted && !user.confirmPassword ? ' has-error' : '')}>
                    <label htmlFor="formGroupExampleInput2">Confirm Password</label>
                    <input type="password" class="form-control" id="formGroupExampleInput2" placeholder="Re-Enter Password" name="confirmPassword" value={user.confirmPassword} onChange={this.handleChange}/>
                    {submitted && !user.confirmPassword &&
                       <div className="help-block"> Confirm Password is required</div>
                    }
                  </div>
                  <h6 class="mb-2">Company Info</h6>
                   <div class={'form-group' + (submitted && !user.companyName ? ' has-error' : '')}>
                    <label htmlFor="formGroupExampleInput">Company Name</label>
                    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Company Name" name="companyName" value={user.companyName} onChange={this.handleChange}/>
                    {submitted && !user.lastName &&
                       <div className="help-block"> Company Name is required</div>
                    }
                  </div>
                  <div class={'form-group' + (submitted && !user.email ? ' has-error' : '')}>
                    <label htmlFor="formGroupExampleInput">Email</label>
                    <input type="email" class="form-control" id="formGroupExampleInput" placeholder="Email" name="email" value={user.email} onChange={this.handleChange}/>
                    {submitted && !user.email &&
                       <div className="help-block"> Email is required</div>
                    }
                  </div>
                  <div class={'form-group' + (submitted && !user.phone ? ' has-error' : '')}>
                    <label htmlFor="formGroupExampleInput">Phone</label>
                    <input type="tel" class="form-control" id="formGroupExampleInput" placeholder="Phone" name="phone" value={user.phone} onChange={this.handleChange}/>
                    {submitted && !user.lastName &&
                       <div className="help-block"> Phone Number is required</div>
                    }
                  </div>
                 <div class="form-group clearfix">
                        <button type="submit" class="btn btn-primary float-right mt-3 mb-3">
                        <span class="float-left">
                          Submit
                        </span>
                        <span class="float-right">
                        <i class="fa fa-caret-right ml-4"></i>
                         </span> 
                      </button>
                 </div>
                 <div class="form-group mb-0 text-center">
                    <a href="/login" class="back_titile">Login now</a>
                    <a href="javascript:void(0)" class="back_titile_seprator">|</a>
                    <a href="/forgot" class="back_titile">Forgot Password</a>
                 </div>
                </form>   
              </div>
            </div> 
      </body>
        );
    }
}

function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedRegisterPage = connect(mapStateToProps)(RegisterPage);
export { connectedRegisterPage as RegisterPage };

 // <div className="col-md-6 col-md-offset-3">
            //     <h2>Register</h2>
            //     <form name="form" onSubmit={this.handleSubmit}>
            //         <div className={'form-group' + (submitted && !user.firstName ? ' has-error' : '')}>
            //             <label htmlFor="firstName">First Name</label>
            //             <input type="text" className="form-control" name="firstName" value={user.firstName} onChange={this.handleChange} />
            //             {submitted && !user.firstName &&
            //                 <div className="help-block">First Name is required</div>
            //             }
            //         </div>
            //         <div className={'form-group' + (submitted && !user.lastName ? ' has-error' : '')}>
            //             <label htmlFor="lastName">Last Name</label>
            //             <input type="text" className="form-control" name="lastName" value={user.lastName} onChange={this.handleChange} />
            //             {submitted && !user.lastName &&
            //                 <div className="help-block">Last Name is required</div>
            //             }
            //         </div>
            //         <div className={'form-group' + (submitted && !user.username ? ' has-error' : '')}>
            //             <label htmlFor="username">Username</label>
            //             <input type="text" className="form-control" name="username" value={user.username} onChange={this.handleChange} />
            //             {submitted && !user.username &&
            //                 <div className="help-block">Username is required</div>
            //             }
            //         </div>
            //         <div className={'form-group' + (submitted && !user.password ? ' has-error' : '')}>
            //             <label htmlFor="password">Password</label>
            //             <input type="password" className="form-control" name="password" value={user.password} onChange={this.handleChange} />
            //             {submitted && !user.password &&
            //                 <div className="help-block">Password is required</div>
            //             }
            //         </div>
            //         <div className="form-group">
            //             <button className="btn btn-primary">Register</button>
            //             {registering && 
            //                 <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
            //             }
            //             <Link to="/login" className="btn btn-link">Cancel</Link>
            //         </div>
            //     </form>
            // </div>