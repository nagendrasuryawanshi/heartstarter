import config from "../config";
const Url = config.key.HS_API_URL;

export const deviceService = {
  getAllDevice,
  getAllDeviceList
};


// All device
function getAllDevice() {
  const requestOptions = {
    method: "GET",
  };
  return fetch(Url + `device/getAll`, requestOptions).then(handleResponse);
}

// device list 
function getAllDeviceList() {
  const requestOptions = {
    method: "GET",
  };
  return fetch(Url + `deviceType/getAll`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
        // auto logout if 401 response returned from api
        logout();
        location.reload(true);
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}

//End