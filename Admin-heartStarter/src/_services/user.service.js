import { authHeader } from "../_helpers";

export const userService = {
  login,
  logout,
  register,
  getAll,
  getById,
  update,
  delete: _delete,
  getRole
};
import config from "../config";
const Url=config.key.HS_API_URL
function login(username, password) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ username, password })
  };
  console.log("Action start", requestOptions);
  // return fetch(`${config.apiUrl}/users/authenticate`, requestOptions)
  return fetch(Url +`admin/login`, requestOptions)
    .then(handleResponse)
    .then(user => {
      console.log("User", user.token);
      // login successful if there's a jwt token in the response
      if (user.token) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem("user", JSON.stringify(user));
        console.log("localStorage", localStorage.getItem("user"));
      }

      return user;
    });
}

function logout() {
  // remove user from local storage to log user out
  localStorage.removeItem("user");
}

function getAll() {
   const requestOptions = {
    method: "GET"  , 
    //headers: authHeader(),
    
    
  };
  return fetch(Url+`users/getAll`, requestOptions).then(handleResponse)
    .then(user => {
      //console.log("getalluserdata", user);
      // login successful if there's a jwt token in the response
  
      return user;
    });

  // return fetch(Url+`users/getAll`, requestOptions).then(handleResponse);
}

function getRole() {
  const requestOptions = {
   method: "GET"  , 
  };
  return fetch(Url+`roles/getAll`, requestOptions).then(handleResponse);
}


function getById(id) {
  const requestOptions = {
    method: "GET",
    headers: authHeader()
  };

  return fetch(`${config.apiUrl}/users/${id}`, requestOptions).then(
    handleResponse
  );
}

function register(user) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(user)
  };

  return fetch(`${config.apiUrl}/users/register`, requestOptions).then(
    handleResponse
  );
}

// function update(user) {
//   const requestOptions = {
//     method: "PUT",
//     headers: { ...authHeader(), "Content-Type": "application/json" },
//     body: JSON.stringify(user)
//   };

//   return fetch(`${config.apiUrl}/users/${user.id}`, requestOptions).then(
//     handleResponse
//   );
// }

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
  const requestOptions = {
    method: "DELETE",
    headers: authHeader()
  };

  return fetch(`${config.apiUrl}users/${id}`, requestOptions).then(
    handleResponse
  );
}

function update(data,id) {
  console.log("data,id in update method",data,id)
  console.log("data,id in update method",config.apiUrl)
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(data)
  };

  return fetch(`${Url}users/update/${id}`, requestOptions).then(
    handleResponse
  );
}

function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
        // auto logout if 401 response returned from api
        logout();
        location.reload(true);
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
