import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {  SidebarPage } from "../SidebarPage"

import { userActions } from "../_actions";

class  HomePage extends React.Component {
  componentDidMount() {
    this.props.dispatch(userActions.getAll());
  }

  // handleDeleteUser(status, id) {
  //   let data = {
  //     verified: status,
  //   }
  //   console.log("USERS_REQUEST", data, id)
  //   return e => this.props.dispatch(userActions.update(data, id));
  // }

  render() {
   // const { user, users } = this.props;
      return (
      <div>
        {/* <SidebarPage {...this.props} /> */}
        <div className="loader-fixed" id="loading">
      <div className="loader-centered">
        <div className="loader-inner-centered emphasize-dark">
            <div className="loader">
              <div className="bg-loading-img">
              </div>
            </div>
            <p className="loading-title">Please Wait...</p>
        </div>
      </div>
  </div>
        <div className="sidebar" id="sidebar">
      <div className="block-sidebar">
        <div className="block--elm">
           <div className="block--elm-header">
              <img src="../src/assets/img/logo.png"/>
           </div> 
           <div className="block--elm-list">
              <aside className="sidebar-left-collapse">
                <div className="sidebar-links nav nav-pills flex-column">
                  <div className="nav-item selected">
                    <a href="index.html" className="nav-link">
                      Dashboard
                    </a>
                  </div>
                  <div className="nav-item">
                    <a href="/sos"  className="nav-link">
                      SOS Signals
                    </a>
                  </div>
                  <div className="nav-item">
                    <a href="/users"  className="nav-link">
                      Users
                    </a>
                  </div>
                  <div className="nav-item">
                      <a href="/setting" className="nav-link">
                      Settings
                  </a>
                  </div>
                  <div className="nav-item">
                      <a href="#" className="nav-link">
                          Profile
                  </a>
                      <ul className="nav nav-pills flex-column sub-links">
                          <li className="nav-item">
                              <a className="nav-link" href="settings.html">Personal Info</a>
                          </li>
                          <li className="nav-item">
                              <a className="nav-link" href="/login">Logout</a>
                          </li>
                      </ul>
                  </div>
                  <div className="nav-item">
                    <a href="/device"  className="nav-link">
                      Device List
                    </a>
                  </div>
                </div>
              </aside>
           </div> 
        </div>
      </div>
    </div>
      <div className="content">
        <div className="block-header">
         <div className="container-fluid">
          <div className="row">
            <div className="col">
               <div className="block-inline"> 
                <div className="block-inline_elm">
                  <h2 className="heading-text mr-3">Dashboard</h2>
                </div>
              </div>
            </div>
          </div>
        </div> 
      </div>
    </div>
    </div>
    );
  }
}

function mapStateToProps(state) {
  const { users, authentication } = state;
  const { user } = authentication;
  return {
    user,
    users
  };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export { connectedHomePage as HomePage };


{/* <div>
         <nav className="navbar navbar-default navbar-fixed-top">
          <div className="container">
            <div className="navbar-header">
                <a className="navbar-brand" href="#myPage"><img src="../src/assets/logo.png" style={{height:'100%',width:null}}/></a>
            </div>
            <div className="collapse navbar-collapse" id="myNavbar">
              <ul className="nav navbar-nav navbar-right">
                <li><Link to="/Devices">Devices</Link></li> 
                <li><Link to="/login">Logout</Link></li>
              </ul>
            </div>
          </div>
        </nav>
        <div className="row">

          <h1>Hi {user.firstName}!</h1>
          <p>You're logged in with HeartStarter!!</p>
          <h3>All registered users:</h3>
          {users.loading && <em>Loading users...</em>}
          {users.error && (
            <span className="text-danger">ERROR: {users.error}</span>
          )}
           <table className="table table-striped table-bordered table-hover table-condensed">
            <thead>
              <tr>
                <th> Serial Number </th>
                <th> User Name </th>
                <th> Authenticate Status And Update </th>
              </tr>
            </thead>
            <tbody>
              {users.items && users.items.result.map((user, index) => (
                <tr key={index}>
                  <td> {index + 1} </td>
                  <td> {user.firstName + " " + user.lastName} </td>
                  <td> {user.verified == true ?
                    <button onClick={this.handleDeleteUser(!user.verified, user._id)} type="button" className="btn btn-primary">verified</button>
                      :
                    <button onClick={this.handleDeleteUser(!user.verified, user._id)} type="button" className="btn btn-danger">unverified</button>
                      }
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
         </div>

      </div> */}