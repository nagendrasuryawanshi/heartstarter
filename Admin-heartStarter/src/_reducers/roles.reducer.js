import { userConstants } from '../_constants';

export function roles(state = {}, action) {
  switch (action.type) {
    case userConstants.GET_ROLE_REQUEST:
      return { 
        roles: true
         };
    case userConstants.GET_ROLE_SUCCESS:
    return {
        items: action.roles
      };
    case userConstants.GET_ROLE_FAILURE:
      return {
          rror: action.error
        };
    default:
      return state
  }
}