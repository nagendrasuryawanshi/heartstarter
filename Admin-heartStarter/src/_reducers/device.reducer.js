import { userConstants } from '../_constants';

export function devices(state = {}, action) {
  switch (action.type) {
    case userConstants.GETALL_DEVICE_REQUEST:
      return { 
          devices: true
         };
    case userConstants.GETALL_DEVICE_SUCCESS:
    return {
        items: action.devices
      };
    case userConstants.GETALL_DEVICE_FAILURE:
      return {
          error: action.error
        };
    default:
      return state
  }
}

export function device_list(state = {}, action) {
  switch (action.type) {
    case userConstants.GETALL_DEVICE_LIST_REQUEST:
      return { 
        device_list: true
         };
    case userConstants.GETALL_DEVICE_LIST_SUCCESS:
    return {
        items: action.device_list
      };
    case userConstants.GETALL_DEVICE_LIST_FAILURE:
      return {
          error: action.error
        };
    default:
      return state
  }
}