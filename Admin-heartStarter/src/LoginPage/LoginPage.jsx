import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../_actions';




class LoginPage extends React.Component {
    constructor(props) {
        super(props);

        // reset login status
        this.props.dispatch(userActions.logout());

        this.state = {
            username: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username, password } = this.state;
        const { dispatch } = this.props;
        if (username && password) {
            dispatch(userActions.login(username, password));
        }
    }

    render() {
        const { loggingIn } = this.props;
        const { username, password, submitted } = this.state;
        return (
            <div class="container-fluid bodyLogin h-100">
            <div class="row h-100 justify-content-center align-items-center">
              <form class="login_view" onSubmit={this.handleSubmit}>
                <div class="form-group">
                  <h2>Login</h2>
                </div>
                <div class={'form-group' + (submitted && !username ? ' has-error' : '')}>
                  <label for="formGroupExampleInput">Username</label> 
                  <input type="text" class="form-control" id="formGroupExampleInput" placeholder="User name"  name="username" value={username} onChange={this.handleChange}/>
                     { submitted && !username &&
                        <div className="help-block">Username is required</div>
                     }
                </div>
                <div class={'form-group' + (submitted && !password ? ' has-error' : '')}>
                  <label for="formGroupExampleInput2">Password</label>
                  <input type="password" class="form-control" id="formGroupExampleInput2" placeholder="Password" name="password" value={password} onChange={this.handleChange}/>
                  {submitted && !password &&
                        <div className="help-block">Password is required</div>
                    }
                
                </div>
                <div class="form-group mb-0">
                   <div class="toggle_button toggle_rememberme">
                    <span>Remember Me?</span>
                    <div class="flat-toggle">
                        <span></span>
                        <input value="1" type="hidden"/>
                    </div>
                  </div>
                </div>  
               <div class="form-group clearfix">
                  <button type="submit" class="btn btn-primary btn-block float-left mt-3 mb-3">
                    <span class="float-left">
                      Login
                    </span>
                    <span class="float-right">
                    <i class="fa fa-caret-right ml-4"></i>
                   </span> 
                   </button>
               </div>
               <div class="form-group mb-0 text-center">
                  <a href="/forgot" class="back_titile">Forgot Password</a>
                  <a href="javascript:void(0)" class="back_titile_seprator">|</a>
                  <a href="/register" class="back_titile">sign up now</a>
               </div>
              </form>   
            </div>
          </div> 
      
        );
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage }; 

{/*<div className="col-md-6 col-md-offset-3">
 <img src="../src/assets/logo.png" /> 
  <h2>Login</h2>
  <form name="form" onSubmit={this.handleSubmit}>
      <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
          <label htmlFor="username">Username</label>
          <input type="text" className="form-control" name="username" value={username} onChange={this.handleChange} />
          {submitted && !username &&
              <div className="help-block">Username is required</div>
          }
      </div>
      <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
          <label htmlFor="password">Password</label>
          <input type="password" className="form-control" name="password" value={password} onChange={this.handleChange} />
          {submitted && !password &&
              <div className="help-block">Password is required</div>
          }
      </div>
      <div className="form-group">
          <button style={{backgroundColor:'#EA1640',color:'#fff'}} className="loginButton">Login</button>
          {loggingIn &&
              <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
          }
          <Link to="/register" style={{color:'black'}} className="btn btn-link">Register</Link>
      </div>
  </form>

</div>*/}