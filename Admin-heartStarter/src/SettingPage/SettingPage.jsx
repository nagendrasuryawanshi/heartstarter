import React from "react";
import { connect } from "react-redux";
//import { deviceActions } from "../_actions";

class SettingPage extends React.Component {

    render() {
        return (
            <div>
        <div className="loader-fixed" id="loading">
      <div className="loader-centered">
        <div className="loader-inner-centered emphasize-dark">
            <div className="loader">
              <div className="bg-loading-img">
              </div>
            </div>
            <p className="loading-title">Please Wait...</p>
        </div>
      </div>
  </div>
        <div className="sidebar" id="sidebar">
      <div className="block-sidebar">
        <div className="block--elm">
           <div className="block--elm-header">
              <img src="../src/assets/img/logo.png"/>
           </div> 
           <div className="block--elm-list">
              <aside className="sidebar-left-collapse">
                <div className="sidebar-links nav nav-pills flex-column">
                  <div className="nav-item ">
                    <a href="/" className="nav-link">
                      Dashboard
                    </a>
                  </div>
                  <div className="nav-item">
                    <a href="/sos"  className="nav-link">
                      SOS Signals
                    </a>
                  </div>
                  <div className="nav-item">
                    <a href="/users"  className="nav-link">
                      Users
                    </a>
                  </div>
                  <div className="nav-item selected">
                      <a href="/setting" className="nav-link">
                      Settings
                  </a>
                  </div>
                  <div className="nav-item">
                      <a href="#" className="nav-link">
                          Profile
                  </a>
                      <ul className="nav nav-pills flex-column sub-links">
                          <li className="nav-item">
                              <a className="nav-link" href="settings.html">Personal Info</a>
                          </li>
                          <li className="nav-item">
                              <a className="nav-link" href="/login">Logout</a>
                          </li>
                      </ul>
                  </div>
                  <div className="nav-item">
                    <a href="/device"  className="nav-link">
                      Device List
                    </a>
                  </div>
                </div>
              </aside>
           </div> 
        </div>
      </div>
    </div>
    <div class="content xs-ml-0">
    <div class="block-header">
     <div class="container-fluid">
      <div class="row">
        <div class="col col-sm-6">
          <div class="block-inline"> 
            <div class="block-inline_elm">
              <h2 class="heading-text mr-3 hide_xs">Settings</h2>
              <a class="logo_mobile show_xs">
                <span>
                  UAV<br/>
                  Control Tower
                </span>
                </a>
             </div>
          </div>
        </div>
        <div class="col col-sm-6 text-right">
          <div class="block-inline">
            <div class="block-inline_elm float-right"> 
             <button type="button" class="btn btn-outline-primary">Back <i class="fa fa-retweet ml-1" aria-hidden="true"></i></button>
           </div>
         </div>
       </div>
     </div>
   </div> 
  </div>
    <div class="block-content">
    <div class="block-list mt-3 mb-3">
      <div class="container-fluid">
        <div class="row">
          <div class="col">
            <div class="card b-0">
              <div class="card-header bg-white b-0 pl-0">
                <h1>Complete your profile</h1>
              </div>
              <div class="card-content p-0">
                <form>
                  <div class="row">
                    <div class="col-12">
                      <div class="form-group label-floating">
                        <input type="text" class="form-control inpt" placeholder="Company Name"/>
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-group label-floating">
                        <input type="tel" class="form-control inpt" placeholder="Phone Number"/>
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-group label-floating">
                        <input type="email" class="form-control inpt" placeholder="example@gmail.com"/>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12">
                      <h6 class="sm-head">Office Address</h6>
                    </div>
                    <div class="col-12">
                      <div class="form-group label-floating">
                        <input type="text" class="form-control inpt" placeholder="Address1"/>
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-group label-floating">
                        <input type="text" class="form-control inpt" placeholder="Address2"/>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-group label-floating">
                        <input type="text" class="form-control inpt" placeholder="Zip"/>
                      </div>
                    </div>
                    <div class="col-8">
                      <div class="form-group label-floating">
                        <input type="text" class="form-control inpt" placeholder="City"/>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12">
                      <div class="form-group label-floating">
                        <select  class="custom-select form-control sel"> 
                          <option class="option">Country</option>
                          <option>Country A</option>
                          <option>Country B</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-group label-floating">
                        <select  class="custom-select form-control sel"> 
                          <option class="option">Province</option>
                          <option>Province A</option>
                          <option>Province B</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
          <div class="col">
            <div class="card card-profile text-center">
              <div class="profile-content">
                <div class="file-upload">
                   <div class="image-upload-wrap">
                   {/* onchange="readURL(this);" */}
                    <input class="file-upload-input" type='file'  accept="image/*" />
                    <div class="drag-text">
                      <h3>Drag and drop a file or select add Image</h3>
                    </div>
                  </div>
                  <div class="file-upload-content">
                    <img class="file-upload-image" src="#" alt="your image"/>
                    <div class="image-title-wrap">
                    {/* onclick="removeUpload()" */}
                      <button type="button"  class="btn remove-image">Remove <span class="image-title">Uploaded Image</span> X </button>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div class="row">
              <div class="col">
                <button type="submit" class="btn btn-primary float-left mt-3 mb-3">Save Profile<i class="fa fa-location-arrow ml-4"></i></button>
              </div>
              <div class="col">
              {/* onclick="$('.file-upload-input').trigger( 'click' )" */}
                <button type="submit" class="btn btn-primary float-right mt-3 mb-3 file-upload-btn" >Upload Logo <i class="fa fa-plus-circle ml-4"></i></button>
              </div>
            </div>

          </div>  
        </div>
        <div class="row">
          <div class="col">
            <h6 class="">Pilots</h6>
            <div class="row"> 
              <div class="col">
                <div class="d-block b-1 r-2">
                  <div class="table-responsive">
                   <table class="table table-borderless table-striped mb-0 pd10" cellSpacing="0" cellPadding="0">
                    <thead>                   
                      <tr>
                        <th>Name</th>
                        <th>Phone</th>
                        <th></th>
                      </tr>
                    </thead> 
                    <tbody>
                      <tr>
                        <td>Jonathan King</td>
                        <td>864 574 2000</td>
                        <td><img src="../src/assets/img/del_black.png"/></td>
                      </tr>
                      <tr>
                        <td>Jonathan King</td>
                        <td>864 574 2000</td>
                        <td><img src="../src/assets/img/del_black.png"/></td>
                      </tr>
                      <tr>
                        <td>Jonathan King</td>
                        <td>864 574 2000</td>
                        <td><img src="../src/assets/img/del_black.png"/></td>
                      </tr>
                      <tr>
                        <td>Jonathan King</td>
                        <td>864 574 2000</td>
                        <td><img src="../src/assets/img/del_black.png"/></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div> 
           {/*onclick="openSidepanel('pilots')"  */}
          <button type="submit" class="btn btn-primary float-right mt-3 mb-3" >Add Pilots <i class="fa fa-plus-circle ml-4"></i></button>
        </div>
        <div class="col">
          <h6 class="">Approved Regions (SFOC Documenation)</h6>
          <div class="row"> 
            <div class="col">
              <div class="d-block b-1 r-2">
                <div class="table-responsive">
                  <table class="table table-borderless table-striped mb-0 pd10" cellSpacing="0" cellPadding="0">
                    <thead>                   
                      <tr>
                        <th>Status</th>
                        <th>Country</th>
                        <th>Region</th>
                        <th>SFOC Number</th>
                      </tr>
                    </thead> 
                    <tbody>
                      <tr>
                        <td>Approved</td>
                        <td>Canada</td>
                        <td>NewFoundLand & lanbourate</td>
                        <td>234876523487652348765</td>
                        <td><img src="../src/assets/img/del_black.png"/></td>
                      </tr>
                      <tr>
                        <td>Approved</td>
                        <td>Canada</td>
                        <td>Nova Scotia</td>
                        <td>234876523487652348765</td>
                        <td><img src="../src/assets/img/del_black.png"/></td>
                      </tr>
                      <tr>
                        <td>Awaiting Approved</td>
                        <td>Canada</td>
                        <td>Ontario</td>
                        <td>234876523487652348765</td>
                        <td><img src="../src/assets/img/del_black.png"/></td>
                      </tr>
                      <tr>
                        <td>Jonathan King</td>
                        <td>Canada</td>
                        <td>Ontario Last</td>
                        <td>234876523487652348765</td>
                        <td><img src="../src/assets/img/del_black.png"/></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div> 
           {/*onclick="openSidepanel('region')"  */}
          <button type="submit" class="btn btn-primary float-right mt-3 mb-3" >Add Region <i class="fa fa-plus-circle ml-4"></i></button>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <h6 class="">Ground Supervisor</h6>
          <div class="row"> 
            <div class="col">
              <div class="d-block b-1 r-2">
                <div class="table-responsive">
                  <table class="table table-borderless table-striped mb-0 pd10" cellSpacing="0" cellPadding="0">
                    <thead>                   
                      <tr>
                        <th>Name</th>
                        <th>Phone</th>
                        <th></th>
                      </tr>
                    </thead> 
                    <tbody>
                      <tr>
                        <td>Jonathan King</td>
                        <td>864 574 2000</td>
                        <td><img src="../src/assets/img/del_black.png"/></td>
                      </tr>
                      <tr>
                        <td>Jonathan King</td>
                        <td>864 574 2000</td>
                        <td><img src="../src/assets/img/del_black.png"/></td>
                      </tr>
                      <tr>
                        <td>Jonathan King</td>
                        <td>864 574 2000</td>
                        <td><img src="../src/assets/img/del_black.png"/></td>
                      </tr>
                      <tr>
                        <td>Jonathan King</td>
                        <td>864 574 2000</td>
                        <td><img src="../src/assets/img/del_black.png"/></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>  
          {/* onclick="openSidepanel('supervisor')" */}
          <button type="submit" class="btn btn-primary float-right mt-3 mb-3" >Add Ground Supervisor<i class="fa fa-plus-circle ml-4"></i></button>
        </div>
        <div class="col">
          <h6 class="">Drones</h6>
          <div class="row"> 
            <div class="col">
              <div class="d-block b-1 r-2">
                <div class="table-responsive">
                  <table class="table table-borderless table-striped mb-0 pd10" cellSpacing="0" cellPadding="0" style={{width:'100%'}} id="droneTable">
                    <thead>                   
                      <tr>
                        <th>Make & Modal</th>
                        <th>Serial Number</th>
                        <th>Serial Code</th>
                        <th></th>
                      </tr>
                    </thead> 
                    <tbody>
                      <tr>
                        <td>DJI 1</td>
                        <td>ABCS864742000</td>
                        <td>ABCSKKSL</td>
                        <td><img src="../src/assets/img/del_black.png"/></td>
                      </tr>
                      <tr>
                        <td>DJI 2</td>
                        <td>ABCS864742000</td>
                        <td>ABCSKKSL</td>
                        <td><img src="../src/assets/img/del_black.png"/></td>
                      </tr>
                      <tr>
                        <td>DJI 3</td>
                        <td>ABCS864742000</td>
                        <td>ABCSKKSL</td>
                        <td><img src="../src/assets/img/del_black.png"/></td>
                      </tr>
                      <tr>
                        <td>DJI Form</td>
                        <td>ABCS864742000</td>
                        <td>ABCSKKSL</td>
                        <td><img src="../src/assets/img/del_black.png"/></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div> 
           {/*onclick="openSidepanel('drones')"  */}
          <button type="submit" class="btn btn-primary float-right mt-3 mb-3" >Add Drones <i class="fa fa-plus-circle ml-4"></i></button>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
    </div>
        );
    }

}

function mapStateToProps(state) {
    const { devices, authentication } = state;
    // const { user } = authentication;
    return {
        // user,
        devices
    };
}

const connectedSettingPage = connect(mapStateToProps)(SettingPage);
export { connectedSettingPage as SettingPage };