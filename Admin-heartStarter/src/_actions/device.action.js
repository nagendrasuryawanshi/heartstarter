import { userConstants } from "../_constants";
import { deviceService } from "../_services";
// import { alertActions } from "./";
// import { history } from "../_helpers";

export const deviceActions = {
  getAllDevice,
  getAllDeviceList
};

// <!-- getAll Device  -->
function getAllDevice() {
  return dispatch => {
    dispatch(request());

    deviceService
      .getAllDevice()
      .then(
        devices => dispatch(success(devices)),

        error => dispatch(failure(error.toString()))
      );
  };

  function request() {
    return { type: userConstants.GETALL_DEVICE_REQUEST };
  }
  function success(devices) {
    return { type: userConstants.GETALL_DEVICE_SUCCESS, devices };
  }
  function failure(error) {
    return { type: userConstants.GETALL_DEVICE_FAILURE, error };
  }
}

// <!-- End  -->

// <!-- Get All Device List  -->
function getAllDeviceList() {
  return dispatch => {
    dispatch(request());

    deviceService
      .getAllDeviceList()
      .then(
        device_list => dispatch(success(device_list)),

        error => dispatch(failure(error.toString()))
      );
  };

  function request() {
    return { type: userConstants.GETALL_DEVICE_LIST_REQUEST };
  }
  function success(device_list) {
    return { type: userConstants.GETALL_DEVICE_LIST_SUCCESS, device_list };
  }
  function failure(error) {
    return { type: userConstants.GETALL_DEVICE_LIST_FAILURE, error };
  }
}

// <!-- End  -->

