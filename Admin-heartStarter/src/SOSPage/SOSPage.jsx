import React from "react";
import { connect } from "react-redux";
//import { deviceActions } from "../_actions";

class SOSPage  extends React.Component {
    render() {
        return (
            <div>
              <div className="loader-fixed" id="loading">
          <div className="loader-centered">
            <div className="loader-inner-centered emphasize-dark">
                <div className="loader">
                  <div className="bg-loading-img">
                  </div>
                </div>
                <p className="loading-title">Please Wait...</p>
            </div>
          </div>
      </div>
            <div className="sidebar" id="sidebar">
          <div className="block-sidebar">
            <div className="block--elm">
               <div className="block--elm-header">
                  <img src="../src/assets/img/logo.png"/>
               </div> 
               <div className="block--elm-list">
                  <aside className="sidebar-left-collapse">
                    <div className="sidebar-links nav nav-pills flex-column">
                      <div className="nav-item ">
                        <a href="/" className="nav-link">
                          Dashboard
                        </a>
                      </div>
                      <div className="nav-item selected">
                        <a href="/sos"  className="nav-link">
                          SOS Signals
                        </a>
                      </div>
                      <div className="nav-item">
                        <a href="/users"  className="nav-link">
                          Users
                        </a>
                      </div>
                      <div className="nav-item">
                          <a href="/setting" className="nav-link">
                          Settings
                      </a>
                      </div>
                      <div className="nav-item">
                          <a href="#" className="nav-link">
                              Profile
                      </a>
                          <ul className="nav nav-pills flex-column sub-links">
                              <li className="nav-item">
                                  <a className="nav-link" href="settings.html">Personal Info</a>
                              </li>
                              <li className="nav-item">
                                  <a className="nav-link" href="/login">Logout</a>
                              </li>
                          </ul>
                      </div>
                      <div className="nav-item">
                        <a href="/device"  className="nav-link">
                          Device List
                        </a>
                      </div>
                    </div>
                  </aside>
               </div> 
            </div>
          </div>
        </div>
        <div class="content">
        <div class="block-header">
         <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
               <div class="block-inline"> 
                <div class="block-inline_elm">
                  <h2 class="heading-text mr-3">SOS Signals</h2>
                </div>
                <div class="block-inline_elm">
                </div>
                <div class="block-inline_elm">
                  <input type="search" placeholder="Search" class="search_input bg_search"/>
                </div>
              </div>
            </div>
          </div>
        </div> 
      </div>
    <div class="block-content">
      <div class="block-activity">
        <div class="container-fluid">
           <div class="row">
              <div class="col-sm-8">
                  <div class="row">
                    <div class="col-sm-2">
                      <div class="form-group multiselect_dropdown mb-0">
                          <select id="ms" multiple="multiple" class="hidden">
                              <option value="1">Roles</option>
                              <option value="2">Drone NL</option>
                              <option value="3">Drone NL</option>
                              <option value="4">Fighter Drones</option>
                              <option value="5">Gregs´s Drones</option>
                              <option value="6">Wahuuu SOS Signals</option>
                              <option value="1">Atlantic Drone</option>
                              <option value="2">Drone NL</option>
                              <option value="3">Drone NL</option>
                              <option value="4">Fighter Drones</option>
                              <option value="5">Gregs´s Drones</option>
                              <option value="6">Wahuuu SOS Signals</option>
                          </select>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group multiselect_dropdown mb-0">
                          <select id="ms1" multiple="multiple" class="hidden">
                              <option value="1">Atlantic Drone</option>
                              <option value="2">Drone NL</option>
                              <option value="3">Drone NL</option>
                              <option value="4">Fighter Drones</option>
                              <option value="5">Gregs´s Drones</option>
                              <option value="6">Wahuuu SOS Signals</option>
                          </select>
                      </div>
                    </div>
                  <div class="col-sm-2">
                      <div class="form-group multiselect_dropdown mb-0">
                          <select id="ms2" multiple="multiple" class="hidden">
                              <option value="1">Atlantic Drone</option>
                              <option value="2">Drone NL</option>
                              <option value="3">Drone NL</option>
                              <option value="4">Fighter Drones</option>
                              <option value="5">Gregs´s Drones</option>
                              <option value="6">Wahuuu SOS Signals</option>
                          </select>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-sm-4 text-right">
                  <p class="normal-text inline mr-3 mb-0">Viewing<strong> 1-20 </strong> of <strong>36</strong></p>
                  <div class="btn-group tbl-next" role="group">
                      <button type="button" class="btn btn-outline-secondary">
                        <img src="../src/assets/img/tbl_next_left.png" class="tbl_next"/>
                      </button>
                      <button type="button" class="btn btn-outline-secondary">
                        <img src="../src/assets/img/tbl_next_right.png" class="tbl_next"/>
                      </button>
                  </div>
              </div>
          </div>
        </div>
      </div>
      <div class="block-list">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
               <div class="tableresponsive">
                  <table class="table table-borderless table-striped mb-0 tr56" id="flightTable" cellSpacing="0" cellPadding="0" style={{width:'100%'}}>
                  <thead>                   
                    <tr>
                      <th>Company</th>
                      <th>Pilot</th>
                      <th>Flight Status</th>
                      <th>Last Updated</th>
                      <th>Notes</th>
                      <th></th>
                    </tr>
                  </thead> 
                  <tbody>
                         <tr>
                          <td>Drone NL</td>
                          <td>Jonathan King</td>
                          <td>In Flight</td>
                          <td>9/12/2017</td>
                          <td>The universe is a big place, perhaps the biggest …</td>
                          <td class="dropdown tbl-dropdown p-0">
                             <button class="btn btn-link dropdown-toggle area72" type="button" id="about-us" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                              </button>
                              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="about-us">
                                <h6 class="dropdown-header">Action</h6>
                                <a class="dropdown-item" href="#"><img src="../src/assets/img/edit.png" class="mr-2"/> Edit</a>
                                <a class="dropdown-item" href="#"><img src="../src/assets/img/delete.png" class="mr-2"/> Delete</a>
                                <a class="dropdown-item" href="#"><img src="../src/assets/img/report.png" class="mr-2"/> Report</a>
                              </div>
                          </td>
                        </tr>
                         <tr>
                          <td>Drone NL</td>
                          <td>Jonathan King</td>
                          <td>In Flight</td>
                          <td>9/12/2017</td>
                          <td>The universe is a big place, perhaps the biggest …</td>
                          <td class="dropdown tbl-dropdown p-0">
                             <button class="btn btn-link dropdown-toggle area72" type="button" id="about-us" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                              </button>
                              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="about-us">
                                <h6 class="dropdown-header">Action</h6>
                                <a class="dropdown-item" href="#"><img src="../src/assets/img/edit.png" class="mr-2"/> Edit</a>
                                <a class="dropdown-item" href="#"><img src="../src/assets/img/delete.png" class="mr-2"/> Delete</a>
                                <a class="dropdown-item" href="#"><img src="../src/assets/img/report.png" class="mr-2"/> Report</a>
                              </div>
                          </td>
                        </tr>
                         <tr>
                          <td>Drone NL</td>
                          <td>Jonathan King</td>
                          <td>In Flight</td>
                          <td>9/12/2017</td>
                          <td>The universe is a big place, perhaps the biggest …</td>
                          <td class="dropdown tbl-dropdown p-0">
                             <button class="btn btn-link dropdown-toggle area72" type="button" id="about-us" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                              </button>
                              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="about-us">
                                <h6 class="dropdown-header">Action</h6>
                                <a class="dropdown-item" href="#"><img src="../src/assets/img/edit.png" class="mr-2"/> Edit</a>
                                <a class="dropdown-item" href="#"><img src="../src/assets/img/delete.png" class="mr-2"/> Delete</a>
                                <a class="dropdown-item" href="#"><img src="../src/assets/img/report.png" class="mr-2"/> Report</a>
                              </div>
                          </td>
                        </tr>
                         <tr>
                          <td>Drone NL</td>
                          <td>Jonathan King</td>
                          <td>In Flight</td>
                          <td>9/12/2017</td>
                          <td>The universe is a big place, perhaps the biggest …</td>
                          <td class="dropdown tbl-dropdown p-0">
                             <button class="btn btn-link dropdown-toggle area72" type="button" id="about-us" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                              </button>
                              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="about-us">
                                <h6 class="dropdown-header">Action</h6>
                                <a class="dropdown-item" href="#"><img src="../src/assets/img/edit.png" class="mr-2"/> Edit</a>
                                <a class="dropdown-item" href="#"><img src="../src/assets/img/delete.png" class="mr-2"/> Delete</a>
                                <a class="dropdown-item" href="#"><img src="../src/assets/img/report.png" class="mr-2"/> Report</a>
                              </div>
                          </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="clearfix"></div>
                  </div>
              </div>    
            </div>
          </div>  
      </div>
    </div>
  </div>
        </div>
        );
    }

}

function mapStateToProps(state) {
    const { devices, authentication } = state;
   // const { user } = authentication;
    return {
       // user,
        devices
    };
}

const connectedSOSPage = connect(mapStateToProps)(SOSPage);
export { connectedSOSPage as SOSPage };