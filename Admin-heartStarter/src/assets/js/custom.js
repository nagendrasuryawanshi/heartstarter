$(window).on('load', function(){
  setTimeout(removeLoader, 10); 
});
function removeLoader(){
     $( "#loading" ).fadeOut(500, function() {
      $( "#loading" ).addClass("hideMe"); 
    });  
}


// $("button").click(function(){
//     button = $(this);
//     var btntext = '&nbsp;Please Wait....';
//     var btnHtml = '<div class="button-loading"><div class="loader button_loading"><div class="bg-loading-img"></div></div>'+btntext+'</div>';
//     setTimeout(function(){
//         $(".button_loading").fadeOut(5000, function() {
//           $(".button_loading").addClass("hideMe"); 
//         });
//         btntext = 'Submit';   
//         button.html(btnHtml);
//     }, 80);
// });


$(function () {
    var links = $('.sidebar-links > div');

    links.on('click', function () {
      links.removeClass('selected');
      $(this).addClass('selected');
    });
});

$(window).bind("load resize", function() {
    setHeightDesktop();
});

$(function() {
    $('#ms').change(function() {
       // console.log($(this).val());
    }).multipleSelect({
        width: '100%',
        placeholder: 'Roles (2)'
    });
});

$(function() {
    $('#ms1').change(function() {
        //console.log($(this).val());
    }).multipleSelect({
        width: '100%',
        placeholder: 'Status'
    });
});

$(function() {
    $('#ms2').change(function() {
       // console.log($(this).val());
    }).multipleSelect({
        width: '100%',
        placeholder: 'Last Updated'
    });
});

$(function() {
    $('#ms3').change(function() {
        //console.log($(this).val());
    }).multipleSelect({
        width: '100%',
        placeholder: 'Max Altitude'
    });
});

$('.flat-toggle').on('click', function() {
    $(this).toggleClass('on');
});


function setHeightDesktop(){
    $("#sidebar").height($(window).height());
    $(".right_pbody").height( $(window).height() - ($(".right_phead").height() + $(".right_pfoot").height()) );

  
}
$(document).ready(function () {
    /*----Toggle JS------*/
    $('#sidebarCollapse').on('click', function () {
         $('#sidebar').toggleClass('active');
         $(this).toggleClass('active');
     });
    /*------Custom Scroll section-----*/
    $(".overflowscroll").mCustomScrollbar({
        theme: "minimal-dark"
    });
    $(".scroll_dark").mCustomScrollbar({
        theme: "minimal-dark"
    });

   $(".ms-drop ul").mCustomScrollbar({
            theme: "dark"
    });
    /*-----custom Scroll Sidebar*/  
    $("#sidebar").mCustomScrollbar({
        theme: "minimal-dark"
    });
    /*----asidebar----*/
});


$("#menu-close").click(function(e) {
    e.preventDefault();
    $("#sidebar-wrapper").removeClass("active");
    $("body").css("overflow", "auto");
});

// var table = $('#flightTable');
// table.on('click', 'tr > td', function () {
//     if ($(this).index() == 5)
//     { return; }
    
//     if ($(this).parent().hasClass('rowSelected')) {
//         $(this).parent().removeClass('rowSelected');
//         $("body").css("overflow", "hidden");
//         $("#sidebar-wrapper").removeClass("active");
       
//         $("body").css("overflow", "auto");
//     }
//     else {
//         $(".rowSelected").removeClass("rowSelected");
//         $(this).parent().addClass('rowSelected');
//         $("body").css("overflow", "hidden");
//         $("#sidebar-wrapper").addClass("active");
//         $("body").css("overflow", "hidden");
//     }
// });

function openSidepanel(id)
{
    $("body").css("overflow", "hidden");
    $("#sidebar-wrapper").addClass("active");    
   
    if(id=="pilots")
    {
        $("#pilots").css("display", "block");
    }
    else if(id=="region")
    {
        //$("#pilots").css("display", "none");
    }
    else if(id=="supervisor")
    {
        //$("#pilots").css("display", "none");
    }
    else(id=="drones")
    {
        //$("#pilots").css("display", "none");
    }
}




