/** @format */

import {AppRegistry ,Dimensions } from 'react-native';
import RootStack from './src/routes/RootStack';
import {name as appName} from './app.json';
import { createDrawerNavigator } from 'react-navigation';
import SideMenu  from './src/componants/SideBar';
console.disableYellowBox = true;

const App = createDrawerNavigator({
    Item1: {
        screen: RootStack,
      }
    }, {
      contentComponent: SideMenu,
      drawerWidth: Dimensions.get('window').width - 60,  
  });

AppRegistry.registerComponent(appName, () => App);
